/*!
  \file                  RD53BProdInterface.h
  \brief                 User interface to the RD53B readout chip
  \author                Alkis Papadopoulos
  Support:               email to alkiviadis.papadopoulos@cern.ch
*/

#ifndef RD53BProdInterface_H
#define RD53BProdInterface_H

#include "BeBoardFWInterface.h"
#include "HWDescription/ChipRegItem.h"
#include "HWDescription/RD53BCommands.h"
#include "HWDescription/RD53BProd.h"
#include "RD53FWInterface.h"
#include "RD53InterfaceBase.h"
#include "Utils/BitMaster/BitVector.h"
#include "Utils/RD53BUtils.h"

// #############
// # CONSTANTS #
// #############
#define VCALSLEEP 50000 // [microseconds]

namespace Ph2_HwInterface
{
template <class Flavor>
class RD53BProdInterface : public RD53InterfaceBase
{
    using Chip        = Ph2_HwDescription::Chip;
    using ReadoutChip = Ph2_HwDescription::ReadoutChip;
    using BeBoard     = Ph2_HwDescription::BeBoard;
    using Hybrid      = Ph2_HwDescription::Hybrid;
    using RD53B       = Ph2_HwDescription::RD53BProd<Flavor>;

    using Reg           = typename RD53B::Reg;
    using Register      = Ph2_HwDescription::RD53BProdConstants::Register;
    using RegisterField = Ph2_HwDescription::RD53BProdConstants::RegisterField;

  public:
    RD53BProdInterface(const BeBoardFWMap& pBoardMap);

    // #############################
    // # Override member functions #
    // #############################
    bool     ConfigureChip(Chip* pChip, bool pVerifLoop = true, uint32_t pBlockSize = 310) override;
    bool     WriteChipReg(Chip* pChip, const std::string& regName, uint16_t data, bool pVerifLoop = true) override;
    void     WriteBoardBroadcastChipReg(const BeBoard* pBoard, const std::string& regName, uint16_t data) override;
    bool     WriteChipAllLocalReg(ReadoutChip* pChip, const std::string& regName, const ChipContainer& pValue, bool pVerifLoop = true) override { return true; };
    void     ReadChipAllLocalReg(ReadoutChip* pChip, const std::string& regName, ChipContainer& pValue) override {}
    uint16_t ReadChipReg(Chip* pChip, const std::string& regName) override;
    bool     ConfigureChipOriginalMask(ReadoutChip* pChip, bool pVerifLoop = true, uint32_t pBlockSize = 310) override;
    bool     MaskAllChannels(ReadoutChip* pChip, bool mask, bool pVerifLoop = true) override;
    bool     maskChannelsAndSetInjectionSchema(ReadoutChip* pChip, const std::shared_ptr<ChannelGroupBase> group, bool mask, bool inject, bool pVerifLoop = false) override { return true; }

    boost::optional<uint16_t> ReadChipReg(RD53B* chip, const Register& reg);
    uint16_t                  ReadReg(Chip* chip, const Register& reg, bool update = false);
    size_t                    ReadReg(Chip* chip, const std::string& regName, bool update = false);

    void WriteReg(Chip* chip, const Register& reg, uint16_t value);
    void WriteRegField(Chip* chip, const RegisterField& field, uint16_t value, bool update = false);
    void WriteReg(Chip* chip, const std::string& regName, size_t value, bool update = false);
    void WriteReg(Hybrid* chip, const Register& reg, uint16_t value);
    void WriteReg(Hybrid* chip, const std::string& regName, size_t value, bool update = false);

    void ConfigureReg(RD53B* rd53b, const std::string& regName, size_t value)
    {
        rd53b->configureRegister(regName, value);
        WriteReg(rd53b, regName, value);
    }

    void ConfigureReg(RD53B* rd53b, const Register& reg, size_t value)
    {
        rd53b->configureRegister(reg.name, value);
        WriteReg(rd53b, reg, value);
    }

    void ChipErrorReport(ReadoutChip* pChip);

    void InitRD53Downlink(const BeBoard* pBoard) override;
    void InitRD53Uplinks(ReadoutChip* pChip) override;

    // update masks only
    int UpdatePixelMasks(Chip*                                                  chip,
                          const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>& enable,
                          const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>& enableInjections,
                          const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>& enableHitOr)
    {
        return UpdatePixelConfig(chip, &enable, &enableInjections, &enableHitOr, nullptr, true, false);
    }

    // update tdac only
    int UpdatePixelTDAC(Chip* chip, const Ph2_HwDescription::pixel_matrix_t<Flavor, uint8_t>& tdac) { 
        return UpdatePixelConfig(chip, nullptr, nullptr, nullptr, &tdac, false, true); 
    }

    // update both masks and tdac
    int UpdatePixelConfig(Chip*                                                     chip,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>&    enable,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>&    enableInjections,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>&    enableHitOr,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, uint8_t>& tdac)
    {
        return UpdatePixelConfig(chip, &enable, &enableInjections, &enableHitOr, &tdac, true, true);
    }

    int UpdatePixelConfig(Chip* chip, bool updateMasks = true, bool updateTdac = true)
    {
        auto  rd53b = static_cast<RD53B*>(chip);
        auto& cfg   = rd53b->pixelConfig();
        if(updateMasks && updateTdac)
            return UpdatePixelConfig(chip, cfg.enable, cfg.enableInjections, cfg.enableHitOr, cfg.tdac);
        else if(updateMasks)
            return UpdatePixelMasks(chip, cfg.enable, cfg.enableInjections, cfg.enableHitOr);
        else
            return UpdatePixelTDAC(chip, cfg.tdac);
    }

    template <class Device>
    void UpdatePixelMasksUniform(Device* device, bool enable, bool enableInjections, bool enableHitOr)
    {
        UpdatePixelConfigUniform(device, enable, enableInjections, enableHitOr, 0, true, false);
    }

    template <class Device>
    void UpdatePixelTDACUniform(Device* device, uint8_t tdac)
    {
        UpdatePixelConfigUniform(device, false, false, false, tdac, false, true);
    }

    template <class Device>
    void UpdatePixelConfigUniform(Device* device, bool enable, bool enableInjections, bool enableHitOr, uint8_t tdac)
    {
        UpdatePixelConfigUniform(device, enable, enableInjections, enableHitOr, tdac, true, true);
    }

  private:
    template <class T>
    using ChipDataMap = std::map<Ph2_HwDescription::ChipLocation, T>;

    template <class Device>
    void UpdatePixelConfigUniform(Device* device, bool enable, bool enableInjections, bool enableHitOr, uint8_t tdac, bool updateMasks, bool updateTdac)
    {
        setup(device);
        std::vector<uint16_t> cmdStream;

        uint8_t masks = bits::pack<1, 1, 1>(enableHitOr, enableInjections, enable);

        ChipDataMap<uint16_t> pixMode;
        RD53BUtils::for_each_device<Chip>(device, [&](Chip* chip) { pixMode[chip] = ReadReg(chip, Reg::PIX_MODE); });

        tdac                           = Flavor::encodeTDAC(tdac);
        std::vector<uint16_t> maskData = std::vector<uint16_t>(RD53B::nRows, masks << 5 | masks);
        std::vector<uint16_t> tdacData = std::vector<uint16_t>(RD53B::nRows, tdac << 5 | tdac);

        if(updateMasks && !updateTdac) SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::PIX_MODE.address, uint16_t{5});

        if(!updateMasks && updateTdac) SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::PIX_MODE.address, uint16_t{7});

        for(uint16_t colPair = 0; colPair < 4; ++colPair)
        {
            SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::REGION_COL.address, colPair);

            if(updateMasks)
            {
                if(updateTdac) SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::PIX_MODE.address, uint16_t{5});
                SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::REGION_ROW.address, uint16_t{0});
                SerializeCommand<RD53BCmd::WrRegLong>(device, cmdStream, maskData);
            }

            if(updateTdac)
            {
                if(updateMasks) SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::PIX_MODE.address, uint16_t{7});
                SerializeCommand<RD53BCmd::WrReg>(device, cmdStream, Reg::REGION_ROW.address, uint16_t{0});
                SerializeCommand<RD53BCmd::WrRegLong>(device, cmdStream, tdacData);
            }
        }
        SendCommandStream(device, cmdStream);

        RD53BUtils::for_each_device<Chip>(device, [&](Chip* chip) { WriteReg(chip, Reg::PIX_MODE, pixMode[chip]); });
    }

    int UpdatePixelConfig(Chip*                                                     chip,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>*    enable,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>*    enableInjections,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, bool>*    enableHitOr,
                           const Ph2_HwDescription::pixel_matrix_t<Flavor, uint8_t>* tdac,
                           bool                                                      updateMasks,
                           bool                                                      updateTdac);

  public:
    // serialize a command for any destination device into an existing vector
    template <class Cmd, class Device, class... Args>
    void SerializeCommand(const Device* destination, std::vector<uint16_t>& cmdStream, Args&&... args)
    {
        RD53BCmd::serialize(RD53BCmd::make_cmd<Cmd>(RD53B::ChipIdFor(destination), args...), cmdStream);
    }

    // serialize a command for any destination device into a new vector
    template <class Cmd, class Device, class... Args>
    auto SerializeCommand(const Device* destination, Args&&... args)
    {
        std::vector<uint16_t> cmdStream;
        SerializeCommand<Cmd>(destination, cmdStream, std::forward<Args>(args)...);
        return cmdStream;
    }

    // send a single command to any destination device
    template <class Cmd, class Device, class... Args>
    void SendCommand(const Device* destination, Args&&... args)
    {
        auto cmdStream = SerializeCommand<Cmd>(destination, std::forward<Args>(args)...);
        SendCommandStream(destination, cmdStream);
    }

    static size_t PLLlockPeriod;
    static size_t nPLLlockWords;

  private:
    static const size_t syncPeriod;
    static const size_t nSyncWords;

    std::vector<uint16_t> SprinklePllLock(const std::vector<uint16_t>&) const;

  public:
    // send a BitVector containing serialized commands to the hybrid which is/contains the given destination device
    template <class Device>
    void SendCommandStream(const Device* destination, const std::vector<uint16_t>& cmdStream)
    {
        // Compute number of 16-bit words to which we add 2 sync words every 30:
        // n16bitWordsPerPacketExclSync + nSyncWords * n16bitWordsPerPacketExclSync / syncPeriod = totaNumb16bitWords = 2 * (1 << 16) - 1
        static const size_t n16bitWordsPerPacketExclSync = (2 * (1 << 16) - 1) / (1 + (double)nSyncWords / syncPeriod);

        RD53FWInterface& fwInterface = setup(destination);

        auto streamWithPllLock = SprinklePllLock(cmdStream);

        auto begin = streamWithPllLock.begin();
        while(begin != streamWithPllLock.end())
        {
            const size_t n16WordsThisPacketExclSync = std::min(n16bitWordsPerPacketExclSync, size_t(streamWithPllLock.end() - begin));
            // const size_t n32bitWordsThisPacket      = std::ceil((n16WordsThisPacketExclSync + nSyncWords * (n16WordsThisPacketExclSync / syncPeriod)) / 2.0);

            BitVector<uint32_t> chipCmdPacket;

            auto it   = begin;
            auto next = std::min(it + syncPeriod, begin + n16WordsThisPacketExclSync);
            for(; it != next; ++it) chipCmdPacket.append(*it, 16);

            while(it != begin + n16WordsThisPacketExclSync)
            {
                for(size_t i = 0; i < nSyncWords; ++i) 
                    chipCmdPacket.append(RD53BCmd::Sync::cmdCode(), 16);

                auto next = std::min(it + syncPeriod, begin + n16WordsThisPacketExclSync);
                for(; it != next; ++it) chipCmdPacket.append(*it, 16);
            }
            if (chipCmdPacket.size() % 32 != 0) 
                chipCmdPacket.append(RD53BCmd::Sync::cmdCode(), 16);

            BitVector<uint32_t> cmdPacket;
            cmdPacket.blocks().reserve(chipCmdPacket.blocks().size() + 1);
            cmdPacket.append(bits::pack<6, 10, 16>(RD53FWconstants::HEADEAR_WRTCMD, fwInterface.getHybridMaskFor(destination), chipCmdPacket.blocks().size()), 32);
            cmdPacket.append(chipCmdPacket.view());

            fwInterface.SendChipCommands(cmdPacket.blocks());
            begin = it;
        }
    }

    template <class Device>
    void SendGlobalPulse(Device* destination, const std::vector<std::string>& routes, uint8_t width = 1, bool skipCheck = false)
    {
        uint16_t route_mask = 0;

        for(const auto& name: routes) route_mask |= (1 << Flavor::GlobalPulseRoutes.at(name));

        //TODO: get these values from the register table for the chip
        uint16_t old_route_mask = 0;
        uint16_t old_width = 1;

        //do not try to read the registers: might fail during initialisation
        if (skipCheck == false) {
            old_route_mask = ReadReg(destination, Reg::GlobalPulseConf);
            old_width = ReadReg(destination, Reg::GlobalPulseWidth);
        }

        WriteReg(destination, Reg::GlobalPulseConf, route_mask);
        WriteReg(destination, Reg::GlobalPulseWidth, width);

        SendCommand<RD53BCmd::GlobalPulse>(destination);

        WriteReg(destination, Reg::GlobalPulseConf, old_route_mask);
        WriteReg(destination, Reg::GlobalPulseWidth, old_width);
    }

    // void StartPRBSpattern(Chip* pChip) {
    //   WriteReg(pChip, RD53B::vRegs.at("EnablePRBS").at(0).reg,1);
    //   WriteReg(pChip, Reg::SER_SEL_OUT, 0xaa); // PRBS
    // }

    // void StopPRBSpattern(Chip* pChip) {
    //   WriteReg(pChip, Reg::SER_SEL_OUT, 0x0055); // Aurora
    // }

    void StartPRBSpattern(Ph2_HwDescription::ReadoutChip* pChip)
    {
        WriteReg(pChip, RD53B::vRegs.at("EnablePRBS").at(0).reg, 1);
        WriteReg(pChip, Reg::SER_SEL_OUT, 0xaa); // PRBS
    }
    void StopPRBSpattern(Ph2_HwDescription::ReadoutChip* pChip)
    {
        WriteReg(pChip, Reg::SER_SEL_OUT, 0x0055); // Aurora
    }

    RD53FWInterface& setup(const Ph2_HwDescription::FrontEndDescription* device)
    {
        this->setBoard(device->getBeBoardId());
        return *static_cast<RD53FWInterface*>(fBoardFW);
    }

    RD53FWInterface& setup(const BeBoard* pBoard)
    {
        this->setBoard(pBoard->getId());
        return *static_cast<RD53FWInterface*>(fBoardFW);
    }

    void ResetCoreColumns(Chip* chip);
    // void UpdateCoreColumns(Chip* chip);
    // void UpdateCoreColRegs(
    //     Chip* chip,
    //     const std::string& prefix,
    //     const std::array<bool, RD53B::nCoreCols>& core_col_en
    // );

    // ###########################
    // # Dedicated to minitoring #
    // ###########################
    void ReadChipMonitor(ReadoutChip* pChip, const std::vector<std::string>& args)
    {
        for(const auto& arg: args) ReadChipMonitor(pChip, arg);
    }
    float    ReadChipMonitor(ReadoutChip* pChip, const std::string& observableName) override { return 0; }
    float    ReadHybridTemperature(ReadoutChip* pChip) override;
    float    ReadHybridVoltage(ReadoutChip* pChip) override;
    uint32_t ReadChipADC(ReadoutChip* pChip, const std::string& observableName);

    // private:
    // uint32_t getADCobservable(const std::string& observableName, bool* isCurrentNotVoltage);
    // uint32_t measureADC(ReadoutChip* pChip, uint32_t data);
    // float    measureVoltageCurrent(ReadoutChip* pChip, uint32_t data, bool isCurrentNotVoltage);
    // float    measureTemperature(ReadoutChip* pChip, uint32_t data);
    // float    convertADC2VorI(ReadoutChip* pChip, uint32_t value, bool isCurrentNotVoltage = false);
};

} // namespace Ph2_HwInterface

#endif
