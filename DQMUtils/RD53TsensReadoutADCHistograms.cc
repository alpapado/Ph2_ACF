/*!
  \file                  RD53TsensReadoutADCHistograms.cc
  \brief                 Implementation of TsensReadoutADC histograms
  \author                Umberto MOLINATTI
  \version               1.0
  \date                  04/07/22
  Support:               email to umberto.molinatti@cern.ch
*/

#include "RD53TsensReadoutADCHistograms.h"

#include <boost/filesystem.hpp>
#include "Utils/xtensor/xadapt.hpp"
#include "Utils/xtensor/xcsv.hpp"


using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;

void TsensReadoutADCHistograms::fillTRA(const double (&temperature)[10])
{
	TFile *file = new TFile("Results/temperature.root","UPDATE");
	static std::vector<const char*> tNames = {"Poly TEMPSENS top","Poly TEMPSENS bottom","RADSENS Ana. SLDO","TEMPSENS Ana. SLDO","RADSENS Dig. SLDO","TEMPSENS Dig. SLDO","RADSENS center","TEMPSENS center","NTC","MUX_NTC"};
    
    static const std::string fileName = "Results/temperature.csv";
    std::ofstream outFile;
    if (boost::filesystem::exists(fileName))
        outFile.open(fileName, std::ios_base::app);
    else {
        outFile.open(fileName);
        outFile << "time, ";
        for (size_t i = 0; i < tNames.size() - 1; ++i)
            outFile << tNames[i] << ", ";
        outFile << tNames.back() << '\n';
    }
    auto now = time(0);
    outFile << std::put_time(std::localtime(&now), "%Y-%m-%d_%H:%M:%S, ");
    xt::dump_csv(outFile, xt::adapt(temperature, {1, 10}));

	for(int sensor=0;sensor<10;sensor++){
		TGraph* countPlot = NULL;
		if(file->GetListOfKeys()->Contains(tNames[sensor])){
			countPlot = (TGraph*)file->Get(tNames[sensor]);
		}else{
			countPlot = new TGraph (1);
			countPlot->SetTitle(tNames[sensor]);		
			countPlot->SetName(tNames[sensor]);		
		}
		countPlot->SetPoint(countPlot->GetN(),(countPlot->GetN()-1),temperature[sensor]);
		countPlot->Write("",TObject::kOverwrite);
	}
	file->Write();
    file->Close();
}
