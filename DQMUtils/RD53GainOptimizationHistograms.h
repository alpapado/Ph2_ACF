/*!
  \file                  RD53GainOptimizationHistograms.h
  \brief                 Header file of Gain optimization calibration histograms
  \author                Mauro DINARDO
  \version               1.0
  \date                  28/06/18
  Support:               email to alkiviadis.papadopoulos@cern.ch
  Support:               email to mauro.dinardo@cern.ch
*/

#ifndef RD53GainOptimizationHistograms_H
#define RD53GainOptimizationHistograms_H

#include "DQMHistogramBase.h"
#include "Utils/ContainerFactory.h"

#include <TH1F.h>

class GainOptimizationHistograms : public DQMHistogramBase
{
  public:
    void book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_Parser::SettingsMap& settingsMap) override;
    void process() override;
    bool fill(std::string& inputStream) override;
    void reset() override{};

    void fill(const DetectorDataContainer& DataContainer);

  private:
    DetectorContainer* fDetectorContainer;

    DetectorDataContainer KrumCurr;
};

#endif
