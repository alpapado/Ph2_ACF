#ifndef THRESHOLDESTIMATOR_H
#define THRESHOLDESTIMATOR_H

#include "HWDescription/RD53BProd.h"
#include "RD53BEventDecoding.h"

#include "Utils/xtensor/xmath.hpp"
#include "Utils/xtensor/xsort.hpp"
#include "Utils/xtensor/xtensor.hpp"
#include "Utils/xtensor/xview.hpp"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
#include "Utils/xtensor-blas/xlinalg.hpp"
#pragma GCC diagnostic pop
#include "Utils/xtensor/xnorm.hpp"

double normal_pdf(double x);

double normal_cdf(double x);

double theta3(double x);
double theta4(double x);
double theta5(double x);
double theta6(double x);

struct ThresholdEstimatorResults
{
    ThresholdEstimatorResults(){};

    ThresholdEstimatorResults(size_t nRows, size_t nCols, size_t nVcalBins);

    xt::xtensor<float, 3>  occupancy;
    xt::xtensor<float, 3>  occupancyRaw;
    xt::xtensor<double, 2> threshold;
    xt::xtensor<double, 2> noise;
    xt::xtensor<double, 2> pseudoR2;
    xt::xtensor<double, 2> threshold_diff;
    xt::xtensor<double, 2> noise_diff;
};

struct ThresholdEstimator
{
    ThresholdEstimator(const xt::xtensor<size_t, 1>& vcalBins, size_t nInjections, size_t triggerDuration, double epsilon, std::array<size_t, 2> size, std::array<size_t, 2> offset = {0, 0});

    template <class Events, class Mask>
    void operator()(const Events& events, const Mask& enabled, ThresholdEstimatorResults& results) const;

  private:
    xt::xtensor<double, 2> X;
    xt::xtensor<double, 2> XX_inv;
    xt::xtensor<size_t, 1> vcalBins;
    xt::xtensor<size_t, 1> vcalDiffBins;
    std::array<size_t, 2>  size;
    std::array<size_t, 2>  offset;
    double                 standardizationOffset;
    double                 standardizationScale;
    size_t                 nInjections;
    size_t                 triggerDuration;
    double                 epsilon;
};

inline ThresholdEstimator::ThresholdEstimator(const xt::xtensor<size_t, 1>& vcalBins,
                                              size_t                        nInjections,
                                              size_t                        triggerDuration,
                                              double                        epsilon,
                                              std::array<size_t, 2>         size,
                                              std::array<size_t, 2>         offset)
    : vcalBins(vcalBins)
    , size(size)
    , offset(offset)
    , standardizationOffset(xt::mean(vcalBins)())
    , standardizationScale(xt::stddev(vcalBins)())
    , nInjections(nInjections)
    , triggerDuration(triggerDuration)
    , epsilon(epsilon)
{
    X                         = xt::ones<double>({vcalBins.size(), 2ul});
    xt::col(X, 1)             = (vcalBins - standardizationOffset) / standardizationScale;
    xt::xtensor<double, 2> XX = xt::linalg::dot(xt::transpose(X), X);
    XX_inv                    = xt::linalg::inv(XX);
    // auto vcalStep = vcalBins(1) - vcalBins(0);
    // vcalDiffBins = xt::view(vcalBins + vcalStep / 2.0, xt::range(0, vcalBins.size() - 1));
    vcalDiffBins = xt::view(vcalBins, xt::range(0, vcalBins.size() - 1)) + (xt::view(vcalBins, xt::range(1, vcalBins.size())) - xt::view(vcalBins, xt::range(0, vcalBins.size() - 1))) / 2.0;
}

template <class Events, class Mask>
inline void ThresholdEstimator::operator()(const Events& events, const Mask& enabled, ThresholdEstimatorResults& results) const
{
    using Vec2   = xt::xtensor_fixed<double, xt::xshape<2>>;
    using Mat2x2 = xt::xtensor_fixed<double, xt::xshape<2, 2>>;

    // auto nTriggers = std::min(maxTriggerId + 1, triggerDuration);

    for(size_t i = 0; i < vcalBins.size(); ++i)
    {
        if(triggerDuration > 1)
        {
            xt::xtensor<int, 3> hitCount = xt::zeros<int>({size[0], size[1], triggerDuration});
            for(const auto& event: xt::view(events, i, xt::all()))
            {
                for(const auto& hit: event.hits)
                {
                    auto row = hit.row - offset[0];
                    auto col = hit.col - offset[1];
                    if(row < 0 || row >= size[0] || col < 0 || col >= size[1] || !enabled(row, col))
                    {
                        // std::cout << "Warning: hit in unused pixel" << std::endl;
                        continue;
                    }
                    ++hitCount(row, col, event.triggerId % triggerDuration);
                }
            }

            // using namespace xt::placeholders;
            auto adjacentSums = xt::view(hitCount, xt::all(), xt::all(), xt::range(0, triggerDuration - 1)) + xt::view(hitCount, xt::all(), xt::all(), xt::range(1, triggerDuration));

            auto maxSum        = xt::amax(adjacentSums, {2});
            auto bestTriggerId = xt::argmax(adjacentSums, 2);

            for(const auto& event: xt::view(events, i, xt::all()))
            {
                for(const auto& hit: event.hits)
                {
                    auto triggerId = event.triggerId % triggerDuration;
                    auto row       = hit.row - offset[0];
                    auto col       = hit.col - offset[1];
                    if(row < 0 || row >= size[0] || col < 0 || col >= size[1]) continue;
                    results.occupancyRaw(row, col, i) += 1. / nInjections;
                    if(enabled(row, col) && (triggerId == bestTriggerId(row, col) || triggerId == bestTriggerId(row, col) + 1)) results.occupancy(row, col, i) += 1. / nInjections;
                }
            }
        }
        else
        {
            for(const auto& event: xt::view(events, i, xt::all()))
            {
                for(const auto& hit: event.hits)
                {
                    results.occupancy(hit.row - offset[0], hit.col - offset[1], i) += 1. / nInjections;
                    results.occupancyRaw(hit.row - offset[0], hit.col - offset[1], i) += 1. / nInjections;
                }
            }
        }
    }

    for(const auto& pixelPosition: xt::argwhere(enabled))
    {
        size_t row = pixelPosition[0];
        size_t col = pixelPosition[1];

        xt::xtensor<float, 1> pixelOcc = xt::clip(xt::view(results.occupancy, row, col, xt::all()), 0.0, 1.0);

        if(pixelOcc.periodic(-1) < 0.1 || pixelOcc(0) > 0.9)
        {
            results.pseudoR2(row, col) = 0;
            // std::cout << "misbehaving: " << row << ", " << col << " (" << pixelOcc << ")" << std::endl;
            continue;
        }

        bool converged = false;
        Vec2 theta     = xt::linalg::dot(XX_inv, xt::linalg::dot(xt::transpose(X), pixelOcc));

        for(size_t iter = 0; iter < 200; ++iter)
        {
            Vec2   grad    = xt::zeros<double>({2});
            Mat2x2 hessian = xt::zeros<double>({2, 2});

            for(size_t i = 0; i < vcalBins.size(); ++i)
            {
                double k = std::min(1.0f, pixelOcc(i)) * nInjections;
                double l = nInjections - k;
                double s = xt::linalg::dot(xt::row(X, i), theta)();
                grad += xt::row(X, i) * (k * theta3(s) - l * theta4(s));
                hessian -= xt::linalg::outer(xt::row(X, i), xt::row(X, i)) * (k * theta5(s) + l * theta6(s));
            }

            if(xt::norm_l2(grad)() < epsilon)
            {
                converged = true;
                break;
            }

            Vec2 step;
            try
            {
                step = xt::linalg::solve(hessian, -grad);
            }
            catch(std::runtime_error& e)
            {
                break;
            }

            theta += step;
        }
        if(!converged)
        {
            std::cout << "not converging: " << row << ", " << col << std::endl;
            continue;
        }

        double sigma = 1 / theta(1);
        double mean  = -theta(0) * sigma;

        double nullModelP = xt::mean(xt::view(pixelOcc, xt::all()))();

        double LLmodel = 0;
        double LLnull  = 0;

        for(size_t i = 0; i < vcalBins.size(); ++i)
        {
            double p = std::max(1e-6, std::min(1 - 1e-6, normal_cdf(xt::linalg::dot(xt::row(X, i), theta)())));
            LLmodel += pixelOcc(i) * log(p) + (1 - pixelOcc(i)) * log(1 - p);
            LLnull += pixelOcc(i) * log(nullModelP) + (1 - pixelOcc(i)) * log(1 - nullModelP);
        }

        double pseudoR2 = 1 - LLmodel / LLnull;

        results.threshold(row, col) = mean * standardizationScale + standardizationOffset;
        results.noise(row, col)     = sigma * standardizationScale;
        results.pseudoR2(row, col)  = pseudoR2;
    }

    for(const auto& pixelPosition: xt::argwhere(enabled))
    {
        size_t                row      = pixelPosition[0];
        size_t                col      = pixelPosition[1];
        xt::xtensor<float, 1> pixelOcc = xt::clip(xt::view(results.occupancy, row, col, xt::all()), 0.0, 1.0);

        auto weights                     = xt::diff(pixelOcc);
        results.threshold_diff(row, col) = xt::average(vcalDiffBins, xt::abs(weights))();
        results.noise_diff(row, col)     = sqrt(xt::average(xt::square(vcalDiffBins - results.threshold_diff(row, col)), xt::abs(weights))());
    }
}

#endif