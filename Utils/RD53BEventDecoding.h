#ifndef RD53BEVENTDECODING_H
#define RD53BEVENTDECODING_H

#include "AsyncWorkerPool.h"

#include <condition_variable>
#include <future>
#include <map>
#include <ostream>
#include <thread>
#include <vector>

namespace RD53BEventDecoding
{
struct Hit
{
    Hit() {}
    Hit(uint16_t row, uint16_t col, uint8_t tot) : row(row), col(col), tot(tot) {}

    friend std::ostream& operator<<(std::ostream& os, const Hit& e) { return os << "{ row: " << +e.row << ", col: " << +e.col << ", tot: " << +e.tot << " }"; }

    uint16_t row;
    uint16_t col;
    uint8_t  tot;
};

struct RD53BEvent
{
    RD53BEvent() {}

    RD53BEvent(size_t triggerId, std::vector<Hit> hits) : hits(std::move(hits)), triggerId(triggerId) {}

    RD53BEvent(size_t triggerId, size_t BCID, size_t triggerTag, std::vector<Hit> hits) : hits(std::move(hits)), triggerId(triggerId), BCID(BCID), triggerTag(triggerTag) {}

    std::vector<Hit> hits;
    uint32_t         triggerId  = 0;
    uint16_t         BCID       = 0;
    uint8_t          triggerTag = 0;
};

struct RD53BChipEvent
{
    std::vector<Hit> hits;
    uint16_t         BCID = 0;
    uint16_t         triggerId = 0;
    uint8_t          hybridId = 0;
    uint8_t          chipLane = 0;
    uint8_t          triggerTag = 0;
    uint8_t          chipIdMod4 = 0;
};

struct RD53BEventContainer
{
    std::vector<RD53BChipEvent> events;
    uint32_t                    BCID = 0;
    uint32_t                    l1a_counter = 0;
    uint16_t                    tlu_trigger_id = 0;
    uint16_t                    triggerTag = 0;
    uint8_t                     tdc = 0;
};

template <class Flavor>
size_t decode_events(const std::vector<uint32_t>& data, std::vector<RD53BEventContainer>& events, const typename Flavor::FormatOptions& options = {});

template <class Flavor>
size_t
decode_events(std::vector<uint32_t>::const_iterator begin, std::vector<uint32_t>::const_iterator end, std::vector<RD53BEventContainer>& events, const typename Flavor::FormatOptions& options = {});

template <class Flavor>
std::vector<RD53BEventContainer> decode_events(std::vector<uint32_t>::const_iterator begin, std::vector<uint32_t>::const_iterator end, const typename Flavor::FormatOptions& options = {}) {
    std::vector<RD53BEventContainer> events;
    decode_events<Flavor>(begin, end, events, options);
    return events;
}

std::pair<size_t, size_t> count_events(const std::vector<uint32_t>& data);

std::vector<size_t> event_start(const std::vector<uint32_t>& data);

struct DecodingResult
{
    DecodingResult() : _valid(false) {}
    DecodingResult(std::vector<RD53BEventContainer>&& value) : _value(std::move(value)), _valid(true) {}
    DecodingResult(std::string&& error) : _error(std::move(error)), _valid(false) {}

    DecodingResult& operator=(std::vector<RD53BEventContainer>&& value)
    {
        _value = std::move(value);
        _valid = true;
        return *this;
    }

    DecodingResult& operator=(std::string&& error)
    {
        _error = std::move(error);
        _valid = false;
        return *this;
    }

    operator bool() const { return _valid; }

    auto& error() { return _error; }

    auto& value() { return _value; }

  private:
    std::string                      _error;
    std::vector<RD53BEventContainer> _value;
    bool                             _valid;
};

template <class Flavor>
struct AsyncEventDecoder : AsyncWorkerPool<DecodingResult>
{
    AsyncEventDecoder(size_t nThreads = std::thread::hardware_concurrency(), const typename Flavor::FormatOptions& options = {}) : AsyncWorkerPool<DecodingResult>(nThreads), options(options) {}

    std::future<DecodingResult> decode_events(std::vector<uint32_t>&& data)
    {
        return enqueue_task([&, data = std::move(data)]() {
            std::vector<RD53BEventContainer> events;
            try
            {
                RD53BEventDecoding::decode_events<Flavor>(data, events, options);
                return DecodingResult{std::move(events)};
            }
            catch(std::runtime_error& e)
            {
                return DecodingResult{e.what()};
            }
        });
    }

  private:
    typename Flavor::FormatOptions options;
};

} // namespace RD53BEventDecoding

#endif