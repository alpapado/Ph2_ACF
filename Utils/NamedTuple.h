#ifndef NAMEDTUPLE_H
#define NAMEDTUPLE_H

#include <functional>
#include <iomanip>
#include <ostream>
#include <regex>
#include <tuple>
#include <vector>

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "ostream_indent.h"

#ifndef COMPILETIMESTRINGLITERAL
#define COMPILETIMESTRINGLITERAL

namespace compile_time_string_literal
{
template <char... Chars>
struct c_str
{
    static constexpr size_t size                        = sizeof...(Chars);
    static constexpr char   value[sizeof...(Chars) + 1] = {Chars..., 0};
};

template <char... Chars>
constexpr size_t c_str<Chars...>::size;

template <char... Chars>
constexpr char c_str<Chars...>::value[];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

template <typename Char, Char... Cs>
constexpr c_str<Cs...> operator"" _s()
{
    return {};
}

#pragma GCC diagnostic pop

} // namespace compile_time_string_literal

#endif

namespace NamedTuple
{
namespace detail
{
// constexpr variant of std::find
template <class InputIt, class T, class Cmp = std::equal_to<>>
constexpr InputIt find(InputIt first, InputIt last, const T& value, Cmp&& cmp = {})
{
    for(; first != last; ++first)
        if(std::forward<Cmp>(cmp)(*first, value)) return first;
    return last;
}

constexpr bool strings_equal(char const* a, char const* b) { return *a == *b && (*a == '\0' || strings_equal(a + 1, b + 1)); }

// flattens and prints str if it fits the console otherwise just prints str
inline std::ostream& print(std::ostream& os, const std::string& str)
{
    using namespace ostream_indent;

    struct winsize window_size;
    size_t         width;
    int            status = ioctl(STDOUT_FILENO, TIOCGWINSZ, &window_size);
    if(status < 0)
        width = 100;
    else
        width = window_size.ws_col;
    static const std::regex newline_re("\\n");
    static const std::regex whitespace_chain_re("\\s\\s+");

    std::string flattened = std::regex_replace(std::regex_replace(str, newline_re, " "), whitespace_chain_re, " ");

    if(4 * os.iword(get_indent_index()) + flattened.size() < width)
        return os << flattened;
    else
        return os << str;
}

template <class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    using namespace ostream_indent;
    if(vec.size() == 0) return os << "[]";
    std::ostringstream ss;
    ss.iword(get_indent_index()) = os.iword(get_indent_index());
    ss << '[' << increase_indent << endl_indent;
    for(int i = 0; i < (int)vec.size() - 1; ++i) { ss << vec[i] << ',' << endl_indent; }
    ss << vec.back() << decrease_indent << endl_indent;
    ss << ']';
    return print(os, ss.str());
}

template <class T, class U>
std::ostream& operator<<(std::ostream& os, const std::map<T, U>& map)
{
    using namespace ostream_indent;
    if(map.size() == 0) return os << "{}}";
    std::ostringstream ss;
    ss.iword(get_indent_index()) = os.iword(get_indent_index());
    ss << '{' << increase_indent << endl_indent;
    for(const auto& item: map) { ss << item.first << " = " << item.second << ',' << endl_indent; }
    ss << decrease_indent << endl_indent;
    ss << '}';
    return print(os, ss.str());
}

} // namespace detail

struct NamedTupleBase
{
};

template <class... Fields>
struct NamedTuple : public NamedTupleBase
{
    using names_tuple = std::tuple<typename Fields::first_type...>;
    using value_type  = std::tuple<typename Fields::second_type...>;

    static constexpr size_t size = sizeof...(Fields);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
    static constexpr const char* names[size] = {Fields::first_type::value...};
#pragma GCC diagnostic pop
    // static constexpr const char* names[sizeof...(Fields)] = {Fields::first_type::value...};

    template <size_t I>
    using field_type = std::tuple_element_t<I, value_type>;

    constexpr NamedTuple() {}

    template <size_t Size = size, class... Us, std::enable_if_t<(Size > 0 && std::is_constructible<value_type, Us...>::value), int> En = 0>
    constexpr NamedTuple(Us&&... values) : _values(std::forward<Us>(values)...)
    {
    }

    template <size_t Size = size, std::enable_if_t<(Size > 0), int> En = 0>
    constexpr NamedTuple(const typename Fields::second_type&... values) : _values(values...)
    {
    }

    template <class Name>
    static constexpr size_t index = detail::find(std::begin(names), std::end(names), Name::value, detail::strings_equal) - std::begin(names);

    template <class Name>
    constexpr auto& operator[](Name)
    {
        return std::get<index<Name>>(_values);
    }

    template <class Name>
    constexpr const auto& operator[](Name) const
    {
        return std::get<index<Name>>(_values);
    }

    constexpr value_type&       values() { return _values; }
    constexpr const value_type& values() const { return _values; }

    template <class F, size_t... Is>
    static void for_each_index(F&& f, std::index_sequence<Is...>)
    {
        int unused[] = {0, (std::forward<F>(f)(std::integral_constant<size_t, Is>{}), 0)...};
        (void)unused;
    }

    template <class F>
    static void for_each_index(F&& f)
    {
        for_each_index(std::forward<F>(f), std::make_index_sequence<size>());
    }

    template <class F>
    bool visit(const std::string& name, F&& f) const
    {
        bool found = false;
        for_each([&](const auto& field_name, auto& value) {
            if(!found && field_name.value == name)
            {
                std::forward<F>(f)(value);
                found = true;
            }
        });
        return found;
    }

    template <class T, size_t... Is>
    static constexpr size_t index_of(std::index_sequence<Is...>)
    {
        size_t i        = size;
        size_t unused[] = {0, (i = (i == size && std::is_same<std::tuple_element_t<Is, value_type>, std::decay_t<T>>::value) ? Is : i)...};
        return i;
    }

    template <class T>
    static constexpr size_t index_of()
    {
        return index_of<T>(std::make_index_sequence<size>{});
    }

    friend std::ostream& operator<<(std::ostream& os, const NamedTuple& t)
    {
        using namespace detail;
        using namespace ostream_indent;

        std::ostringstream ss;
        ss.iword(get_indent_index()) = os.iword(get_indent_index());

        size_t max_size = 0;
        for_each(t, [&](auto name, const auto& value) { max_size = std::max(max_size, name.size); });

        ss << '{' << increase_indent << endl_indent;
        int i = 0;
        for_each(t, [&](auto name, const auto& value) {
            // os << '\"'<< name.value << "\" : " << value;
            ss << std::left << std::setw(max_size) << name.value << " = " << value;
            if(++i != size)
                ss << ',';
            else
                ss << decrease_indent;
            ss << endl_indent;
        });
        ss << '}';

        return print(os, ss.str());
    }

    template <class T>
    using name_of = std::tuple_element_t<index_of<T>(), names_tuple>;

  private:
    value_type _values;
};

template <class... Names, class... Ts>
constexpr auto make_named_tuple(const std::pair<Names, Ts>&... fields)
{
    return NamedTuple<std::pair<Names, Ts>...>(fields.second...);
}

template <class T, class Name>
constexpr std::pair<Name, T> tuple_field(Name, T&& value = {})
{
    return {Name{}, std::forward<T>(value)};
}

template <class Tuple, class F>
void for_each(Tuple&& tuple, F&& f)
{
    using T = std::remove_reference_t<Tuple>;
    T::for_each_index([&](auto index) {
        constexpr auto i = decltype(index)::value;
        std::forward<F>(f)(std::tuple_element_t<i, typename T::names_tuple>{}, std::get<i>(std::forward<Tuple>(tuple).values()));
    });
}

template <size_t I, class T>
auto get(T&& tuple)
{
    return std::get<I>(std::forward<T>(tuple).values());
}

template <class T, class F>
auto visit(T&& t, const std::string& name, F&& f)
{
    bool found = false;
    for_each(std::forward<T>(t), [&](const auto& field_name, auto& value) {
        if(!found && field_name.value == name)
        {
            std::forward<F>(f)(value);
            found = true;
        }
    });
    return found;
}

} // namespace NamedTuple

#endif