#ifndef ASYNCWORKERPOOL_H
#define ASYNCWORKERPOOL_H

#include "concurrentqueue.h"

#include <future>
#include <thread>
#include <vector>

template <class T>
struct AsyncWorkerPool
{
    AsyncWorkerPool(size_t nThreads = std::thread::hardware_concurrency(), size_t maxQueueSize = 0) : ptok(tasks), done(false), maxQueueSize(maxQueueSize)
    {
        threads.reserve(nThreads);
        for(size_t i = 0; i < nThreads; ++i) threads.emplace_back(&AsyncWorkerPool::loop, std::ref(*this));
    }

    ~AsyncWorkerPool() { join(); }

    template <class F>
    std::future<T> enqueue_task(F&& f)
    {
        while(maxQueueSize > 0 && tasks.size_approx() > maxQueueSize) std::this_thread::sleep_for(std::chrono::microseconds(25));
        std::packaged_task<T()> task(std::forward<F>(f));
        std::future<T>          result = task.get_future();
        tasks.enqueue(ptok, std::move(task));
        return result;
    }

    void join()
    {
        done = true;
        for(auto& t: threads)
            if(t.joinable()) t.join();
    }

  private:
    void loop()
    {
        std::packaged_task<T()> task;
        while(!done)
        {
            if(tasks.try_dequeue_from_producer(ptok, task))
                task();
            else
                std::this_thread::sleep_for(std::chrono::nanoseconds(1));
        }
    }

    std::vector<std::thread>                             threads;
    moodycamel::ConcurrentQueue<std::packaged_task<T()>> tasks;
    moodycamel::ProducerToken                            ptok;
    std::atomic<bool>                                    done;
    size_t                                               maxQueueSize;
};

#endif