#include "ThresholdEstimator.h"

#include "Utils/xtensor/xhistogram.hpp"
#include "Utils/xtensor/xindex_view.hpp"
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xmath.hpp"
#include "Utils/xtensor/xoperation.hpp"
#include "Utils/xtensor/xstrided_view.hpp"
#include "Utils/xtensor/xview.hpp"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
#include "Utils/xtensor-blas/xlinalg.hpp"
#pragma GCC diagnostic pop
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xnorm.hpp"

#include <math.h>

using namespace RD53BEventDecoding;
using namespace Ph2_HwDescription;

double normal_pdf(double x) { return std::exp(-x * x / 2) / std::sqrt(2 * M_PI); }

double normal_cdf(double x) { return (1 + std::erf(x / sqrt(2))) / 2; }

// probit model implementation based on: Demidenko, E. (2001). Computational aspects of probit model. Mathematical Communications, 6, 233-247.
double theta3(double x)
{
    if(x < -5)
        return -x;
    else if(x <= 5)
        return normal_pdf(x) / normal_cdf(x);
    else
        return normal_pdf(x);
}

double theta4(double x)
{
    if(x < -5)
        return normal_pdf(x);
    else if(x <= 5)
        return normal_pdf(x) / (1 - normal_cdf(x));
    else
        return x;
}

double theta5(double x)
{
    if(x < -5)
        return 0;
    else
    {
        double fx = normal_pdf(x);
        if(x <= 5)
        {
            double Fx    = normal_cdf(x);
            double ratio = fx / Fx;
            return ratio * (x + ratio);
        }
        else
            return fx * (x + fx);
    }
}

double theta6(double x)
{
    if(x <= 5)
    {
        double fx = normal_pdf(x);
        if(x < -5)
            return fx * (fx - x);
        else
        {
            double Fx    = normal_cdf(x);
            double ratio = fx / (1 - Fx);
            return ratio * (ratio - x);
        }
    }
    else
        return 0;
}

ThresholdEstimatorResults::ThresholdEstimatorResults(size_t nRows, size_t nCols, size_t nVcalBins)
{
    occupancy      = xt::zeros<float>({nRows, nCols, nVcalBins});
    occupancyRaw   = xt::zeros<float>({nRows, nCols, nVcalBins});
    threshold      = xt::zeros<double>({nRows, nCols});
    noise          = xt::zeros<double>({nRows, nCols});
    pseudoR2       = xt::zeros<double>({nRows, nCols});
    threshold_diff = xt::zeros<double>({nRows, nCols});
    noise_diff     = xt::zeros<double>({nRows, nCols});
}
