#ifndef RD53Base_H
#define RD53Base_H

#include "ReadoutChip.h"

namespace Ph2_HwDescription
{
class RD53Base : public ReadoutChip
{
  public:
    RD53Base(const RD53Base& chipObj) : ReadoutChip(chipObj) {}

    RD53Base(uint8_t pBeId, uint8_t pFMCId, uint8_t pOpticalGroupId, uint8_t pHybridId, uint8_t pRD53Id, uint8_t pRD53Lane = 0) : ReadoutChip(pBeId, pFMCId, pOpticalGroupId, pHybridId, pRD53Id)
    //   , _comment(std::move(comment))
    {
        chipLane = pRD53Lane;
    }

    // virtual void    copyMaskToDefault() {}

    virtual size_t getNbMaskedPixels() const = 0;
    virtual bool isPrimary() const = 0;
    virtual uint8_t getPrimary() const = 0;

    uint8_t getChipLane() const { return chipLane; }
    const auto& comment() const { return _comment; }

  private:
    std::string _comment;
    uint8_t     chipLane;
};

} // namespace Ph2_HwDescription

#endif