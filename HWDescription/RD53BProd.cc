/*!
  \file                  RD53B.cc
  \brief                 RD53B implementation class, config of the RD53B
  \author                Mauro DINARDO
  \version               1.0
  \date                  28/06/18
  Support:               email to mauro.dinard@cern.ch
*/

#include "RD53BProd.h"
#include "Utils/FilesystemUtils.h"

#include <boost/filesystem.hpp>

#include "Utils/xtensor/xadapt.hpp"
#include "Utils/xtensor/xcsv.hpp"
#include "Utils/xtensor/xio.hpp"

#include <regex>
#include <sstream>

namespace Ph2_HwDescription
{
namespace RD53BFlavor
{
constexpr char ATLAS::name[];
constexpr char ATLAS::feTypeName[];
constexpr char CMS::name[];
constexpr char CMS::feTypeName[];
constexpr char ITkPixV2::name[];
constexpr char ITkPixV2::feTypeName[];
constexpr char CROCv2::name[];
constexpr char CROCv2::feTypeName[];
} // namespace RD53BFlavor

RD53B_Base::LaneConfig::LaneConfig() : _outputLaneMapping({0, 1, 2, 3}), _inputLaneMapping({0, 1, 2, 3}), _internalLanesEnabled({0, 0, 0, 0, 0}), _nOutputLanes(1), _isPrimary(true) {}

RD53B_Base::LaneConfig::LaneConfig(bool isPrimary, uint8_t primary, const std::array<uint8_t, 4>& outputLanes, const std::array<bool, 4>& signleChannelInputLanes, const std::array<bool, 4>& dualChannelInputLanes)
    : _outputLaneMapping({0, 0, 0, 0})
    , _inputLaneMapping({0, 0, 0, 0})
    , _internalLanesEnabled({0, 0, 0, 0, 0})
    , _nOutputLanes(std::count_if(outputLanes.begin(), outputLanes.end(), [](auto x) { return x > 0; }))
    , _primary(primary)
    , _isPrimary(isPrimary)
{
    static const std::string indexNames[] = {"first", "second", "third", "fourth"};
    // auto nOutputLanes = std::count_if(outputLanes.begin(), outputLanes.end(), [] (auto x) { return x > 0; });
    std::cout << "primary: " << +primary << std::endl;
    std::cout << "nOutputLanes: " << +_nOutputLanes << std::endl;
    for(auto i = 0u; i < 4; ++i)
    {
        if(outputLanes[3 - i] - 1 >= _nOutputLanes)
        {
            std::ostringstream os;
            os << "GTX" << i << " configured to use lane " << +outputLanes[3 - i] << " but only " << _nOutputLanes << " lanes are enabled";
            throw std::runtime_error(os.str());
        }
        if(outputLanes[3 - i] > 0)
        {
            _outputLaneMapping[i] = outputLanes[3 - i] - 1;
            // _outputLanesEnabled[i] = 1;
        }
        else
        {
            _outputLaneMapping[i] = _nOutputLanes;
            // _outputLanesEnabled[i] = 0;
        }
    }

    size_t nBondedChannels = std::count(dualChannelInputLanes.begin(), dualChannelInputLanes.end(), true);
    std::cout << "nBondedChannels: " << nBondedChannels << std::endl;
    if(nBondedChannels > 0)
    {
        if(nBondedChannels != 2) throw std::runtime_error("The dual channel input must use exactly two lanes but " + std::to_string(nBondedChannels) + " were specified");
        _internalLanesEnabled[0] = true;
        auto it                  = std::find(dualChannelInputLanes.rbegin(), dualChannelInputLanes.rend(), true);
        _inputLaneMapping[0]     = it - dualChannelInputLanes.rbegin();
        it                       = std::find(it + 1, dualChannelInputLanes.rend(), true);
        _inputLaneMapping[1]     = it - dualChannelInputLanes.rbegin();
    }

    size_t nSingleChannels = std::count(signleChannelInputLanes.begin(), signleChannelInputLanes.end(), true);
    std::cout << "nSingleChannels: " << nSingleChannels << std::endl;
    if(nSingleChannels > 0)
    {
        if(nBondedChannels + nSingleChannels > 4) throw std::runtime_error("Attempted to enable " + std::to_string(nSingleChannels + nBondedChannels) + "input lanes, but threre are only 4");

        size_t j = nBondedChannels + 1;
        for(auto i = 0u; i < 4; ++i)
        {
            if(signleChannelInputLanes[3 - i])
            {
                _inputLaneMapping[j - 1] = i;
                _internalLanesEnabled[j] = true;
                ++j;
            }
        }
    }

    std::cout << "_outputLaneMapping: " << xt::adapt(_outputLaneMapping) << std::endl;

    // std::cout << "_outputLanesEnabled: " << xt::adapt(_outputLanesEnabled) << std::endl;

    std::cout << "_inputLaneMapping: " << xt::adapt(_inputLaneMapping) << std::endl;

    std::cout << "_internalLanesEnabled: " << xt::adapt(_internalLanesEnabled) << std::endl;
}

template <class Flavor>
RD53BProd<Flavor>::RD53BProd(uint8_t pBeId, uint8_t pFMCId, uint8_t pOpticalGroupId, uint8_t pHybridId, uint8_t pRD53Id, uint8_t pRD53Lane, const std::string& fileName, std::string comment)
    : RD53B_Base(pBeId, pFMCId, pOpticalGroupId, pHybridId, pRD53Id, pRD53Lane)
{
    fMaxRegValue = 0xFFFF;
    // fChipOriginalMask = new ChannelGroup<nRows, nCols>;
    configFileName = fileName;
    setFrontEndType(Flavor::feType);

    RD53BProd::loadfRegMap(configFileName);

    // defaultPixelConfig = pixelConfig;
}

template <class Flavor>
RD53BProd<Flavor>::RD53BProd(const RD53BProd& chipObj) : RD53B_Base(chipObj)
{
}

template <class Flavor>
void RD53BProd<Flavor>::setDefaultState()
{
    for(const auto& reg: Regs) registerValues[reg.address] = reg.defaultValue;
}

template <class Flavor>
uint32_t RD53BProd<Flavor>::getNumberOfChannels() const
{
    return nRows * nCols;
}

template <class Flavor>
bool RD53BProd<Flavor>::isDACLocal(const std::string& regName)
{
    if(regName != "PIX_PORTAL") return false;
    return true;
}

template <class Flavor>
uint8_t RD53BProd<Flavor>::getNumberOfBits(const std::string& regName)
{
    auto it = fRegMap.find(regName);
    if(it == fRegMap.end()) return 0;
    return it->second.fBitSize;
}

template <class Flavor>
void RD53BProd<Flavor>::loadfRegMap(const std::string& fileName)
{
    if(boost::filesystem::exists(fileName))
    {
        config = toml::parse<toml::preserve_comments, tsl::ordered_map>(fileName);

        if(config.contains("Registers"))
        {
            for(const auto& item: config.at("Registers").as_table())
            {
                auto it = vRegs.find(item.first);
                if(it == vRegs.end()) throw std::runtime_error(toml::format_error("Invalid register name", item.second, item.first + " does not match any virtual register name"));
                configureRegister(item.first, item.second.as_integer());
            }
        }

        if(config.contains("Pixels"))
        {
            auto&& pixelsConfig = config.at("Pixels").as_table();
            for(const auto& item: pixelsConfig)
            {
                if(!visit(pixelConfigFields(), item.first, [&](auto ptr) {
                       if(item.second.is_integer())
                           (pixelConfig().*ptr).fill(item.second.as_integer());
                       else
                       {
                           std::string csvFileName = item.second.as_string();
                           // pixelConfigFileNames[fieldName.value] = csvFileName;
                           if(boost::filesystem::exists(csvFileName))
                           {
                               std::ifstream csvFile(csvFileName);
                               auto          data = xt::load_csv<double>(csvFile);
                               if(data.size() == 1)
                                   (pixelConfig().*ptr).fill(data.flat(0));
                               else
                                   pixelConfig().*ptr = data;
                           }
                       }
                   }))
                    throw std::runtime_error(toml::format_error("Invalid pixel configuration field name", item.second, item.first + " does not match any pixel configuration field name"));
            }
        }
    }
}

template <class Flavor>
void RD53BProd<Flavor>::saveConfig()
{
    // std::string fileName = configFileName;
    // size_t slash_position = fileName.rfind('/');
    // fileName.insert(slash_position == std::string::npos ? 0 : slash_position, fName2Add);
    // std::ofstream file(fileName);
    std::ofstream file(configFileName);

    // std::stringstream ss;

    for_each(pixelConfigFields(), [&](const auto& fieldName, auto ptr) {
        const auto& data = pixelConfig().*ptr;

        bool exists    = config.contains("Pixels") && config["Pixels"].contains(fieldName.value);
        bool isUniform = xt::all(xt::equal(data, data(0)));
        bool isDefault = isUniform && data(0) == pixelConfigDefauls().at(fieldName.value);

        if(isDefault)
        {
            if(exists) config["Pixels"].as_table().erase(fieldName.value);
        }
        else
        {
            bool isString = exists && config["Pixels"][fieldName.value].is_string();
            if(isUniform && !isString)
                config["Pixels"][fieldName.value] = (int)data(0);
            else
            {
                std::string csvFileName;
                if(isString)
                    csvFileName = config["Pixels"][fieldName.value].as_string();
                else
                {
                    std::ostringstream csvFileNameStream;
                    csvFileNameStream << fieldName.value << ".csv";
                    csvFileName                       = FSUtils::getAvailableFilePath(csvFileNameStream.str()).string();
                    config["Pixels"][fieldName.value] = csvFileName;
                }
                std::ofstream out_file(csvFileName);
                if(isUniform)
                    out_file << (int)data(0);
                else
                    xt::dump_csv(out_file, xt::cast<int>(pixelConfig().*ptr));
            }
        }
    });

    for(const auto& item: registerConfig) { config["Registers"][item.first] = item.second; }

    file << config;
    // ss << config;
    // return ss;
}

template class RD53BProd<RD53BFlavor::ATLAS>;
template class RD53BProd<RD53BFlavor::CMS>;
template class RD53BProd<RD53BFlavor::ITkPixV2>;
template class RD53BProd<RD53BFlavor::CROCv2>;

} // namespace Ph2_HwDescription
