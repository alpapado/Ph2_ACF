
from datetime import datetime, timedelta
from difflib import restore
import xml.etree.ElementTree as ET
import toml
import sys, os, shutil, copy, time, csv, subprocess, itertools, argparse, importlib, atexit

powerSupplyResource = "TTi"
powerSupplyVoltage = 1.7
powerSupplyCurrent = 2.0

powerSupply=None

defaultConfigFile = "CROC.xml"

defaultUpdateConfig = False

timeout = 3600
maxAttempts = 5

runID = -1
baseDir = 'Runs' #/Run_' + str(runID) if runID >= 0 else datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

# initialDose = 0
# doseRate = 0.5 #Mrad / hour

fmt = "%y_%m_%d-%H_%M_%S_%f"

configFile = "CROC.xml"
toolsFile = "RD53BTools.toml"

chipConfigFiles = ["RD53B.toml"]

doseSchedule = [
#       hours       dose rate       from            to
    [   2.6,        0.616   ], #    12/8-14:36:55   12/8-17:12:55
    [   2.28,       0       ], #    12/8-17:12:55   12/8-18:29:43
    [   20,         0.616   ], #    12/8-18:29:43   13/8-15:29:43
    [   5 / 60.,    0       ], #    13/8-15:29:43   13/8-15:34:43
    [   338.504,    3.08    ], #    13/8-15:34:43   27/8-18:05:00
    [   0,          0       ]  #    27/8-18:05:00   -
]

configs = {}
tasks = []

def dose2timedelta(dose):
    hours = 0
    dose_left = dose
    for intervalHours, doseRate in doseSchedule[:-1]:
        intervalDose = doseRate * intervalHours
        if intervalDose < dose_left:
            hours += intervalHours
        else:
            hours += dose_left / doseRate
            return timedelta(hours=hours)
        dose_left -= intervalDose
    if doseSchedule[-1][1] == 0:
        return (datetime.max - datetime.min)
    hours += dose_left / doseSchedule[-1][1]
    return timedelta(hours=hours)

    # return timedelta(hours=((dose - initialDose) / doseRate))

def timedelta2dose(dt):
    dose = 0
    hours_left = dt.total_seconds() / 3600
    for intervalHours, doseRate in doseSchedule:
        intervalDose = doseRate * intervalHours
        if intervalHours < hours_left:
            dose += intervalDose
        else:
            dose += hours_left * doseRate
            return dose
        hours_left -= intervalHours
    dose += hours_left * doseSchedule[-1][1]
    return dose
    # return initialDose + doseRate * dt.total_seconds() / 3600

def get_nested(nested_container, indices):
    current = nested_container
    for i in indices:
        if i not in current:
            return None
        current = current[i]
    return current

def set_nested(nested_container, indices, value):
    current = nested_container
    for i in indices[:-1]:
        if i not in current:
            current[i] = {}
        current = current[i]
    current[indices[-1]] = value

class TaskLog:
    fieldNames = ['name', 'config', 'start_time', 'end_time', 'start_dose', 'end_dose', 'status', 'failed_attempts', 'output_dir', 'VinA', 'VinD', 'IinA', 'IinD']

    def __init__(self, baseDir):
        self.filePath = os.path.join(baseDir, "log.csv")
        if not os.path.exists(self.filePath):
            with open(self.filePath, 'w', newline='') as f:
                csv.DictWriter(f, self.fieldNames).writeheader()
            self.rows = []
        else:
            with open(self.filePath, newline='') as f:
                self.rows = list(csv.DictReader(f))
    
    def add_row(self, *args):
        print(args)
        row = {self.fieldNames[i] : args[i] for i in range(len(self.fieldNames))}
        self.rows.append(row)
        with open(self.filePath, 'a', newline='') as f:
            csv.DictWriter(f, self.fieldNames).writerow(row)
    

def getTomlFile(configFile):
    tree = ET.parse(configFile)
    root = tree.getroot()
    return next(root.iter("CROC")).attrib["configfile"]

def powerCycle(cwd):
    if powerSupply:
        powerSupply.power_cycle()
    else:
        subprocess.run(["RD53BminiDAQ", "-f", configFile, "-r"], cwd=cwd)
    time.sleep(.5)


def run_task(task, log, elapsed):
    start_time = datetime.now()

    # configFile = task.get('configFile', defaultConafigFile)
    #tomlFile = os.path.join(baseDir, getTomlFile(configFile))
    updateConfig = task.get('updateConfig', defaultUpdateConfig)
    #outputDir = os.path.join("Results", task["name"] + "_" + datetime.now().strftime(fmt))
    outputDir = os.path.join(
        baseDir, 
        "Results", 
        task["name"] + (f'_{timedelta2dose(elapsed):.4f}' if 'doseIterator' in task else '')
    )

    os.makedirs(outputDir, exist_ok=True)

    config = task.get('config', 'main')

    base = os.path.join(baseDir, config)

    # save config
    originalConfig = os.path.join(base, 'original_config')

    def restore_original_config(deleteBackup=True):
        for f in os.listdir(originalConfig):
            shutil.copy(os.path.join(originalConfig, f), os.path.join(base, f))
        shutil.rmtree(originalConfig)

    if not updateConfig and 'params' in task:
        os.makedirs(originalConfig, exist_ok=True)
        for f in os.listdir(base):
            if f.endswith(('.xml', '.csv', '.toml')):
                shutil.copy(os.path.join(base, f), os.path.join(originalConfig, f))
        atexit.register(restore_original_config)

    
    if task.get("powerCycleBefore", False):
        powerCycle(base)

    if "current" in task:
        if powerSupply is None:
            print(f'Task {task["name"]} requires setting the supplied current but the communication with the power supply is disabled')
            print('Proceeding without setting the current...')
        else:
            # powerSupply.power_off()
            powerSupply.set_current(0, task["current"])
            powerSupply.set_current(1, task["current"])
            # powerSupply.power_on()
            time.sleep(.5)

    # set task params and save original values
    # original_values = []
    if "params" in task:
        for p in task['params']:
            tomlData = toml.load(os.path.join(base, p['tomlFile']))
            # if not updateConfig:
            #     original_values.append({})
            for key in p["keys"]:
                print(key[-1])
                # original_values[-1]['.'.join(key)] = get_nested(tomlData, key)
                set_nested(tomlData, key, p['value'])
            with open(os.path.join(base, p['tomlFile']), "w") as f:
                    toml.dump(tomlData, f)

    # save config
    tmpDir = os.path.join(base, 'tmp')

    def delete_tmp():
        # for f in os.listdir(tmpDir2):
        #     shutil.copy(os.path.join(tmpDir2, f), os.path.join(base, f))
        # if deleteBackup:
        shutil.rmtree(tmpDir)

    # if updateConfig:
    os.makedirs(tmpDir, exist_ok=True)
    for f in os.listdir(base):
        if f.endswith(('.xml', '.csv', '.toml')):
            shutil.copy(os.path.join(base, f), os.path.join(tmpDir, f))
    atexit.register(delete_tmp)

    # n_attempts = run_Ph2_ACF(task['tools'], updateConfig, outputDir, base)

    n_attempts = maxAttempts
    for i in range(maxAttempts):
        
        if i >= 1:
            powerCycle(base)
            # if updateConfig:
            #     restore_config2(False)
            
        for f in os.listdir(base):
            if f.endswith(('.xml', '.csv', '.toml')):
                shutil.copy(os.path.join(base, f), os.path.join(tmpDir, f))
        
        # extra_flags = ["-s"] if updateConfig else []

        all_done = True
        for tool in task['tools']:

            cmd = [
                # "gdb",
                # "--args",
                "RD53BminiDAQ", 
                "-f", configFile, 
                "-t", toolsFile,
                "-h",
                "-o", outputDir, 
                # *extra_flags, 
                '-s',
                tool
            ]
            
            print(" ".join(cmd))

            p = subprocess.Popen(cmd, cwd=tmpDir, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            # cmd = ["echo $PWD"]
            # p = subprocess.Popen(cmd, cwd=tmpDir)
            # p = subprocess.Popen(" ".join(cmd), shell=True, cwd=tmpDir, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            # p = subprocess.Popen(" ".join(cmd), shell=True, cwd=tmpDir)
            # p = subprocess.Popen("tput cols", shell=True, cwd=tmpDir, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=os.environ)

            tee = subprocess.Popen(('tee', '-a', f'{outputDir}/{tool}.log'), stdin=p.stdout)

            try:
                returncode = p.wait(timeout=timeout)
                if returncode != 0:
                    all_done = False
                    # input("Press Enter to continue...")
                    break
            except:
                p.terminate()
                all_done = False
                print("timed out")
                break
        if all_done:
            n_attempts = i
            break

    success = n_attempts < maxAttempts

    # if updateConfig:
    atexit.unregister(delete_tmp)
    if updateConfig and success:
        for f in os.listdir(tmpDir):
            shutil.copy(os.path.join(tmpDir, f), os.path.join(base, f))

    shutil.rmtree(tmpDir)

    # restore config
    if not updateConfig and 'params' in task:
        atexit.unregister(restore_original_config)
        restore_original_config()

    if task.get("powerCycleAfter", False):
        powerCycle(base)

    log.add_row(
        task['name'], 
        config,
        start_time.strftime(fmt),
        datetime.now().strftime(fmt),
        timedelta2dose(elapsed),
        timedelta2dose(elapsed + (datetime.now() - start_time)),
        'success' if success else 'fail',
        n_attempts,
        outputDir,
        powerSupply.read_voltage(0).decode().rstrip() if powerSupply else None,
        powerSupply.read_voltage(1).decode().rstrip() if powerSupply else None,
        powerSupply.read_current(0).decode().rstrip() if powerSupply else None,
        powerSupply.read_current(1).decode().rstrip() if powerSupply else None
    )

    if updateConfig and success:
        for parameterSet in configs[config]:
            for otherConfig in parameterSet['configs']:
                print(otherConfig)
                for chipConfigFile in chipConfigFiles:
                    print(chipConfigFile)
                    otherTomlData = toml.load(os.path.join(baseDir, otherConfig, chipConfigFile))
                    tomlData = toml.load(os.path.join(base, chipConfigFile))
                    for key in parameterSet['keys']:
                        value = get_nested(tomlData, key)
                        if value is not None:
                            set_nested(otherTomlData, key, value)
                            if isinstance(value, str):
                                path = os.path.join(base, value)
                                if os.path.exists(path):
                                    shutil.copy(path, os.path.join(baseDir, otherConfig, value))
                    with open(os.path.join(baseDir, otherConfig, chipConfigFile), "w") as f:
                        toml.dump(otherTomlData, f)

            
def run_tasks():
    log = TaskLog(baseDir)

    start_time = datetime.strptime(log.rows[0]["start_time"], fmt) if len(log.rows) else datetime.now()

    while len(tasks) > 0:
        now = datetime.now()
        elapsed = now - start_time

        maxDelay = timedelta(0)
        currentTask = None

        tasksToBeRemoved = []

        for task in tasks:
            doseIterator = copy.deepcopy(task.get('doseIterator', iter([0])))
            currentDose = None
            for currentDose in itertools.takewhile(lambda d: dose2timedelta(d) < elapsed, doseIterator):
                pass            
            nextDose = next(doseIterator, None)
            if currentDose is not None:
                last_time = next((datetime.strptime(row["start_time"], fmt) for row in reversed(log.rows) if row["name"] == task["name"]), None)
                scheduled_time = start_time + dose2timedelta(currentDose)
                if last_time is None or last_time < scheduled_time:
                    delay = now - scheduled_time
                    if delay > maxDelay:
                        currentTask = task
                        maxDelay = delay
                elif nextDose is None:
                    tasksToBeRemoved.append(task)

          
        for task in tasksToBeRemoved:
            print(f"removing task: {task['name']}")
            tasks.remove(task)

        if currentTask is not None:
            run_task(currentTask, log, elapsed)
        else:
            time.sleep(.5)
    print("No tasks left")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--no-instruments', action='store_true', help='Flag to not use the instrument control library')
    parser.add_argument('-t', '--task-list', action='store', help='The python script containing the task list')
    parser.add_argument('-r', '--run-id', action='store', default=None, help='The run ID')
    parser.add_argument('-c', '--config-dir', action='store', default='.', help='The directory containing all the configuration files')
    args = parser.parse_args()

    global powerSupply
    global baseDir
    global configs
    global tasks

    runID = args.run_id or datetime.now().strftime(fmt)

    baseDir = os.path.join(os.getcwd(), baseDir, runID)

    print(baseDir)

    if not os.path.exists(baseDir):
        os.makedirs(baseDir)

    if args.no_instruments:
        powerSupply = None
    else:
        from instrument_control import PowerSupplyController
        powerSupply = PowerSupplyController(powerSupplyResource, 2)

    if powerSupply is not None:
        powerSupply.power_off()
        time.sleep(2)
        # set power supply voltage/current
        powerSupply.set_voltage(0, powerSupplyVoltage)
        powerSupply.set_voltage(1, powerSupplyVoltage)
        powerSupply.set_current(0, powerSupplyCurrent)
        powerSupply.set_current(1, powerSupplyCurrent)
        time.sleep(3)
        powerSupply.power_on()
        time.sleep(.5)

    mod = importlib.import_module(args.task_list)
    
    tasks = mod.tasks

    if 'configs' in dir(mod):
        configs = mod.configs

    for task in mod.tasks:
        config = task.get('config', 'main')
        if config not in configs:
            configs[config] = []

    # copy config files into baseDir
    for config in configs:
        path = os.path.join(baseDir, config)
        if not os.path.exists(path):
            os.makedirs(path)
        for fileName in [configFile, *chipConfigFiles]:
            if not os.path.exists(os.path.join(path, fileName)):
                shutil.copy(os.path.join(args.config_dir, fileName), path)
        shutil.copy(os.path.join(args.config_dir, toolsFile), path)
        # for fileName in os.listdir(args.config_dir):
        #     if fileName.endswith(('.xml', '.csv', '.toml')) and not os.path.exists(os.path.join(path, fileName)):
        #         shutil.copy(os.path.join(args.config_dir, fileName), path)

    run_tasks()

    if powerSupply is not None:
        powerSupply.power_off()


if __name__=='__main__':
    main()
