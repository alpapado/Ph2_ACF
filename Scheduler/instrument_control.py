import serial
import time

class PowerSupplyController:
    def __init__(self, powerSupplyId, outputs):
        self.s = serial.Serial('/dev/ttyUSB1', baudrate=9600, parity='N', xonxoff=True)
        # self.s.connect(("127.0.0.1", 7000))
        # self.powerSupplyId = powerSupplyId
        # self.packetNumber = 0

    # def send(self, msg):
        # length = len(msg) + 8
        # self.s.sendall(length.to_bytes(4, byteorder='big') + self.packetNumber.to_bytes(4, byteorder='big') + msg)
        # self.packetNumber += 1

    def power_on(self):
        self.s.write(b'OP1 1\nOP2 1\n')
        # self.send(f"TurnOn,PowerSupplyId:{self.powerSupplyId},ChannelId:0".encode())
        # self.send(f"TurnOn,PowerSupplyId:{self.powerSupplyId},ChannelId:1".encode())

    def power_off(self):
        self.s.write(b'OP1 0\nOP2 0\n')

    def set_voltage(self, channel, value):
        self.s.write(f'V{channel + 1} {value}\n'.encode())
        # self.send(f"SetVoltage,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel},Voltage:{value}".encode())

    def read_voltage(self, channel):
        self.s.write(f'V{channel + 1}O?\n'.encode())
        return self.s.readline()
        # self.send(f"GetVoltage,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel}".encode())
        # return self.s.recv(1024)

    def set_current(self, channel, value):
        self.s.write(f'I{channel + 1} {value}\n'.encode())
        # self.send(f"SetCurrent,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel},Current:{value}".encode())

    def read_current(self, channel):
        self.s.write(f'I{channel + 1}O?\n'.encode())
        return self.s.readline()
        # self.send(f"GetCurrent,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel}".encode())
        # return self.s.recv(1024)

    def power_cycle(self):
        self.power_off()
        time.sleep(5)
        self.power_on()