#ifndef RD53VrefTrimming_H
#define RD53VrefTrimming_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53VrefTrimming : public RD53BTool<RD53VrefTrimming, Flavor>
{
    using Base = RD53BTool<RD53VrefTrimming, Flavor>;
    using Base::Base;

    struct Results
    {
        double vdddVoltage[16];
        double vddaVoltage[16];
    };

    Results run() const;

    void draw(const Results& results);
};

} // namespace RD53BTools

#endif
