#ifndef RD53BSignalScan_H
#define RD53BSignalScan_H

#include "RD53BInjectionTool.h"

class TTree;

namespace RD53BTools
{
template <class>
struct RD53BSignalScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BSignalScan<Flavor>> = make_named_tuple(std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
                                                                      std::make_pair("GDACRange"_s, std::vector<size_t>({200, 800})),
                                                                      std::make_pair("GDACStep"_s, 20),
                                                                      // std::make_pair("maxDelay"_s, 32u),
                                                                      // std::make_pair("maxFineDelay"_s, 32u),
                                                                      std::make_pair("fineDelayStep"_s, 1u),
                                                                      std::make_pair("analyzerThreads"_s, 1u),
                                                                      std::make_pair("nPlots"_s, 100ul));

template <class Flavor>
struct RD53BSignalScan : public RD53BTool<RD53BSignalScan, Flavor>
{
    using Base = RD53BTool<RD53BSignalScan, Flavor>;
    using Base::Base;
    using Base::param;

    using Result = ChipDataMap<xt::xtensor<float, 4>>;

    void   init();
    Result run(Task progress);
    void   draw(const Result& lateHitRatio);

  private:
    ChipDataMap<TTree*> hitTrees;
    size_t              nGDACSteps;
    size_t              nDelaySteps;
};

} // namespace RD53BTools

#endif