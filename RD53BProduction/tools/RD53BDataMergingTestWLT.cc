#include "RD53BDataMergingTestWLT.h"

#include <string>

namespace RD53BTools {

template <class Flavor>
bool RD53BDataMergingTestWLT<Flavor>::run(Task progress) const {
    //setup
    Ph2_System::SystemController& system = Base::system();
    auto& chipInterface = Base::chipInterface();
    ChipDataMap<uint16_t> dataMergingRegValues;

    //saving the previous chip configuration and setting the one for the test
    for_each_device<Chip>(system, [&] (Chip* chip) {
        dataMergingRegValues.insert({chip, chipInterface.ReadReg(chip, Flavor::Reg::DataMerging)});
        chipInterface.WriteReg(chip, Flavor::Reg::DataMerging, 317);
    });

    //looping on all devices to perform the test
    for_each_device<BeBoard>(system, [&] (BeBoard* board) {
        auto& fwInterface = Base::getFWInterface(board);

        //making sure that the multiplexer for selecting between scan chain and
        //data merging tests is configured correctly
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.enable_tests", 0);

        //resetting the emulator
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.emulator_reset", 1);
        usleep(0.1e6);
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.emulator_reset", 0);
        usleep(0.1e6);

        //configuring the data merging emulator
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.dummy_word", 0xFF);
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.delay", 100);
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.max_rx_data", 1000);
        //fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.shift_en", 0);

        //Aurora configuration to send data from the data merging emulator
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.set_cnfg", 1);
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.set_cnfg", 0);

        //transmitting data
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.enable_tx", 1);
        usleep(0.1e6);
        fwInterface.WriteReg("user.ctrl_regs.data_merging_cnfg.enable_tx", 0);
        usleep(0.1e6);

        //waiting for the completion of the test
        while (fwInterface.ReadReg("user.stat_regs.readout2.data_merging_test_done") == 0) {
            usleep(0.1e6);
        }

        //checking the result of the test
        if (fwInterface.ReadReg("user.stat_regs.readout2.data_merging_error") == 0)
            LOG(INFO) << BOLDGREEN << "Data merging test successful." << RESET;
        else
            LOG(INFO) << BOLDRED << "Data merging test failed." << RESET;
    });

    //restoring the previous configuration
    for_each_device<Chip>(system, [&] (Chip* chip) {
        chipInterface.WriteReg(
            chip,
            Flavor::Reg::DataMerging,
            dataMergingRegValues[chip]
        );
    });

    return true;
}

template class RD53BDataMergingTestWLT<RD53BFlavor::ATLAS>;
template class RD53BDataMergingTestWLT<RD53BFlavor::CMS>;
template class RD53BDataMergingTestWLT<RD53BFlavor::ITkPixV2>;
template class RD53BDataMergingTestWLT<RD53BFlavor::CROCv2>;

}
