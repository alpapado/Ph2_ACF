#include "RD53BInjectionTool.h"
#include "HWDescription/RD53BProd.h"
#include "HWInterface/RD53FWInterface.h"

#include <boost/version.hpp>
#include <cctype>
#include <cstdint>
#include <stdexcept>

#if BOOST_VERSION >= 106400
#include <boost/integer/common_factor_rt.hpp>
#define LCM boost::integer::lcm
#else
#include <boost/math/common_factor_rt.hpp>
#define LCM boost::math::lcm
#endif

#include "Utils/xtensor/xindex_view.hpp"
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xmanipulation.hpp"
#include "Utils/xtensor/xview.hpp"

#include <TCanvas.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TSystem.h>

using namespace RD53BEventDecoding;

namespace RD53BTools
{
template <class Flavor>
void RD53BInjectionTool<Flavor>::init()
{
    if (param("useHitOr"_s) && param("useSelfTriggers"_s))
        throw std::runtime_error("Hit-or and self-triggers are mutually exclusive");

    if(param("size"_s)[0] == 0) param("size"_s)[0] = RD53BProd<Flavor>::nRows - param("offset"_s)[0];
    if(param("size"_s)[1] == 0) param("size"_s)[1] = RD53BProd<Flavor>::nCols - param("offset"_s)[1];

    std::transform(param("injectionType"_s).begin(), param("injectionType"_s).end(), param("injectionType"_s).begin(), [](const char& c) { return (char)std::tolower(c); });

    static const std::set<std::string> injectionTypes = {{
        "none",
        "analog",
        "digital"
    }};
    if (injectionTypes.find(param("injectionType"_s)) == injectionTypes.end())
        throw std::runtime_error("Invalid injectionType: " + param("injectionType"_s) + " (should be one of: analog, digital, none)");

    auto& steps = param("maskGen"_s);

    std::vector<size_t> sizes[2] = {{1}, {1}};
    std::vector<size_t> stepId[2];

    int i = 0;
    for(auto step: steps)
    {
        sizes[step["dim"_s]].push_back(step["size"_s]);
        stepId[step["dim"_s]].push_back(i);
        ++i;
    }

    for(int dim = 0; dim < 2; ++dim)
    {
        auto  zeroPos   = std::find(sizes[dim].begin(), sizes[dim].end(), 0);
        auto& zeroStep  = steps[stepId[dim][zeroPos - sizes[dim].begin() - 1]];
        auto  prev_size = std::accumulate(sizes[dim].begin(), zeroPos, 1lu, std::multiplies<>{});
        auto  next_size = std::accumulate(sizes[dim].rbegin(), std::make_reverse_iterator(zeroPos + 1), param("size"_s)[dim], [=](auto a, auto b) { return size_t(std::ceil(double(a) / b)); });
        auto  lcm       = zeroStep["shift"_s].size() ? LCM(prev_size, zeroStep["shift"_s][0]) : prev_size;
        *zeroPos        = size_t(std::ceil(double(next_size) / lcm)) * lcm / prev_size;
        std::partial_sum(sizes[dim].begin(), sizes[dim].end() - 1, sizes[dim].begin(), std::multiplies<>{});
        sizes[dim].back() = size_t(std::ceil(double(param("size"_s)[dim]) / *(sizes[dim].end() - 2))) * *(sizes[dim].end() - 2);
        for(auto it = sizes[dim].rbegin(); it != sizes[dim].rend() - 1; ++it) *it = *it / *(it + 1);
        for(size_t i = 1; i < sizes[dim].size(); ++i) steps[stepId[dim][i - 1]]["size"_s] = sizes[dim][i];
    }

    _nFrames = 1;
    for(const auto& step: steps)
        if(!step["parallel"_s]) _nFrames *= step["size"_s];

    _nFrames = std::ceil((double)_nFrames / param("frameStep"_s));
}


template <class Flavor>
RD53BInjectionTool<Flavor>::~RD53BInjectionTool<Flavor>() {
    // std::cout << "~RD53BInjectionTool" << std::endl;
    // Base::for_each_chip([&, this](auto chip) { Base::chipInterface().WriteReg(chip, "SelfTriggerEn", true); });
    // Base::for_each_board([&, this](auto board) { Base::getFWInterface(board).WriteReg("user.ctrl_regs.Aurora_block.self_trigger_en", true); });
}

template <class Flavor>
typename RD53BInjectionTool<Flavor>::ChipEventsMap RD53BInjectionTool<Flavor>::run(Task progress) const
{
    ChipEventsMap result;
    Base::for_each_chip([&](auto chip) { result[chip].reserve(_nFrames * nEvents()); });

    injectionScan(progress, std::array<ScanRange, 0>{}, [&, this](size_t i, auto&& events) {
        this->for_each_chip([&](auto chip) { result[chip].insert(result[chip].end(), std::make_move_iterator(events[chip].begin()), std::make_move_iterator(events[chip].end())); });
    });

    return result;
}

template <class Flavor>
void RD53BInjectionTool<Flavor>::setupMaskFrame(size_t frameId) const
{
    using namespace xt::placeholders;

    auto& chipInterface = Base::chipInterface();

    auto mask = generateInjectionMask(frameId);

    Base::for_each_chip([&](auto* chip) {
        auto& pixelCfg = chip->pixelConfig();
        pixel_matrix_t<Flavor, bool> injectionMask;
        injectionMask.fill(0);
        int row_start, row_end, col_start, col_end;
        if (param("injectionRowOffset"_s) > 0) {
            row_start = param("injectionRowOffset"_s);
            row_end = Flavor::nRows;
        }
        else {
            row_start = 0;
            row_end = Flavor::nRows + param("injectionRowOffset"_s);
        }
        if (param("injectionColOffset"_s) > 0) {
            col_start = param("injectionColOffset"_s);
            col_end = Flavor::nCols;
        }
        else {
            col_start = 0;
            col_end = Flavor::nCols + param("injectionColOffset"_s);
        }
        xt::view(injectionMask, xt::range(row_start, row_end), xt::range(col_start, col_end)) = 
            xt::view(mask, xt::range(Flavor::nRows - row_end, Flavor::nRows - row_start), xt::range(Flavor::nCols - col_end, Flavor::nCols - col_start));
        chipInterface.UpdatePixelMasks(chip,
                                       param("disableUnusedPixels"_s) ? pixel_matrix_t<Flavor, bool>(mask && pixelCfg.enable) : pixelCfg.enable,
                                       param("injectUnusedPixels"_s) ? pixelCfg.enableInjections : pixel_matrix_t<Flavor, bool>(injectionMask && pixelCfg.enableInjections),
                                       pixelCfg.enableHitOr);
        // usleep(100000);
    });
}

template <class Flavor>
ChipDataMap<pixel_matrix_t<Flavor, double>> RD53BInjectionTool<Flavor>::occupancy(const ChipEventsMap& events) const
{
    ChipDataMap<pixel_matrix_t<Flavor, double>> occ;
    for(const auto& item: events)
    {
        occ[item.first].fill(0);
        for(const auto& event: item.second)
            for(const auto& hit: event.hits) occ[item.first](hit.row, hit.col) += 1.0 / (param("nInjections"_s) * param("injectionDuration"_s));
    }
    return occ;
}

template <class Flavor>
ChipDataMap<std::array<size_t, 16>> RD53BInjectionTool<Flavor>::totDistribution(const ChipEventsMap& events) const
{
    ChipDataMap<std::array<size_t, 16>> tot;

    Base::for_each_chip([&](auto* chip) {
        auto it = tot.insert({chip, {0}}).first;
        for(const auto& event: events.at(chip))
        {
            for(const auto& hit: event.hits) { ++it->second[hit.tot]; }
        }
    });
    return tot;
}

template <class Flavor>
void RD53BInjectionTool<Flavor>::draw(const ChipEventsMap& result)
{
    Base::createRootFile();

    auto occMap = occupancy(result);
    auto totMap = totDistribution(result);

    auto used = usedPixels();

    Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
        Base::createRootFileDirectory(chip);

        pixel_matrix_t<Flavor, bool> enabled = used && chip->injectablePixels();

        const auto& occ = occMap[chip];
        const auto& tot = totMap[chip];

        Base::drawHistRaw(tot, "ToT Distribution", 0, 16, "ToT", "Frequency");

        Base::drawMap(occ, "Occupancy Map", "Occupancy");

        xt::xtensor<double, 4> missingOccupancy = xt::reshape_view(enabled - occ, {Flavor::nRows / 8, 8ul, Flavor::nCols / 8, 8ul});

        xt::xarray<double> missingCoreOcc = xt::mean(missingOccupancy, {0, 2});

        Base::drawHist2D(xt::rot90(missingCoreOcc, {1, 0}), "Core Missing Occupancy", 0, 8, 0, 8, "Column", "Row", "Missing Occupancy", true);

        LOG(INFO) << "Number of enabled pixels: " << xt::count_nonzero(enabled)() << RESET;

        LOG(INFO) << "Mean occupancy for enabled pixels: " << xt::mean(xt::filter(occ, enabled))() << RESET;
        LOG(INFO) << "Max occupancy for enabled pixels: " << xt::amax(xt::filter(occ, enabled))() << RESET;
        LOG(INFO) << "Min occupancy for enabled pixels: " << xt::amin(xt::filter(occ, enabled))() << RESET;

        LOG(INFO) << "Mean occupancy for disabled pixels: " << xt::nan_to_num(xt::mean(xt::filter(occ, !enabled)))() << RESET;

        {
            std::vector<size_t> triggerIdHist(param("triggerDuration"_s), 0);
            for(const auto& event: result.at(chip)) 
                triggerIdHist[event.triggerId % param("triggerDuration"_s)] += event.hits.size();
            Base::drawHistRaw(triggerIdHist, "Trigger ID Distribution", 0, param("triggerDuration"_s), "Trigger ID", "Count");
        }

        if(param("enableBCIDReadout"_s))
        {
            auto                minmax    = std::minmax_element(result.at(chip).begin(), result.at(chip).end(), [](const auto& a, const auto& b) { return a.BCID < b.BCID; });
            const size_t        nBCIDBins = minmax.second->BCID - minmax.first->BCID + 1;
            std::vector<size_t> BCICHist(nBCIDBins, 0);
            for(const auto& event: result.at(chip)) BCICHist[event.BCID - minmax.first->BCID] += event.hits.size();
            Base::drawHistRaw(BCICHist, "BCID Distribution", minmax.first->BCID, minmax.second->BCID + 1, "BCID", "Hit Count");
        }

        {
            std::vector<size_t> eventIdHist(param("nInjections"_s) * param("injectionDuration"_s), 0);
            for(auto i = 0u; i < param("nInjections"_s); ++i)
                for(auto j = 0u; j < param("injectionDuration"_s); ++j)
                    for(auto k = 0u; k < param("triggerDuration"_s); ++k) 
                        eventIdHist[i] += result.at(chip)[i * param("injectionDuration"_s) + j * param("triggerDuration"_s) + k].hits.size();

            Base::drawHistRaw(eventIdHist, "Event ID Distribution", 0, param("nInjections"_s), "Event ID", "Count");
        }
    });
}

template <class Flavor>
pixel_matrix_t<Flavor, bool> RD53BInjectionTool<Flavor>::generateInjectionMask(size_t frameId) const
{
    pixel_matrix_t<Flavor, bool> fullMask;
    fullMask.fill(false);

    frameId *= param("frameStep"_s);

    xt::xtensor<bool, 2> mask    = xt::ones<bool>({1, 1});
    size_t               lastDim = 0;
    std::vector<size_t>  currentShifts;

    for(auto step: param("maskGen"_s))
    {
        auto size  = step["size"_s];
        auto shape = mask.shape();

        std::array<size_t, 2> new_shape = shape;
        new_shape[step["dim"_s]] *= size;

        xt::xtensor<bool, 2> new_mask = xt::zeros<bool>(new_shape);

        size_t n = step["parallel"_s] ? size : 1;
        for(size_t i = 0; i < n; ++i)
        {
            size_t j              = step["parallel"_s] ? i : frameId % size;
            size_t shift_count    = i;
            size_t shift          = 0;
            size_t wrapAroundSize = shape[lastDim];
            for(auto s: currentShifts)
            {
                shift += shift_count * s;
                shift_count    = shift_count * s / wrapAroundSize;
                wrapAroundSize = s;
            }
            shift %= shape[lastDim];
            if(step["dim"_s] == 0)
                xt::view(new_mask, xt::range(j * shape[0], (j + 1) * shape[0]), xt::all()) = xt::roll(mask, shift, lastDim);
            else
                xt::view(new_mask, xt::all(), xt::range(j * shape[1], (j + 1) * shape[1])) = xt::roll(mask, shift, lastDim);
        }

        if(!step["parallel"_s]) frameId /= size;

        mask          = new_mask;
        lastDim       = step["dim"_s];
        currentShifts = step["shift"_s];
    }

    auto row_range = xt::range(param("offset"_s)[0], param("offset"_s)[0] + param("size"_s)[0]);
    auto col_range = xt::range(param("offset"_s)[1], param("offset"_s)[1] + param("size"_s)[1]);

    xt::view(fullMask, row_range, col_range) = xt::view(mask, xt::range(0, param("size"_s)[0]), xt::range(0, param("size"_s)[1]));

    return fullMask;
}


template <class Flavor>
void RD53BInjectionTool<Flavor>::ResetChipCounters() const
{
    // Reset TriggerID of each chip
    Base::for_each_chip([&, this] (auto* chip) {
        // Base::chipInterface().template SendCommand<RD53BCmd::Clear>(chip);
        if (Flavor::GlobalPulseRoutes.find("ResetCounters") != Flavor::GlobalPulseRoutes.end())
            Base::chipInterface().SendGlobalPulse(chip, {"ResetCounters"}, 16);
        else
            Base::chipInterface().template SendCommand<RD53BCmd::Clear>(chip);
    });
}

template <class Flavor>
void RD53BInjectionTool<Flavor>::configureInjections() const
{
    auto& chipInterface = Base::chipInterface();
    Base::for_each_chip([&](auto* chip) {
        chipInterface.WriteReg(chip, "LatencyConfig", param("triggerLatency"_s));
        chipInterface.WriteReg(chip, "DigitalInjectionEnable", param("injectionType"_s) == "digital");
        // chipInterface.WriteReg(chip, "EnOutputDataChipId", Base::system().template findValueInSettings<double>("eventReadoutChipID", 0));
        chipInterface.WriteReg(chip, "EnOutputDataChipId", param("enableChipIdReadout"_s));
        chipInterface.WriteReg(chip, "BinaryReadOut", !param("enableToTReadout"_s));
        chipInterface.WriteReg(chip, "RawData", !param("compressHitMap"_s));


        if(std::is_same<Flavor, RD53BFlavor::CMS>::value)
        {
            chipInterface.WriteReg(chip, "EnBCId", param("enableBCIDReadout"_s));
            chipInterface.WriteReg(chip, "EnLv1Id", param("enableL1IDReadout"_s));
        }

        if(param("resetBCID"_s)) chipInterface.SendGlobalPulse(chip, {"ResetBCIDCounter"}); // reset BCID

        if(param("useSelfTriggers"_s)) {
            chipInterface.WriteReg(chip, "SelfTriggerEn", true);
            chipInterface.WriteReg(chip, "SelfTriggerDelay", param("selfTriggerDelay"_s));
            chipInterface.WriteReg(chip, "SelfTriggerMultiplier", param("triggerDuration"_s));
            chipInterface.WriteReg(chip, "HitOrPatternLUT", 0xFFFE);
        }
    });

    for(auto* board: *Base::system().fDetectorContainer)
    {
        RD53FWInterface& fwInterface = Base::getFWInterface(board);
        // auto& fastCmdConfig = *fwInterface.getLocalCfgFastCmd();
        RD53FWInterface::FastCommandsConfig fastCmdConfig;

        fastCmdConfig.n_triggers       = param("nInjections"_s);
        fastCmdConfig.trigger_multiplier = param("injectionDuration"_s);
        fastCmdConfig.trigger_duration = param("triggerDuration"_s) - 1;

        if(param("resetBCID"_s))
        {
            fastCmdConfig.autozero_source                   = RD53FWInterface::AutozeroSource::FastCMDFSM;
            fastCmdConfig.fast_cmd_fsm.delay_after_autozero = param("delayAfterBCIDReset"_s);
        }

        fastCmdConfig.fast_cmd_fsm.ecr_en     = false;
        fastCmdConfig.fast_cmd_fsm.trigger_en = true;

        fastCmdConfig.fast_cmd_fsm.delay_after_first_prime = 1000;
        fastCmdConfig.fast_cmd_fsm.delay_after_prime       = param("delayAfterPrime"_s);
        fastCmdConfig.fast_cmd_fsm.delay_after_inject      = param("delayAfterInject"_s);
        fastCmdConfig.fast_cmd_fsm.delay_after_trigger     = param("delayAfterTrigger"_s);

        if (param("useHitOr"_s))
            Base::for_each_chip([&](auto* chip) { fastCmdConfig.enable_hitor |= 1 << chip->getHybridId(); });

        if (param("useSelfTriggers"_s))
            fwInterface.WriteReg("user.ctrl_regs.Aurora_block.self_trigger_en", true);

        if (std::tolower(param("injectionType"_s)[0]) == 'n')
        {
            fastCmdConfig.fast_cmd_fsm.first_cal_en  = false;
            fastCmdConfig.fast_cmd_fsm.second_cal_en = false;

            if (param("useSelfTriggers"_s) or param("useHitOr"_s))
                fastCmdConfig.trigger_source = RD53FWInterface::TriggerSource::HitOr;
        }   
        else {
            fastCmdConfig.trigger_source = RD53FWInterface::TriggerSource::FastCMDFSM;

            if(param("injectionType"_s) == "analog")
            {
                // std::cout << param("injectionDelay160MHz"_s) << std::endl;
                fastCmdConfig.fast_cmd_fsm.first_cal_en    = true;
                fastCmdConfig.fast_cmd_fsm.second_cal_en   = true;
                fastCmdConfig.fast_cmd_fsm.first_cal_data  = bits::pack<1, 5, 8, 1, 5>(1, 0, 2, 0, 0);
                fastCmdConfig.fast_cmd_fsm.second_cal_data = bits::pack<1, 5, 8, 1, 5>(0, param("injectionDelay160MHz"_s), param("pulseDuration"_s), 0, 0);
            }
            else if(param("injectionType"_s) == "digital")
            {
                fastCmdConfig.fast_cmd_fsm.first_cal_en  = false;
                fastCmdConfig.fast_cmd_fsm.second_cal_en = true;
                // fastCmdConfig.fast_cmd_fsm.first_cal_data = bits::pack<1, 5, 8, 1, 5>(1, 0, 2, 0, 0);
                fastCmdConfig.fast_cmd_fsm.second_cal_data = bits::pack<1, 5, 8, 1, 5>(1, param("injectionDelay160MHz"_s), param("pulseDuration"_s), 0, 0);
            }

            if (param("useSelfTriggers"_s) or param("useHitOr"_s))
                fastCmdConfig.fast_cmd_fsm.trigger_en = false;
            
        }

        fwInterface.ConfigureFastCommands(&fastCmdConfig);
    }
}

template <class Flavor>
pixel_matrix_t<Flavor, bool> RD53BInjectionTool<Flavor>::usedPixels() const
{
    pixel_matrix_t<Flavor, bool> result;
    result.fill(0);
    for(size_t frameId = 0; frameId < nFrames(); ++frameId) { result |= generateInjectionMask(frameId); }
    return result;
}


template <class Flavor>
void RD53BInjectionTool<Flavor>::print_debug_info(const std::vector<uint32_t>& data, const typename Flavor::FormatOptions& options) const
{    

    std::cout << "Raw data:\n";

    for(size_t i = 0; i < std::ceil(data.size() / 4.0); ++i)
    {
        std::cout << std::setw(8) << i << " ";
        for(size_t j = 4 * i; j < std::min(4 * i + 4, data.size()); ++j) { std::cout << std::hex << std::setfill('0') << std::setw(8) << data[j] << " "; }
        std::cout << std::dec << '\n';
    }

    auto event_start = RD53BEventDecoding::event_start(data);

    int lastBCID = 0;
    // int lastChipBCID = 0;
    std::map<std::pair<uint8_t, uint8_t>, int> lastChipBCID;

    for (size_t eventId = 0; eventId < event_start.size() - 1; ++ eventId) {
        
        RD53BEventDecoding::RD53BEventContainer event = RD53BEventDecoding::decode_events<Flavor>(data.begin() + event_start[eventId], data.begin() + event_start[eventId + 1], options)[0];

        std::cout << "Event " << eventId << ":\n";

        int BCID_diff = event.BCID - lastBCID;
        lastBCID = event.BCID;

        std::cout << '\t' << "BCID: " << +event.BCID << (BCID_diff >= 0 ? "(+" : "(") << BCID_diff << ")\n";
        std::cout << '\t' << "l1a_counter: " << +event.l1a_counter << '\n';
        std::cout << '\t' << "triggerTag: " << +event.triggerTag << '\n';

        for (const auto& chipEvent : event.events) {
            auto chip_address = std::make_pair(chipEvent.hybridId, chipEvent.chipLane);
            int chipBCID_diff = chipEvent.BCID - lastChipBCID[chip_address];
            lastChipBCID[chip_address] = chipEvent.BCID;
            std::cout << "\t\t" << "hybridId: " << +chipEvent.hybridId << '\n';
            std::cout << "\t\t" << "chipLane: " << +chipEvent.chipLane << '\n';
            std::cout << "\t\t" << "chipIdMod4: " << +chipEvent.chipIdMod4 << '\n';
            std::cout << "\t\t" << "BCID: " << +chipEvent.BCID << (chipBCID_diff >= 0 ? "(+" : "(") << chipBCID_diff << ")\n";
            std::cout << "\t\t" << "triggerId: " << +chipEvent.triggerId << '\n';
            std::cout << "\t\t" << "triggerTag: " << +chipEvent.triggerTag << '\n';
            std::cout << "\t\t" << "nHits: " << +chipEvent.hits.size() << '\n';
        }
    }
    std::cout << "Press Enter to Continue" << std::endl;
    std::cin.ignore();
}

template struct RD53BInjectionTool<RD53BFlavor::ATLAS>;
template struct RD53BInjectionTool<RD53BFlavor::CMS>;
template struct RD53BInjectionTool<RD53BFlavor::ITkPixV2>;
template struct RD53BInjectionTool<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
