#include "RD53BSignalScan.h"

#include "Utils/xtensor/xsort.hpp"
#include "Utils/xtensor/xrandom.hpp"

#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TTree.h>

#include <algorithm>

namespace RD53BTools
{
template <class Flavor>
void RD53BSignalScan<Flavor>::init()
{
    nGDACSteps  = std::ceil((param("GDACRange"_s)[1] - param("GDACRange"_s)[0]) / double(param("GDACStep"_s)));
    nDelaySteps = std::ceil(32.0 / param("fineDelayStep"_s));
}

template <class Flavor>
auto RD53BSignalScan<Flavor>::run(Task progress) -> Result
{
    auto& chipInterface = *static_cast<RD53BProdInterface<Flavor>*>(Base::system().fReadoutChipInterface);

    ChipDataMap<xt::xtensor<float, 4>> occupancy;

    ChipDataMap<bool> isDualEdgeToTCounting;

    auto& injTool = param("injectionTool"_s);

    auto                           GDACBins = xt::arange(param("GDACRange"_s)[0], param("GDACRange"_s)[1], param("GDACStep"_s));
    AsyncWorkerPool<void>          analyzer_pool(param("analyzerThreads"_s));
    std::vector<std::future<void>> analysisFutures;

    auto usedPixels = injTool.usedPixels();

    ChipDataMap<pixel_matrix_t<Flavor, int>> pixelIndices;
    ChipDataMap<size_t>                      originalGDAC;

    Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
        originalGDAC[chip] = chipInterface.ReadReg(chip, Flavor::Reg::DAC_GDAC_M_LIN);
        pixel_matrix_t<Flavor, int> indices;
        indices.fill(-1);
        // pixel_matrix_t<Flavor, bool> enabled = usedPixels && chip->injectablePixels();
        auto   enabled               = usedPixels && chip->pixelConfig().enable;
        size_t nEnabled              = xt::count_nonzero(enabled)();
        xt::filter(indices, enabled) = xt::arange(nEnabled);
        pixelIndices.insert({chip, indices});
        occupancy.insert({chip, xt::zeros<double>({injTool.param("triggerDuration"_s), nDelaySteps, nEnabled, GDACBins.size()})});

        isDualEdgeToTCounting[chip] = chipInterface.ReadReg(chip, "ToTDualEdgeCount");
    });

    injTool.param("enableToTReadout"_s) = true;

    // size_t lastCoarseDelay = 0;
    injTool.injectionScan(progress,
                          makeScan(ScanRange(nDelaySteps,
                                             [&, this](auto i) {
                                                Base::for_each_hybrid([&](Hybrid* hybrid) { chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", i * param("fineDelayStep"_s)); });
                                                //  size_t totalDelay  = i * param("fineDelayStep"_s);
                                                //  size_t fineDelay   = totalDelay % param("maxFineDelay"_s);
                                                //  size_t coarseDelay = (totalDelay - fineDelay) / 8;
                                                //  Base::for_each_hybrid([&](Hybrid* hybrid) { chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", fineDelay); });
                                                //  if(coarseDelay != lastCoarseDelay)
                                                //  {
                                                //      injTool.param("injectionDelay160MHz"_s) = coarseDelay;
                                                //      injTool.configureInjections();
                                                //      lastCoarseDelay = coarseDelay;
                                                //  }
                                             }),
                                   ScanRange(nGDACSteps,
                                             [&, this](auto i) {
                                                 Base::for_each_hybrid([&](Hybrid* hybrid) {
                                                     chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_L_LIN, param("GDACRange"_s)[1] - param("GDACStep"_s) * i);
                                                     chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_R_LIN, param("GDACRange"_s)[1] - param("GDACStep"_s) * i);
                                                     chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_M_LIN, param("GDACRange"_s)[1] - param("GDACStep"_s) * i);
                                                 });
                                                 if (i == 0)
                                                    usleep(5000);
                                             })),
                          [&, this](auto i, auto&& events) {
                              size_t frameId = i / nDelaySteps;
                              size_t delayId = i % nDelaySteps;

                              auto usedPixels = injTool.generateInjectionMask(frameId);
                              Base::for_each_chip([&](auto chip) {
                                  analysisFutures.erase(
                                      std::remove_if(analysisFutures.begin(), analysisFutures.end(), [](const auto& f) { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }),
                                      analysisFutures.end());

                                  auto& pixelIndicesLocal = pixelIndices[chip];
                                  analysisFutures.push_back(analyzer_pool.enqueue_task([&, delayId, chip, events = std::move(events[chip])]() {
                                      for(size_t triggerId = 0; triggerId < injTool.param("triggerDuration"_s); ++triggerId)
                                      {
                                          for(size_t gdac = 0; gdac < GDACBins.size(); ++gdac)
                                          {
                                              for(const auto& event: xt::view(events, gdac, xt::all()))
                                              {
                                                  auto eventTriggerId = event.triggerId % injTool.param("triggerDuration"_s);
                                                  if(eventTriggerId <= triggerId)
                                                      for(const auto& hit: event.hits)
                                                      {
                                                          auto triggerIdToTSum = hit.tot / (isDualEdgeToTCounting[chip] ? 2.0 : 1.0) + (eventTriggerId);
                                                          int  pixelIndex      = pixelIndicesLocal(hit.row, hit.col);
                                                          if(triggerIdToTSum >= triggerId && pixelIndex >= 0)
                                                          { occupancy[chip](triggerId, delayId, pixelIndex, gdac) += 1. / injTool.param("nInjections"_s); }
                                                      }
                                              }
                                          }
                                      }
                                  }));
                              });
                          });

    Base::for_each_chip([&](auto* chip) {
        chipInterface.WriteReg(chip, Flavor::Reg::DAC_GDAC_L_LIN, originalGDAC[chip]);
        chipInterface.WriteReg(chip, Flavor::Reg::DAC_GDAC_R_LIN, originalGDAC[chip]);
        chipInterface.WriteReg(chip, Flavor::Reg::DAC_GDAC_M_LIN, originalGDAC[chip]);
    });

    for(const auto& f: analysisFutures) f.wait();

    analyzer_pool.join();

    return occupancy;
}

template <class Flavor>
void RD53BSignalScan<Flavor>::draw(const Result& occupancy)
{
    Base::createRootFile();

    auto& injTool    = param("injectionTool"_s);
    auto  usedPixels = injTool.usedPixels();

    size_t                 nDelayBins = nDelaySteps * injTool.param("triggerDuration"_s);
    // xt::xtensor<double, 1> delayBins  = 25. / 32. * xt::arange(nDelayBins);
    xt::xtensor<double, 1> delayBins  = 25. / 32. * param("fineDelayStep"_s) * xt::arange(nDelayBins);

    xt::xtensor<double, 1> GDACBins = xt::arange(param("GDACRange"_s)[0], param("GDACRange"_s)[1], param("GDACStep"_s));

    Base::for_each_chip([&, this](auto* chip) {
        Base::createRootFileDirectory(chip);

        auto pixelPositions = xt::argwhere(usedPixels && chip->pixelConfig().enable);

        size_t nPixels = occupancy.at(chip).shape()[2];

        xt::xarray<double> occupancy_local = xt::transpose(occupancy.at(chip), {2, 0, 1, 3});
        occupancy_local                    = xt::flip(occupancy_local, 2);
        occupancy_local.reshape({nPixels, nDelayBins, GDACBins.size()});

        auto max_gdac_reversed = xt::argmax(occupancy_local > .5, 2);
        auto max_gdac          = xt::where(xt::not_equal(max_gdac_reversed, 0u), GDACBins.size() - 1 - max_gdac_reversed, 0);

        auto peak_gdac = xt::index_view(GDACBins, xt::amax(max_gdac, 1));
        auto peak_time = xt::index_view(delayBins, xt::argmax(max_gdac, 1));

        Base::drawHist(peak_gdac, "Peak GDAC distribution", GDACBins.size(), GDACBins(0), GDACBins.periodic(-1));
        Base::drawHist(peak_time, "Peaking time distribution", delayBins.size(), delayBins(0), delayBins.periodic(-1));

        occupancy_local = xt::flip(occupancy_local, 2);

        auto mean_occupancy = xt::mean(occupancy_local, {0});

        {
            auto c = new TCanvas("mean_occ", "Mean Occupancy");
            auto h = new TH2F("mean_occ", "Occupancy", nDelayBins, 0, delayBins.periodic(-1), GDACBins.size(), GDACBins(0), GDACBins.periodic(-1));
            for(size_t i = 0; i < nDelayBins; ++i)
            {
                for(size_t j = 0; j < GDACBins.size(); ++j) { h->SetBinContent(i + 1, j + 1, mean_occupancy(i, j)); }
            }

            h->SetXTitle("Time [ns]");
            h->SetYTitle("GDAC");
            h->SetZTitle("Occupancy");
            h->Draw("colz");

            c->Write();
        }

        // for(auto idx: xt::sort(xt::view(xt::random::permutation(nPixels), xt::range(0, 1000))))
        for(auto idx: xt::arange(std::min(nPixels, param("nPlots"_s))))
        {
            auto        pixelPos = pixelPositions[idx];
            std::string name     = "Signal (" + std::to_string(pixelPos[0]) + ", " + std::to_string(pixelPos[1]) + ')';
            auto        c        = new TCanvas(name.c_str(), name.c_str());

            auto h = new TH2F(name.c_str(), "Occupancy", nDelayBins, 0, delayBins.periodic(-1), GDACBins.size(), GDACBins(0), GDACBins.periodic(-1));
            for(size_t i = 0; i < nDelayBins; ++i)
            {
                for(size_t j = 0; j < GDACBins.size(); ++j) { h->SetBinContent(i + 1, j + 1, occupancy_local(idx, i, j)); }
            }
            h->SetXTitle("Time [ns]");
            h->SetYTitle("GDAC");
            h->Draw("colz");

            c->Write();
        }
    });
}

template class RD53BSignalScan<RD53BFlavor::ATLAS>;
template class RD53BSignalScan<RD53BFlavor::CMS>;
template class RD53BSignalScan<RD53BFlavor::ITkPixV2>;
template class RD53BSignalScan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools