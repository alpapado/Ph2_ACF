#include "RD53TempSensorWLT.h"
#include "HWDescription/RD53BProd.h"

#include <chrono>
#include <fstream>
#include <thread>
#include <vector>

#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

namespace RD53BTools {

template<class Flavor> const std::vector<typename RD53BTools::RD53TempSensorWLT<Flavor>::TransistorConfig> RD53BTools::RD53TempSensorWLT<Flavor>::transistorSeq = {
    { "RADSENS_SLDOA", Flavor::Reg::MON_SENS_SLDO, 0 },
    { "TEMPSENS_SLDOA", Flavor::Reg::MON_SENS_SLDO, 0 },
    { "RADSENS_SLDOD", Flavor::Reg::MON_SENS_SLDO, 6 },
    { "TEMPSENS_SLDOD", Flavor::Reg::MON_SENS_SLDO, 6 },
    { "RADSENS_ACB", Flavor::Reg::MON_SENS_ACB, 0 },
    { "TEMPSENS_ACB", Flavor::Reg::MON_SENS_ACB, 0 }
};

template <class Flavor>
ChipDataMap<typename RD53TempSensorWLT<Flavor>::ChipResults> RD53TempSensorWLT<Flavor>::run() {
    Ph2_System::SystemController& system = Base::system();
    RD53BProdInterface<Flavor>& chipIface = Base::chipInterface();
    TCPClient& psIface = *system.fPowerSupplyClient;

    psIface.sendAndReceivePacket(
        "VoltmeterSetRange,"
        "VoltmeterId:" + param("voltmeterId"_s) + ","
        "ChannelId:" + param("voltmeterChannelId"_s) + ","
        "Value:1.3"
    );

    ChipDataMap<ChipResults> results;

    fWPAC = ::open(param("wpacPort"_s).c_str(), O_RDWR);
    if(fWPAC < 0) {
        throw std::runtime_error("Could not open WPAC port");
    }
    termios conf[2];
    ::tcgetattr(fWPAC, conf + 0);
    ::tcgetattr(fWPAC, conf + 1);
    ::cfmakeraw(conf + 1);
    conf[1].c_cc[VMIN] = 0; //timeout timer starts immediately
    conf[1].c_cc[VTIME] = 20; //== 0.1 s
    ::tcsetattr(fWPAC, TCSADRAIN, conf + 1);

    for_each_device<Chip>(system, [&] (Chip* chip) {
        results[chip].temperature = readTemp() + 273.15;
        RD53BProd<Flavor>* rd53b = static_cast<RD53BProd<Flavor>*>(chip);
        //transistor-based sensors
        for(const TransistorConfig& conf : transistorSeq) {
            LOG(INFO) << "Measuring " << conf.mux << RESET;
            std::vector<float> (&data)[2] = results[chip].transistorData[conf.mux];
            // TODO check effect of enable bit
            chipIface.WriteReg(rd53b, Flavor::Reg::MonitorConfig, bits::pack<6, 6>(Flavor::IMuxMap.at("HIGH_Z"), Flavor::VMuxMap.at(conf.mux)));
            for(int bias : { 0, 1 }) {
                for(int i = 0; i < param("nDEM"_s); ++i) {
                    chipIface.WriteReg(chip, conf.reg, bits::pack<1, 4, 1>(true, i, bias) << conf.offset);
                    std::this_thread::sleep_for(std::chrono::milliseconds(param("millisecondsAfterSettingVMUX"_s)));
                    if(param("useADC"_s)) {
#ifdef DEBUG
                        for(int zz = 0; zz < 10; ++zz) {
#endif
                        data[bias].push_back(chipIface.ReadChipADC(rd53b, conf.mux));
                        LOG(INFO) << conf.mux << " [sel=" << i << " str=" << bias << "]: " << data[bias][i];
#ifdef DEBUG
                        }
#endif
                    }
                    else {
                        data[bias].push_back(std::stof(psIface.sendAndReceivePacket(
                            "ReadVoltmeter,"
                            "VoltmeterId:" + param("voltmeterId"_s) + ","
                            "ChannelId:" + param("voltmeterChannelId"_s)
                        )));
                    }
                }
            }
            chipIface.WriteReg(rd53b, conf.reg, 0);
        }
        //resistor-based sensors
        for (const std::pair<const char*, std::vector<float>&>& p: std::vector<std::pair<const char*, std::vector<float>&>>{
            {"RPOLYTSENS_TOP", std::ref(results[chip].resistorTop)},
            {"RPOLYTSENS_BOTTOM", std::ref(results[chip].resistorBottom)},
            {"I_NTC", std::ref(results[chip].resistorBias)},
            {"GNDA1", std::ref(results[chip].groundTop)},
            {"GNDA0", std::ref(results[chip].groundBottom)},
            {"IMUX_OUT", std::ref(results[chip].groundBias)},
        })
        {
            LOG(INFO) << "Measuring " << p.first << RESET;
            chipIface.ReadChipADC(rd53b, p.first);
            std::this_thread::sleep_for(std::chrono::milliseconds(param("millisecondsAfterSettingVMUX"_s)));
            for(int i = 0; i < param("adcSamples"_s); ++i) {
                if(param("useADC"_s)) {
                    p.second.push_back(chipIface.ReadChipADC(rd53b, p.first));
                }
                else {
                    p.second.push_back(std::stof(psIface.sendAndReceivePacket(
                        "ReadVoltmeter,"
                        "VoltmeterId:" + param("voltmeterId"_s) + ","
                        "ChannelId:" + param("voltmeterChannelId"_s)
                    )));
                }
            }
        }
        // extra grounds
        for (const std::pair<const char*, float&>& p: std::vector<std::pair<const char*, float&>>{
            {"GNDA2", std::ref(results[chip].groundSLDOA)},
            {"GNDA3", std::ref(results[chip].groundSLDOD)},
            {"GNDA4", std::ref(results[chip].groundACB)},
        })
        {
            chipIface.ReadChipADC(rd53b, p.first);
            std::this_thread::sleep_for(std::chrono::milliseconds(param("millisecondsAfterSettingVMUX"_s)));
            p.second = std::stof(psIface.sendAndReceivePacket(
                "ReadVoltmeter,"
                "VoltmeterId:" + param("voltmeterId"_s) + ","
                "ChannelId:" + param("voltmeterChannelId"_s)
            ));
        }
    });

    ::tcsetattr(fWPAC, TCSADRAIN, conf + 0); //restoring original configuration
    ::close(fWPAC);

    return results;
}

template <class Flavor>
void RD53TempSensorWLT<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
    std::ofstream datFile("tempCalibWLT.dat");
    std::ofstream jsonFile(Base::getOutputFilePath("results.json"));

    jsonFile << "{\"chips\":[";
    const char* str = "{";
    for(const std::pair<const ChipLocation, ChipResults>& chipRes : results) {
        jsonFile << str;
        str = ",{";
        jsonFile << "\"board\":" << chipRes.first.board_id << ","
             << "\"hybrid\":" << chipRes.first.hybrid_id << ","
             << "\"id\":" << chipRes.first.chip_lane << ",";
        const char* str2 = "";
        for(const std::pair<const std::string, std::vector<float>[2]>& p : chipRes.second.transistorData) {
            datFile << std::scientific << chipRes.second.temperature / (p.second[1][0] - p.second[0][0]) << " " << p.first << "\n";
            jsonFile << str2
                 << "\"" << p.first << "\":{"
                 << "\"temperature\":" << chipRes.second.temperature << ",";
            str2 = ",";
            float sum = 0.0f;
            for(const int bias : { 0, 1 }) {
                sum = -sum;
                if(bias == 0) jsonFile << "\"min_bias\":[";
                else jsonFile << "\"max_bias\":[";
                const char* str3 = "";
                for(const float v : p.second[bias]) {
                    sum += v;
                    jsonFile << str3 << v;
                    str3 = ",";
                }
                jsonFile << "],";
            }
            jsonFile << "\"ideality_factor\":" << idealityFactor(chipRes.second.temperature, sum / p.second[0].size())
                 << "}";
        }

        jsonFile << ",\"gnd_SLDOA\":" << chipRes.second.groundSLDOA;
        jsonFile << ",\"gnd_SLDOD\":" << chipRes.second.groundSLDOD;
        jsonFile << ",\"gnd_ACB\":" << chipRes.second.groundACB;

        // TODO use average instead of first value! VVV
        datFile << std::scientific << (chipRes.second.resistorTop.front() - chipRes.second.groundTop.front()) << " " << "RPOLYTSENS_TOP" << "\n";
        datFile << std::scientific << (chipRes.second.resistorBottom.front() - chipRes.second.groundBottom.front()) << " " << "RPOLYTSENS_BOTTOM" << "\n";
        datFile << std::scientific << chipRes.second.temperature << " " << "calibtemp" << "\n";

        for (const std::pair<const char *, const std::vector<float>&>& p : std::vector<std::pair<const char*, const std::vector<float>&>>{
            {"Poly_TEMPSENS_top", chipRes.second.resistorTop},
            {"Poly_TEMPSENS_bottom", chipRes.second.resistorBottom},
            {"bias_sgn", chipRes.second.resistorBias},
            {"VGNDA_RPOLYTSENS_TOP", chipRes.second.groundTop},
            {"VGNDA_RPOLYTSENS_BOTTOM", chipRes.second.groundBottom},
            {"bias_gnd", chipRes.second.groundBias},
        })
        {
            jsonFile << ",\"" << p.first << "\":{"
                 << "\"temperature\":" << chipRes.second.temperature << ","
                 << "\"raw_ADC\":[";
            str2 = "";
            for(const float& x : p.second) {
                jsonFile << str2 << x;
                str2 = ",";
            }
            jsonFile << "]}"; // close polytempsens block
        }
        jsonFile << "}"; // close single chip block
    }
    jsonFile << "]}";
}

template class RD53BTools::RD53TempSensorWLT<Ph2_HwDescription::RD53BFlavor::ATLAS>;
template class RD53BTools::RD53TempSensorWLT<Ph2_HwDescription::RD53BFlavor::CMS>;
template class RD53BTools::RD53TempSensorWLT<Ph2_HwDescription::RD53BFlavor::ITkPixV2>;
template class RD53BTools::RD53TempSensorWLT<Ph2_HwDescription::RD53BFlavor::CROCv2>;

}
