#ifndef RD53ShortTempSensor_H
#define RD53ShortTempSensor_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53ShortTempSensor : public RD53BTool<RD53ShortTempSensor, Flavor>
{
    using Base = RD53BTool<RD53ShortTempSensor, Flavor>;
    using Base::Base;

    struct ChipResults
    {
        double valueLow;
        double valueHigh;
        double temperature[10];
        double measureDV;
    };

    static constexpr int sensor_VMUX[8]    = {0b1000000000101, 0b1000000000110, 0b1000000001101, 0b1000000001110, 0b1000000001111, 0b1000000010000, 0b1000000010001, 0b1000000010010};
    double               idealityFactor[8] = {1.25, 1.25, 1., 1.25, 1., 1.25, 1., 1.25};

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;
};

} // namespace RD53BTools

#endif
