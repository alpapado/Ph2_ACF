#include "RD53ShortRingOscillator.h"

#include "ProductionToolsIT/ITchipTestingInterface.h"

#include "Utils/xtensor/xadapt.hpp"
#include "Utils/xtensor/xcsv.hpp"

#include <TGraph.h>

namespace RD53BTools
{
template <class Flavor>
ChipDataMap<typename RD53ShortRingOscillator<Flavor>::ChipResult> RD53ShortRingOscillator<Flavor>::run() const
{
    ChipDataMap<ChipResult> results;
    auto&                   chipInterface = Base::chipInterface();

    Base::for_each_chip([&](auto chip) {
        for(int ringOsc = 0; ringOsc < 8; ringOsc++)
        {
            // Set up oscillators
            chipInterface.WriteReg(chip, "RingOscAEnable", 0b11111111);
            chipInterface.WriteReg(chip, "RingOscAClear", 1);
            chipInterface.WriteReg(chip, "RingOscAClear", 0);
            chipInterface.WriteReg(chip, "RingOscARoute", ringOsc);
            chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsA"}, 50); // Start Oscillators
            results[chip][ringOsc] = chipInterface.ReadReg(chip, "RING_OSC_A_OUT") - 4096;
            LOG(INFO) << BOLDMAGENTA << "Counts: " << results[chip][ringOsc] << RESET;
            // results[chip].trimOscFrequency[ringOsc] = trimOscCounts[ringOsc]/((2*51)/40);
        }
        chipInterface.WriteReg(chip, "RingOscAEnable", 0b0000000);
        chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsA"}, 50); // Stop Oscillators
        for(int ringOsc = 0; ringOsc < 34; ringOsc++)
        {
            // Set up oscillators
            chipInterface.WriteReg(chip, "RingOscBEnBL", 1);
            chipInterface.WriteReg(chip, "RingOscBEnBR", 1);
            chipInterface.WriteReg(chip, "RingOscBEnCAPA", 1);
            chipInterface.WriteReg(chip, "RingOscBEnFF", 1);
            chipInterface.WriteReg(chip, "RingOscBEnLVT", 1);
            chipInterface.WriteReg(chip, "RingOscBClear", 1);
            chipInterface.WriteReg(chip, "RingOscBClear", 0);
            chipInterface.WriteReg(chip, "RingOscBRoute", ringOsc);
            chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsB"}, 50); // Start Oscillators
            results[chip][ringOsc + 8] = chipInterface.ReadReg(chip, "RING_OSC_B_OUT") - 4096;
            // LOG(INFO) << BOLDMAGENTA << "Counts: " << trimOscCounts[ringOsc + 8] << RESET;
            // results[chip].trimOscFrequency[ringOsc + 8] = trimOscCounts[ringOsc + 8]/((2*51)/40);
        }
        chipInterface.WriteReg(chip, "RingOscBEnBL", 0);
        chipInterface.WriteReg(chip, "RingOscBEnBR", 0);
        chipInterface.WriteReg(chip, "RingOscBEnCAPA", 0);
        chipInterface.WriteReg(chip, "RingOscBEnFF", 0);
        chipInterface.WriteReg(chip, "RingOscBEnLVT", 0);
        chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsB"}, 50); // Stop Oscillators
    });

    return results;
}

template <class Flavor>
void RD53ShortRingOscillator<Flavor>::draw(const ChipDataMap<ChipResult>& results)
{
    static const std::vector<const char*> oscNames = {
        "CKND0",          "CKND4",          "INV0",      "INV4",      "NAND0",          "NAND4",           "NOR0",   "NOR4",   "CKND0 L", "CKND0 R", "CKND4 L",    "CKND4 R",    "INV0 L", "INV0 R",
        "INV4 L",         "INV4 R",         "NAND0 L",   "NAND0 R",   "NAND4 L",        "NAND4 R",         "NOR0 L", "NOR0 R", "NOR4 L",  "NOR4 R",  "SCAN DFF 0", "SCAN DFF 0", "DFF 0",  "DFF 0",
        "NEG EDGE DFF 1", "NEG EDGE DFF 1", "LVT INV 0", "LVT INV 4", "LVT 4-IN NAND0", "LVT 4-IN NAND 4", "0",      "1",      "2",       "3",       "4",          "5",          "6",      "7"};
    std::ofstream outFile(Base::getOutputFilePath("shortOscillator.csv"));
    outFile << "board_id, hybrid_id, chip_id\n";
    for(const auto& name: oscNames) outFile << ", " << name;
    outFile << '\n';

    for(const auto& item: results)
    {
        outFile << item.first.board_id << ", " << item.first.hybrid_id << ", " << item.first.chip_lane;
        for(size_t i = 0; i < std::min(oscNames.size(), item.second.size()); ++i) outFile << ", " << item.second[i];
        outFile << '\n';
    }
}

template class RD53ShortRingOscillator<RD53BFlavor::ATLAS>;
template class RD53ShortRingOscillator<RD53BFlavor::CMS>;
template class RD53ShortRingOscillator<RD53BFlavor::ITkPixV2>;
template class RD53ShortRingOscillator<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
