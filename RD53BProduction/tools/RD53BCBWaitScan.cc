#include "RD53BCBWaitScan.h"

#include <string>

namespace RD53BTools {

template <class Flavor>
bool RD53BCBWaitScan<Flavor>::run(Task progress) const {
    //setup
    Ph2_System::SystemController& system = Base::system();
    auto& chipInterface = Base::chipInterface();
    ChipDataMap<uint16_t> cbWaitRegValues;

    //saving the previous chip configuration for AuroraConfig
    for_each_device<Chip>(system, [&] (Chip* chip) {
        cbWaitRegValues.insert({chip, chipInterface.ReadReg(chip, "CBWait")});
    });

    uint32_t minValue = Base::param("minValue"_s);
    uint32_t maxValue = Base::param("maxValue"_s);
    uint32_t nPoints = Base::param("nPoints"_s);
    uint32_t configDelayUs = Base::param("configDelayUs"_s);
    uint16_t sleepTimeUs = Base::param("sleepTimeUs"_s);

    const uint32_t nPts = 255;
    uint16_t iErrors = 0;
    uint16_t nErrors[nPts];

    LOG(INFO) << BOLDGREEN << "Starting CBWait scan from "
        << std::to_string(minValue) << " to " << std::to_string(maxValue)
        << RESET;

    //looping on all devices to perform the test
    for_each_device<Chip>(system, [&](DeviceChain devices) {
        auto& fwInterface = Base::getFWInterface(devices.board);
        auto chip = static_cast<RD53BProd<Flavor>*>(devices.chip);

        //variable for holding the Aurora status
        uint8_t channelStatus = 0;

        //looping
        for (uint32_t i = minValue; i <= maxValue ; i++){
            //setting CBWait
            LOG(INFO) << "Setting CBWait to " << std::to_string(i) << RESET;
            chipInterface.WriteReg(chip, "CBWait", i);
            chipInterface.SendGlobalPulse(chip, {"ResetAurora"}, 10);
            usleep(configDelayUs);

            //performing several tries to measure the Aurora channel status
            iErrors = 0;
            for (uint32_t j = 0; j < nPoints; j++) {
                //reading the channel status
                channelStatus = fwInterface.ReadReg("user.stat_regs.aurora_rx_channel_up");
                if (channelStatus != 1) {
                    iErrors++;
                }
                //sleep and retry
                usleep(sleepTimeUs);
            }
            if (iErrors == 0) {
                LOG(INFO) << BOLDGREEN << "No errors" << RESET;
            } else {
                LOG(INFO) << BOLDRED << "Number of errors: "
                    << std::to_string(iErrors) << "/" << std::to_string(nPoints)
                    << RESET;
                nErrors[i] = iErrors;
            }

        }

    });

    //restoring the previous configuration
    for_each_device<Chip>(system, [&] (Chip* chip) {
        chipInterface.WriteReg(
            chip,
            "CBWait",
            cbWaitRegValues[chip]
        );
    });

    return true;
}

template class RD53BCBWaitScan<RD53BFlavor::ATLAS>;
template class RD53BCBWaitScan<RD53BFlavor::CMS>;
template class RD53BCBWaitScan<RD53BFlavor::ITkPixV2>;
template class RD53BCBWaitScan<RD53BFlavor::CROCv2>;

}
