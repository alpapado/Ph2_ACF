#ifndef RD53TempSensorWLT_H
#define RD53TempSensorWLT_H

#include <cmath>
#include <sstream>
#include <string>
#include <vector>

#include <unistd.h>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53TempSensorWLT;

template <class Flavor>
const auto ToolParameters<RD53TempSensorWLT<Flavor>> = make_named_tuple(
    std::make_pair("wpacPort"_s, std::string("")),
    std::make_pair("wpacTempSens"_s, 1u),
    std::make_pair("millisecondsAfterSettingVMUX"_s, 0u),
    std::make_pair("nDEM"_s, 16),
    std::make_pair("useADC"_s, false),
    std::make_pair("voltmeterId"_s, std::string("analogCalib")),
    std::make_pair("voltmeterChannelId"_s, std::string("analogCalib")),
    std::make_pair("adcSamples"_s, 1)
);

template <class Flavor>
struct RD53TempSensorWLT : public RD53BTool<RD53TempSensorWLT, Flavor> {
    using Base = RD53BTool<RD53TempSensorWLT, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResults {
        float temperature;
        std::map<std::string, std::vector<float>[2]> transistorData;
        float groundSLDOA, groundSLDOD, groundACB;
        std::vector<float> resistorTop, resistorBottom, resistorBias;
        std::vector<float> groundTop, groundBottom, groundBias;
    };

    struct TransistorConfig {
        std::string mux;
        std::reference_wrapper<const RD53BProdConstants::Register> reg;
        int offset;
    };

    static const std::vector<TransistorConfig> transistorSeq;

    static float idealityFactor(float temp, float deltaV) {
        static const double k = 1.602176634e-19 / (1.380649e-23 * std::log(15));
        return k * deltaV / temp;
    }

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;

  private:
    int fWPAC;

    float readTemp() {
        std::string reply;
        std::ostringstream cmd;

        cmd << std::hex << "RT," << param("wpacTempSens"_s) << "\n";
        ::write(fWPAC, cmd.str().c_str(), cmd.str().size());
        char c = '\0';
        while(c != '\n' && ::read(fWPAC, &c, 1) > 0) {
            reply.push_back(c);
        }
        return std::stof(reply.substr(2), nullptr);
    }
};

}

#endif
