#ifndef RD53DACScan_H
#define RD53DACScan_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53DACScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53DACScan<Flavor>> = make_named_tuple(std::make_pair("nVariables"_s, 1));

template <class Flavor>
struct RD53DACScan : public RD53BTool<RD53DACScan, Flavor>
{
    using Base = RD53BTool<RD53DACScan, Flavor>;
    using Base::Base;
    using Base::param;

    static constexpr const char* writeVar[] = {"VCAL_HIGH", "VCAL_MED", "REF_KRUM_LIN", "Vthreshold_LIN"};

    struct Results
    {
        double                           fitStart[9];
        double                           fitEnd[9];
        std::vector<std::vector<double>> VMUXvolt;
        std::vector<std::vector<double>> DACcode;
    };

    Results run() const;

    void draw(const Results& results);
};

} // namespace RD53BTools

#endif
