#ifndef RD53BREGTEST_H
#define RD53BREGTEST_H

#include <sstream>

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53BRegTest; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BRegTest<Flavor>> = make_named_tuple(
    std::make_pair("testRegs"_s, true),
    std::make_pair("testPixels"_s, true),
    std::make_pair("saveSuccesses"_s, false),
    std::make_pair("customVectors"_s, std::map<std::string, std::vector<uint16_t>>{}),
    std::make_pair("ServiceFrameSkip"_s, 50)
);

template <class Flavor>
struct RD53BRegTest : public RD53BTool<RD53BRegTest, Flavor>
{
    using Base = RD53BTool<RD53BRegTest, Flavor>;
    using Base::Base;

    std::stringstream run(Task progress) const;

    void draw(const std::stringstream& results) const;
};

} // namespace RD53BTools

#endif