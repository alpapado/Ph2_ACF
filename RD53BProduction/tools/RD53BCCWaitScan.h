#ifndef RD53BCCWAITSCAN_H
#define RD53BCCWAITSCAN_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BCCWaitScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BCCWaitScan<Flavor>> = make_named_tuple(
    std::make_pair("nPoints"_s, 1000),
    std::make_pair("configDelayUs"_s, 5e5),
    std::make_pair("sleepTimeUs"_s, 5000)
    //std::make_pair("delay"_s, 100),
    //std::make_pair("max_rx_data"_s, 1000),
    //std::make_pair("dummy_word"_s, 0xFF),
    //std::make_pair("do_shift"_s, true)
);

template <class Flavor>
struct RD53BCCWaitScan : public RD53BTool<RD53BCCWaitScan, Flavor> {
    using Base = RD53BTool<RD53BCCWaitScan, Flavor>;
    using Base::Base;

   bool run(Task progress) const;
    //void draw(const std::stringstream& results) const;
};

}

#endif
