#include "RD53BTemp.h"

#include <fstream>
#include <stdexcept>
#include <cmath>

namespace RD53BTools {

template<class Flavor> const std::vector<typename RD53BTemp<Flavor>::TransistorConfig> RD53BTemp<Flavor>::transistorSeq = {
    { "RADSENS_SLDOA", Flavor::Reg::MON_SENS_SLDO, 0 , "GNDA2" },
    { "TEMPSENS_SLDOA", Flavor::Reg::MON_SENS_SLDO, 0 , "GNDA2" },
    { "RADSENS_SLDOD", Flavor::Reg::MON_SENS_SLDO, 6 , "GNDA3" },
    { "TEMPSENS_SLDOD", Flavor::Reg::MON_SENS_SLDO, 6 , "GNDA3" },
    { "RADSENS_ACB", Flavor::Reg::MON_SENS_ACB, 0 , "GNDA4" },
    { "TEMPSENS_ACB", Flavor::Reg::MON_SENS_ACB, 0 , "GNDA4" },
};

template<class Flavor> const std::vector<std::array<std::string, 2>> RD53BTemp<Flavor>::resistorSeq = {
    {"RPOLYTSENS_TOP", "GNDA1"},
    {"RPOLYTSENS_BOTTOM", "GNDA0"}
};

template <class Flavor>
void RD53BTemp<Flavor>::init() {
    std::ifstream f("tempCalibWLT.dat");
    if(!f.is_open()) {
        throw std::runtime_error("Could not access calibration data");
    }
    LOG(INFO) << "Found temperature sensors calibration file" << RESET;
    float x;
    std::string s;
    while(f >> x && std::getline(f, s)) {
        coeff[s.substr(1)] = x; // trim whitespace
    }

    for(const std::string& s : param("enSens"_s)) {
        enSens.insert(s);
    }
}

template <class Flavor>
ChipDataMap<std::unordered_map<std::string, typename RD53BTemp<Flavor>::Result>> RD53BTemp<Flavor>::run() {
    Ph2_System::SystemController& system = Base::system();
    RD53BProdInterface<Flavor>& chipIface = Base::chipInterface();
    TCPClient& psIface = *system.fPowerSupplyClient;

    LOG(INFO) << "Configuring the source-meter unit" << RESET;
    psIface.sendAndReceivePacket(
        "VoltmeterSetRange,"
        "VoltmeterId:" + param("voltmeterId"_s) + ","
        "ChannelId:" + param("voltmeterChannelId"_s) + ","
        "Value:1.3"
    );

    std::string measCmd = "ReadVoltmeter,"
                          "VoltmeterId:" + param("voltmeterId"_s) + ","
                          "ChannelId:" + param("voltmeterChannelId"_s);

    ChipDataMap<std::unordered_map<std::string, Result>> data;
    for_each_device<Chip>(system, [&] (Chip* chip) {
        LOG(INFO) << "Reading data from chip" << RESET;
        /* transistor-based sensors */
        for(const TransistorConfig& conf : transistorSeq) {
            float meas[2];
            if(enSens.count(conf.mux) == 0) continue;
            float coeff = RD53BTemp<Flavor>::coeff.at(conf.mux);
            chipIface.WriteReg(chip, Flavor::Reg::MonitorConfig, bits::pack<6, 6>(Flavor::IMuxMap.at("HIGH_Z"), Flavor::VMuxMap.at(conf.mux)));
            for(int bias : { 0, 1 }) {
                chipIface.WriteReg(chip, conf.reg, bits::pack<1, 4, 1>(true, 0, bias) << conf.offset);
                meas[bias] = std::stof(psIface.sendAndReceivePacket(measCmd));
            }
            chipIface.WriteReg(chip, conf.reg, 0);
            data[chip][conf.mux].temperature = coeff * (meas[1] - meas[0]);
            LOG(INFO) << "Temperature for " << conf.mux << ": " << data[chip][conf.mux].temperature;
            data[chip][conf.mux].vLowBias = meas[0];
            data[chip][conf.mux].vHighBias = meas[1];
        }
        for(const TransistorConfig& conf : transistorSeq) {
            float ground[2];
            if(enSens.count(conf.mux) == 0) continue;
            chipIface.WriteReg(chip, Flavor::Reg::MonitorConfig, bits::pack<6, 6>(Flavor::IMuxMap.at("HIGH_Z"), Flavor::VMuxMap.at(conf.muxGND)));
            for(int bias : { 0, 1 }) {
                chipIface.WriteReg(chip, conf.reg, bits::pack<1, 4, 1>(true, 0, bias) << conf.offset);
                ground[bias] = std::stof(psIface.sendAndReceivePacket(measCmd));
            }
            chipIface.WriteReg(chip, conf.reg, 0);
            data[chip][conf.mux].vLowBias -= ground[0];
            data[chip][conf.mux].vHighBias -= ground[1];
        }
        /* resistor-based sensors */
        for(const std::array<std::string, 2> &mux : resistorSeq) {
            if(enSens.count(mux[0]) == 0) continue;
            chipIface.WriteReg(chip, Flavor::Reg::MonitorConfig, bits::pack<6, 6>(Flavor::IMuxMap.at("HIGH_Z"), Flavor::VMuxMap.at(mux[0])));
            float meas = std::stof(psIface.sendAndReceivePacket(measCmd));
            chipIface.WriteReg(chip, Flavor::Reg::MonitorConfig, bits::pack<6, 6>(Flavor::IMuxMap.at("HIGH_Z"), Flavor::VMuxMap.at(mux[1])));
            meas -= std::stof(psIface.sendAndReceivePacket(measCmd));
            data[chip][mux[0]].temperature = coeff.at("calibtemp") + (meas/coeff.at(mux[0]) - 1.0f) / 0.0022; // TODO give the constant a name
            LOG(INFO) << "Temperature for " << mux[0] << ": " << data[chip][mux[0]].temperature;
            data[chip][mux[0]].vLowBias = meas;
            data[chip][mux[0]].vHighBias = meas;
        }
    });
    return data;
}

template <class Flavor>
void RD53BTemp<Flavor>::draw(const ChipDataMap<std::unordered_map<std::string, typename RD53BTemp<Flavor>::Result>>& meas) {
    std::ofstream jsonFile(Base::getOutputFilePath("results.json"));
    jsonFile << "{\"chips\":[";
    const char* comma = "";
    for(const auto& p : meas) {
        jsonFile << comma << "{"; // data for one chip begins here
        jsonFile << "\"board\":" << p.first.board_id << ",";
        jsonFile << "\"hybrid\":" << p.first.hybrid_id << ",";
        jsonFile << "\"id\":" << p.first.chip_lane << ",";
        comma = "";
        for(const auto& q : p.second) {
            jsonFile << comma << "\"" << q.first << "\":" << q.second.temperature;
            comma = ",";
        }
        jsonFile << ",\"voltages\":{";
        comma = "";
        for(const auto& q : p.second) {
            jsonFile << comma << "\"" << q.first << "\":[";
            jsonFile << q.second.vLowBias << "," << q.second.vHighBias << "]";
            comma = ",";
        }
        jsonFile << "}"; // data for one chip ends here
    }
    jsonFile << "]}";
}

template class RD53BTemp<RD53BFlavor::ATLAS>;
template class RD53BTemp<RD53BFlavor::CMS>;
template class RD53BTemp<RD53BFlavor::ITkPixV2>;
template class RD53BTemp<RD53BFlavor::CROCv2>;

}
