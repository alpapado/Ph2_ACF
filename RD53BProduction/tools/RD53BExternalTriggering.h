#ifndef RD53BExternalTriggering_H
#define RD53BExternalTriggering_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53BExternalTriggering; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BExternalTriggering<Flavor>> = make_named_tuple(std::make_pair("triggerDuration"_s, 1ul));

template <class Flavor>
struct RD53BExternalTriggering : public RD53BTool<RD53BExternalTriggering, Flavor>
{
    using Base = RD53BTool<RD53BExternalTriggering, Flavor>;
    using Base::Base;
    using Base::param;

    bool run(Task progress) const
    {
        for(auto* board: *Base::system().fDetectorContainer)
        {
            auto& fwInterface   = Base::getFWInterface(board);
            auto& fastCmdConfig = *fwInterface.getLocalCfgFastCmd();

            fastCmdConfig.trigger_source   = TriggerSource::External;
            fastCmdConfig.n_triggers       = 0;
            fastCmdConfig.trigger_duration = param("triggerDuration"_s) - 1;

            fastCmdConfig.fast_cmd_fsm.ecr_en        = false;
            fastCmdConfig.fast_cmd_fsm.first_cal_en  = false;
            fastCmdConfig.fast_cmd_fsm.second_cal_en = false;
            fastCmdConfig.fast_cmd_fsm.trigger_en    = true;

            fastCmdConfig.fast_cmd_fsm.delay_after_trigger = param("triggerPeriod"_s);

            fwInterface.ConfigureFastCommands(&fastCmdConfig);

            fwInterface
        }

        RD53BEuaqProducer eudaqProducer(Base::system(), );
    }
};

} // namespace RD53BTools

#endif