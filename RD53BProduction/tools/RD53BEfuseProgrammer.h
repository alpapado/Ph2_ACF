#ifndef RD53BEFUSEPROGRAMMER_H
#define RD53BEFUSEPROGRAMMER_H

#include <string>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
class RD53BEfuseProgrammer;

template <class Flavour>
const auto ToolParameters<RD53BEfuseProgrammer<Flavour>> = make_named_tuple(
    std::make_pair("wpacPort"_s, std::string()),
    std::make_pair("word"_s, uint32_t{ 0 }), // 32-bit word to write in the efuses
    std::make_pair("readbackAttempts"_s, 1),
    std::make_pair("abortIfNonZero"_s, true),
    std::make_pair("width"_s, uint16_t{ 42 } ) // width of the Global Pulse to burn efuses
);

template <class Flavour>
class RD53BEfuseProgrammer : public RD53BTool<RD53BEfuseProgrammer, Flavour> {
  private:
    int fWPAC;

    void writeRegSure(RD53BProdInterface<Flavour>& iface, Chip* chip, const RD53BProdConstants::Register& reg, uint16_t val);

    uint32_t readEfuses(RD53BProdInterface<Flavour>& iface, Chip* chip);

    void writeEfuses(RD53BProdInterface<Flavour>& iface, Chip* chip, uint32_t word);

    /**
     * \brief Read the state of one I2C expander's lines on the WLT probe card
     * \param i2cAddr : I2C address of the expander
     * \return Byte with the state of the 8 output lines of the expander
     */
    unsigned long readExpander(unsigned i2cAddr = 0x21);

    /**
     * \brief Set the outputs of one I2C expander on the WLT probe card
     * \param data : Byte with the state of the 8 output lines of the expander
     * \param i2cAddr : I2C address of the expander
     * \return true if configuration was successful
     */
    bool writeExpander(unsigned long data, unsigned i2cAddr = 0x21);

  public:
    struct EfusesState {
        uint32_t before; ///> word in the efuses before programming
        uint32_t after; ///> word in the efuses after programming
    };

    using Base = RD53BTool<RD53BEfuseProgrammer, Flavour>;
    using Base::RD53BTool;
    using Base::param;

    const uint16_t EFUSES_READ = 0x0f0f;
    const uint16_t EFUSES_WRITE = 0xf0f0;

    ChipDataMap<EfusesState> run();

    void draw(const ChipDataMap<EfusesState>& eFuses) const;
};

}

#endif
