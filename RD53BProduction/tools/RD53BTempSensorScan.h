#ifndef RD53BTempSensorScan_H
#define RD53BTempSensorScan_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53BTempSensorScan : public RD53BTool<RD53BTempSensorScan, Flavor>
{
    using Base = RD53BTool<RD53BTempSensorScan, Flavor>;
    using Base::Base;

    struct SensorResults
    {
        std::vector<double>   voltages[2];
        std::vector<double>   ADC[2];
        std::vector<uint16_t> sensorConfigData[2];
    };

    using Result = std::map<std::string, SensorResults>;

    Result run(Task) const;

    void draw(const Result& results) const;
};

} // namespace RD53BTools

#endif
