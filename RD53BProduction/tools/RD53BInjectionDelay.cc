#include "RD53BInjectionDelay.h"

#include "Utils/xtensor/xindex_view.hpp"
#include "Utils/xtensor/xsort.hpp"

namespace RD53BTools
{
template <class Flavor>
ChipDataMap<typename RD53BInjectionDelay<Flavor>::Results> RD53BInjectionDelay<Flavor>::run(Task progress) const
{
    // ChipDataMap<pixel_matrix_t<Flavor, double>> injectionDelay;
    ChipDataMap<Results>                                                                 results;
    ChipDataMap<xt::xtensor_fixed<double, xt::xshape<Flavor::nRows, Flavor::nCols, 32>>> meanTriggerId;
    ChipDataMap<pixel_matrix_t<Flavor, size_t>>                                          triggerIdSum;
    ChipDataMap<pixel_matrix_t<Flavor, size_t>>                                          hitCount;

    auto& chipInterface = Base::chipInterface();

    Base::for_each_hybrid([&](Hybrid* hybrid) {
        chipInterface.WriteReg(hybrid, "VCAL_MED", param("vcalMed"_s));
        chipInterface.WriteReg(hybrid, "VCAL_HIGH", param("vcalMed"_s) + param("vcal"_s));
        chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", 0);
    });

    param("injectionTool"_s)
        .injectionScan(progress,
                       makeScan(ScanRange(32, [&, this](auto i) { Base::for_each_hybrid([&](Hybrid* hybrid) { chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", i); }); })),
                       [&, this](auto i, auto&& events) {
                           auto mask = param("injectionTool"_s).generateInjectionMask(i);
                           Base::for_each_chip([&](auto chip) {
                               auto hits = std::accumulate(events[chip].begin(), events[chip].end(), 0, [](auto a, auto b) { return a + b.hits.size(); });
                               std::cout << i << ": " << events[chip].size() << ", " << hits << std::endl;

                               for(size_t injDelay = 0; injDelay < 32; ++injDelay)
                               {
                                   triggerIdSum[chip].fill(0);
                                   hitCount[chip].fill(0);
                                   for(const auto& event: xt::view(events[chip], injDelay, xt::all()))
                                   {
                                       for(const auto& hit: event.hits)
                                       {
                                           triggerIdSum[chip](hit.row, hit.col) += event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                                           ++hitCount[chip](hit.row, hit.col);
                                       }
                                   }
                                   auto view = xt::view(meanTriggerId[chip], xt::all(), xt::all(), injDelay);
                                   view      = xt::where(mask, xt::cast<double>(triggerIdSum[chip]) / xt::maximum(hitCount[chip], 1ul), view);
                               }
                           });
                       });

    Base::for_each_chip([&](auto* chip) {
        results[chip].maxMeanTriggerId = xt::amax(meanTriggerId[chip], {2});
        results[chip].meanTriggerId    = xt::mean(meanTriggerId[chip], {2});
        // pixel_matrix_t<Flavor, size_t> minTriggerId = xt::amax(meanTriggerId[chip], {2});

        results[chip].injectionDelay = xt::fmod(32 - xt::argmax(xt::isclose(meanTriggerId[chip], xt::expand_dims(results[chip].maxMeanTriggerId, 2)), 2), 32);

        // auto filteredInjectionDelay = xt::filter(results[chip].injectionDelay, param("injectionTool"_s).usedPixels() && chip->injectablePixels() && results[chip].maxMeanTriggerId > minTriggerId);
        auto filteredInjectionDelay = xt::filter(results[chip].injectionDelay, param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        // auto minInjectionDelay = xt::amin(filteredInjectionDelay)();

        double meanInjectionDelay = xt::mean(filteredInjectionDelay)();

        std::cout << ChipLocation{chip} << std::endl;
        std::cout << "meanInjectionDelay = " << meanInjectionDelay << std::endl;

        std::cout << "Setting CalEdgeFineDelay = " << (32 - meanInjectionDelay + 5) << std::endl;

        // std::cout << "maxInjectionDelay = " << xt::amax(filteredInjectionDelay)() << std::endl;
        // std::cout << "minInjectionDelay = " << minInjectionDelay << std::endl;

        // size_t injDelay = 0;
        // for (; injDelay < 32; ++injDelay) {
        //     if (xt::count_nonzero(filteredInjectionDelay > injDelay)() < 0.99 * filteredInjectionDelay.size())
        //         break;
        // }
        // chipInterface.ConfigureReg(chip, "CalEdgeFineDelay", 32 - injDelay + 1);

        chipInterface.ConfigureReg(chip, "CalEdgeFineDelay", std::round(32 - meanInjectionDelay + 5));
    });

    return results;
}

template <class Flavor>
void RD53BInjectionDelay<Flavor>::draw(const ChipDataMap<Results>& results)
{
    Base::createRootFile();

    Base::for_each_chip([&](auto* chip) {
        Base::createRootFileDirectory(chip);

        Base::drawMap(results.at(chip).injectionDelay, "Injection Delay Map", "Injection Delay");

        auto filteredInjectionDelay = xt::filter(results.at(chip).injectionDelay, param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        Base::drawHist(filteredInjectionDelay, "Injection Delay Histogram", 32, 0, 32, "Injection Delay");

        Base::drawMap(results.at(chip).meanTriggerId, "Mean Trigger Id Delay Map", "Mean Trigger Id");

        auto filteredMeanTriggerId = xt::filter(results.at(chip).meanTriggerId, param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        Base::drawHist(filteredMeanTriggerId, "Mean Trigger Id Histogram", 32, 0, 32, "Mean Trigger Id");

        Base::drawMap(results.at(chip).maxMeanTriggerId, "Max Mean Trigger Id Map", "Max Mean Trigger Id");

        auto filteredMaxMeanTriggerId = xt::filter(results.at(chip).maxMeanTriggerId, param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        Base::drawHist(filteredMaxMeanTriggerId, "Max Mean Trigger Id Histogram", 32, 0, 32, "Max Mean Trigger Id");
    });
}

template class RD53BInjectionDelay<RD53BFlavor::ATLAS>;
template class RD53BInjectionDelay<RD53BFlavor::CMS>;
template class RD53BInjectionDelay<RD53BFlavor::ITkPixV2>;
template class RD53BInjectionDelay<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
