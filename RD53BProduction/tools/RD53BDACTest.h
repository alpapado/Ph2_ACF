#ifndef RD53BDACTest_H
#define RD53BDACTest_H

#include <array>
#include <chrono>
#include <cmath>
#include <fstream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

#include "RD53BTool.h"

#include "TAxis.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"


namespace RD53BTools
{
template <class>
class RD53BDACTest;

template <class F>
const auto ToolParameters<RD53BDACTest<F>> = make_named_tuple(std::make_pair("voltmeterId"_s, std::string("dacCalib")),
                                                              std::make_pair("voltmeterChannelId"_s, std::string("dacCalib")),
                                                              std::make_pair("voltmeterSamples"_s, 2),
                                                              std::make_pair("currentMuxResistance"_s, -1.0f), // value for IMUX_R
                                                              std::make_pair("vcalDelay"_s, 0.0f),
                                                              std::make_pair("keithSpeed"_s, 0.01f),
                                                              std::make_pair("n"_s, std::map<std::string, uint16_t>{}),
                                                              std::make_pair("add"_s, std::map<std::string, std::vector<uint16_t>>{}),
                                                              std::make_pair("target"_s, std::map<std::string, float>{}), // dictionary { DAC -> target value }
                                                              std::make_pair("slow"_s, false),
                                                              std::make_pair("gndsel"_s, -1),
                                                              std::make_pair("debug"_s, false),
                                                              std::make_pair("fitThresholdV"_s, INFINITY));

template <class F>
class RD53BDACTest : public RD53BTool<RD53BDACTest, F>
{
  private:
    struct MyComp {
        bool operator()(const RD53BProdConstants::Register& a, const RD53BProdConstants::Register& b) const {
            return (a < b) || ((a == b) && (a.name < b.name));
        }
    };

  public:
    struct DACData
    {
        std::vector<float> code, volt, voltErr;
    };

    using Base = RD53BTool<RD53BDACTest, F>;
    using Base::Base;
    using Base::param;
    using Results = ChipDataMap<std::map<RD53BProdConstants::Register, DACData, MyComp>>;

    void init()
    {
        nSamp    = param("voltmeterSamples"_s);
        vmetId   = param("voltmeterId"_s);
        vmetChId = param("voltmeterChannelId"_s);
        imuxR    = param("currentMuxResistance"_s);

        code["VCAL"] = {0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4095};
        for(const auto& p: testConds)
        {
            auto     nIt = param("n"_s).find(p.first);
            uint16_t n   = (nIt == param("n"_s).cend()) ? 4 : nIt->second;
            if(n < 2) throw std::logic_error("The number of points for " + p.first + " cannot be < 2");
            for(uint16_t i = 0; i < n; ++i)
            {
                float min, max;
                std::tie(min, max, std::ignore) = testConds.at(p.first);
                float c   = (max * i + min * (n - 1 - i)) / (n - 1);
                code[p.first].insert(std::round(c));
            }
            auto addIt = param("add"_s).find(p.first);
            if(addIt != param("add"_s).cend()) code[p.first].insert(addIt->second.cbegin(), addIt->second.cend());
        }

        Base::system().fPowerSupplyClient->sendAndReceivePacket(
            "K2410:SetSpeed"
            ",PowerSupplyId:" + vmetId +
            ",ChannelId:" + vmetChId +
            ",IntegrationTime:" + std::to_string(param("keithSpeed"_s))
        );
    }

    Results run();
    void draw(const Results& results);

    using calibSeqType = std::vector<std::vector<std::reference_wrapper<const RD53BProdConstants::Register>>>;
    using testCondsType = std::map<std::string, std::tuple<uint16_t, uint16_t, std::map<std::string, uint16_t>>>;
    using regToFuncType = std::map<const RD53BProdConstants::Register*, std::string>;
    using muxSettType = std::map<const RD53BProdConstants::Register*, std::pair<typename F::IMux, typename F::VMux>>;

  private:
    static const calibSeqType calibSeq;
    static const testCondsType testConds;

    static const regToFuncType& regToFunc;

    static const muxSettType& muxSett;

    std::map<std::string, std::set<uint16_t>> code;

    size_t      nSamp;
    float       imuxR;
    std::string vmetId, vmetChId;

    bool isCurr(const Ph2_HwDescription::RD53BProdConstants::Register& dac) const
    {
        //typename F::VMux vmux;
        for (const auto& p : muxSett)
        {
            if (dac.address == p.first->address) return p.second.second == F::VMux::IMUX_OUT;
        }
        // TODO throw exception?
        return false;
    }

    void calibChip(Chip* chip,
                   std::map<RD53BProdConstants::Register, DACData, MyComp>& res,
                   const std::vector<RD53BProdConstants::Register>& dac) {
        std::vector<std::reference_wrapper<const RD53BProdConstants::Register>> dac_wrap;
        dac_wrap.reserve(dac.size());
        for (const RD53BProdConstants::Register& reg : dac) dac_wrap.emplace_back(reg);
        calibChip(chip, res, dac_wrap);
    }

    void calibChip(Chip* chip,
                   std::map<RD53BProdConstants::Register, DACData, MyComp>& res,
                   const std::vector<std::reference_wrapper<const RD53BProdConstants::Register>>& dac) {
        using RD53BProdConstants::Register;

        auto&      chipIface = *static_cast<RD53BProdInterface<F>*>(Base::system().fReadoutChipInterface);
        TCPClient& psIface   = *Base::system().fPowerSupplyClient;

        std::ostringstream psReq;
        psReq = std::ostringstream("ReadVoltmeter,VoltmeterId:", std::ostringstream::ate);
        psReq << vmetId << ",ChannelId:" << vmetChId;
        if(nSamp > 1) psReq << ",Stats,N:" << nSamp;

        std::map<const Register, std::set<uint16_t>> code;
        for(const Register& reg: dac) code[reg] = this->code.at(regToFunc.at(&reg));

        psIface.sendAndReceivePacket("VoltmeterSetRange"
                                     ",VoltmeterId:" +
                                     vmetId + ",ChannelId:" + vmetChId + ",Value:1.3");

        std::map<Register, uint16_t> original;
        for(const Register& reg : dac) original[reg] = chipIface.ReadReg(chip, reg);

        std::vector<uint16_t> cmdQ;
        while(true)
        {
            // setting all the DACs
            cmdQ.clear();
            for(std::pair<const Register, std::set<uint16_t>>& p: code)
            {
                if(p.second.empty()) continue;

                chipIface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdQ);
                chipIface.template SerializeCommand<RD53BCmd::WrReg>(chip, cmdQ, p.first.address, *p.second.begin());
            }

            if(cmdQ.size() == 0) break;

            chipIface.SendCommandStream(chip, cmdQ);
            auto start = std::chrono::steady_clock::now(); ///> for VCAL delay
            // all DACs set

            // use IMUX_R with no current to measure GNDA_REF1
            chipIface.WriteReg(chip, F::Reg::MonitorConfig, bits::pack<6, 6>(
                static_cast<uint8_t>(F::IMux::HIGH_Z), static_cast<uint8_t>(F::VMux::IMUX_OUT)
            ));
            std::istringstream psResp(psIface.sendAndReceivePacket(psReq.str()));
            float gnd;
            psResp >> gnd;
            if(!psResp) continue;

            // measuring all DAC outputs
            for(const Register& reg: dac)
            {
                if(code.at(reg).empty()) continue;

                typename F::IMux imux;
                typename F::VMux vmux;
                std::tie(imux, vmux) = muxSett.at(&reg);
                chipIface.WriteChipReg(chip, "MonitorConfig", bits::pack<6, 6>(static_cast<uint8_t>(imux), static_cast<uint8_t>(vmux)));
                if(reg == F::Reg::VCAL_HIGH || reg == F::Reg::VCAL_MED) { std::this_thread::sleep_until(start + std::chrono::duration<float>(param("vcalDelay"_s))); }
                if(param("slow"_s)) std::this_thread::sleep_for(std::chrono::seconds(1));
                std::istringstream psResp(psIface.sendAndReceivePacket(psReq.str()));
                float              a, b;
                psResp >> a;
                if(nSamp > 1) psResp >> b;
                if(psResp)
                {
                    a -= gnd;
                    if(isCurr(reg))
                    {
                        a /= imuxR;
                        b /= imuxR;
                    }
                    res[reg].code.push_back(*code.at(reg).cbegin());
                    res[reg].volt.push_back(a);
                    if(nSamp > 1) res[reg].voltErr.push_back(b / std::sqrt(nSamp));
                }
            }
            for(std::pair<const Register, std::set<uint16_t>>& p: code)
            {
                if(!p.second.empty()) p.second.erase(p.second.begin());
            }
            // all DAC outputs measured for a single configuration
        }
        // all codes tested for all DACs

        for(const auto& p : original) {
            chipIface.WriteReg(chip, p.first, p.second);
        }
    }
};

} // namespace RD53BTools

#endif
