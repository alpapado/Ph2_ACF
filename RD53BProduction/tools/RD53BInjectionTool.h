#ifndef RD53BINJECTIONTOOL_H
#define RD53BINJECTIONTOOL_H

#include "HWDescription/RD53BProd.h"
#include "RD53BTool.h"
#include "Utils/RD53BEventDecoding.h"

#include "Utils/xtensor/xview.hpp"
#include "Utils/xtensor/xadapt.hpp"

class TH1;

namespace RD53BTools
{
template <class>
struct RD53BInjectionTool; // forward declaration

using MaskStep =
    decltype(make_named_tuple(std::make_pair("dim"_s, size_t{}), std::make_pair("size"_s, size_t{}), std::make_pair("parallel"_s, bool{}), std::make_pair("shift"_s, std::vector<size_t>{})));

template <class Flavor>
const auto ToolParameters<RD53BInjectionTool<Flavor>> = make_named_tuple(std::make_pair("nInjections"_s, 10ul),
                                                                         std::make_pair("triggerDuration"_s, 10ul),
                                                                         std::make_pair("injectionDuration"_s, 1ul),
                                                                         std::make_pair("triggerLatency"_s, 133u),
                                                                         std::make_pair("injectionType"_s, std::string("Analog")),
                                                                         std::make_pair("delayAfterBCIDReset"_s, 100),
                                                                         std::make_pair("delayAfterPrime"_s, 100),
                                                                         std::make_pair("delayAfterInject"_s, 32),
                                                                         std::make_pair("delayAfterTrigger"_s, 800),
                                                                         std::make_pair("injectionDelay160MHz"_s, 0u),
                                                                         std::make_pair("pulseDuration"_s, 6u),
                                                                         std::make_pair("offset"_s, std::vector<size_t>({0, 0})),
                                                                         std::make_pair("size"_s, std::vector<size_t>({0, 0})),
                                                                         std::make_pair("frameStep"_s, 1ul),
                                                                         std::make_pair("disableUnusedPixels"_s, true),
                                                                         std::make_pair("injectUnusedPixels"_s, false),
                                                                         std::make_pair("injectionRowOffset"_s, 0),
                                                                         std::make_pair("injectionColOffset"_s, 0),
                                                                         std::make_pair("decoderThreads"_s, 1u),
                                                                         std::make_pair("maxEventQueueSize"_s, 1u),
                                                                         std::make_pair("compressHitMap"_s, true),
                                                                         std::make_pair("enableToTReadout"_s, true),
                                                                         std::make_pair("enableChipIdReadout"_s, false),
                                                                         std::make_pair("enableBCIDReadout"_s, false),
                                                                         std::make_pair("enableL1IDReadout"_s, false),
                                                                         std::make_pair("resetBCID"_s, false),
                                                                         std::make_pair("useHitOr"_s, false),
                                                                         std::make_pair("useSelfTriggers"_s, false),
                                                                         std::make_pair("selfTriggerDelay"_s, 121u),
                                                                         std::make_pair("readoutTimeoutSeconds"_s, 5.0),
                                                                         std::make_pair("maxFailedAttempts"_s, 10u),
                                                                         std::make_pair("debugMode"_s, false),
                                                                         std::make_pair("maskGen"_s, std::vector<MaskStep>({MaskStep(0, 0, 0, {8, 1}), MaskStep(1, 0, 1, {})})));

struct ScanRange
{
    ScanRange() {}

    template <class F>
    ScanRange(size_t size, F&& f) : _size(size), _f(std::forward<F>(f))
    {
    }

    size_t size() const { return _size; }

    void apply(size_t i) { _f(i); }

  private:
    size_t                   _size;
    std::function<void(int)> _f;
};

template <class... Ts>
auto makeScan(Ts&&... args)
{
    return std::array<ScanRange, sizeof...(Ts)>{std::forward<Ts>(args)...};
}

struct OccupancyData
{
    xt::xarray<double> raw;
    xt::xarray<double> filtered;
};

template <class Flavor>
struct RD53BInjectionTool : public RD53BTool<RD53BInjectionTool, Flavor>
{
    using Base = RD53BTool<RD53BInjectionTool, Flavor>;
    using Base::Base;
    using Base::param;

    using ChipEventsMap = ChipDataMap<std::vector<RD53BEventDecoding::RD53BEvent>>;

    void init();

    ~RD53BInjectionTool<Flavor>();

    ChipEventsMap run(Task progress) const;

    template <size_t N, class A>
    void injectionScan(Task progress, const std::array<ScanRange, N>& scanRanges, A&& analyzer) const;

    ChipDataMap<pixel_matrix_t<Flavor, double>> occupancy(const ChipEventsMap& result) const;

    ChipDataMap<std::array<size_t, 16>> totDistribution(const ChipEventsMap& result) const;

    size_t nFrames() const { return _nFrames; }

    size_t nEvents() const { return param("nInjections"_s) * param("injectionDuration"_s) * param("triggerDuration"_s); }

    void draw(const ChipEventsMap& result);

    pixel_matrix_t<Flavor, bool> usedPixels() const;

    pixel_matrix_t<Flavor, bool> generateInjectionMask(size_t i) const;

    void configureInjections() const;

    const auto& offset(size_t i) const { return param("offset"_s)[i]; }
    const auto& size(size_t i) const { return param("size"_s)[i]; }
    auto        rowRange() const { return xt::xrange<std::ptrdiff_t>(offset(0), offset(0) + size(0)); }
    auto        colRange() const { return xt::xrange<std::ptrdiff_t>(offset(1), offset(1) + size(1)); }

  private:
    void setupMaskFrame(size_t frameId) const;
    void ResetChipCounters() const;
    void print_debug_info(const std::vector<uint32_t>& data, const typename Flavor::FormatOptions& options) const;

    template <size_t N>
    std::array<size_t, N> index2coords(size_t i, const std::array<size_t, N>& sizes) const;

    size_t _nFrames;
};

template <class Flavor>
template <size_t N>
inline std::array<size_t, N> RD53BInjectionTool<Flavor>::index2coords(size_t i, const std::array<size_t, N>& sizes) const
{
    std::array<size_t, N> p{0};
    for(int dim = N - 1; dim >= 0; --dim)
    {
        p[dim] = i % sizes[dim];
        i      = i / sizes[dim];
    }
    return p;
}

template <class Flavor>
inline auto getFormatOptions(bool compressHitMap, bool enableToTReadout, bool enableChipIdReadout, bool enableBCIDReadout, bool enableL1IDReadout);

template <>
inline auto getFormatOptions<RD53BFlavor::ATLAS>(bool compressHitMap, bool enableToTReadout, bool enableChipIdReadout, bool enableBCIDReadout, bool enableL1IDReadout)
{
    RD53BFlavor::ATLAS::FormatOptions options;
    options.compressHitMap = compressHitMap;
    options.enableToT      = enableToTReadout;
    options.enableChipId   = enableChipIdReadout;
    return options;
}

template <>
inline auto getFormatOptions<RD53BFlavor::CMS>(bool compressHitMap, bool enableToTReadout, bool enableChipIdReadout, bool enableBCIDReadout, bool enableL1IDReadout)
{
    RD53BFlavor::CMS::FormatOptions options;
    options.compressHitMap  = compressHitMap;
    options.enableToT       = enableToTReadout;
    options.enableChipId    = enableChipIdReadout;
    options.enableBCID      = enableBCIDReadout;
    options.enableTriggerId = enableL1IDReadout;
    return options;
}

template <>
inline auto getFormatOptions<RD53BFlavor::ITkPixV2>(bool compressHitMap, bool enableToTReadout, bool enableChipIdReadout, bool enableBCIDReadout, bool enableL1IDReadout)
{
    return getFormatOptions<RD53BFlavor::ATLAS>(compressHitMap, enableToTReadout, enableChipIdReadout, enableBCIDReadout, enableL1IDReadout);
}

template <>
inline auto getFormatOptions<RD53BFlavor::CROCv2>(bool compressHitMap, bool enableToTReadout, bool enableChipIdReadout, bool enableBCIDReadout, bool enableL1IDReadout)
{
    return getFormatOptions<RD53BFlavor::CMS>(compressHitMap, enableToTReadout, enableChipIdReadout, enableBCIDReadout, enableL1IDReadout);
}

// General N-dimensional injection scan.
// The scan over the injection pattern is allways the first dimension.
template <class Flavor>
template <size_t N, class A>
inline void RD53BInjectionTool<Flavor>::injectionScan(Task progress, const std::array<ScanRange, N>& scanRanges, A&& analyzer) const
{
    configureInjections();

    auto options = getFormatOptions<Flavor>(param("compressHitMap"_s), param("enableToTReadout"_s), param("enableChipIdReadout"_s), param("enableBCIDReadout"_s), param("enableL1IDReadout"_s));

    // AsyncEventDecoder is a thread pool that can decode events asynchronously
    RD53BEventDecoding::AsyncEventDecoder<Flavor> decoder{param("decoderThreads"_s), options};

    std::array<ScanRange, N + 1> ranges;
    ranges[0] = ScanRange(_nFrames, [&](auto i) { setupMaskFrame(i); });
    std::copy(scanRanges.begin(), scanRanges.end(), ranges.begin() + 1);

    // the size of each dimension of the scan
    std::array<size_t, N + 1> scan_shape;
    std::transform(ranges.begin(), ranges.end(), scan_shape.begin(), [](const auto& r) { return r.size(); });

    std::vector<size_t> result_shape;
    if(N == 0)
        result_shape = {nEvents()};
    else
        result_shape = {scan_shape[N], nEvents()};

    std::map<size_t, ChipDataMap<xt::xarray<RD53BEventDecoding::RD53BEvent>>> events;

    std::array<size_t, N + 1> scan_strides;
    std::partial_sum(scan_shape.crbegin(), scan_shape.crend() - 1, scan_strides.rbegin() + 1, std::multiplies<>());
    scan_strides[N] = 1;

    // get the total size of the scan
    size_t size = std::accumulate(scan_shape.begin(), scan_shape.end(), 1, std::multiplies<>());

    size_t pointsPerAnalysis = N == 0 ? 1 : scan_shape[N];

    // tracks how many points have been completed to update the progress bar
    size_t nDone = 0;

    // tracks which points are completed
    std::vector<bool> done(size, false);

    // std::vector<uint8_t> nFailedAttempts(size, 0);
    size_t nFailedAttempts = 0;

    size_t nAnalyses = size / pointsPerAnalysis;

    // tracks which frames of the injection pattern have been analyzed
    std::vector<bool> analyzed(nAnalyses, false);

    // maps point ids to futures that can be used to get the corresponding events once decoded
    std::map<size_t, std::future<RD53BEventDecoding::DecodingResult>> decodingFutures;

    for(size_t dim = 0; dim < ranges.size(); ++dim) ranges[dim].apply(0);

    // last scan coords applied
    std::array<size_t, N + 1> last_coords = {0};

    // main loop
    while(!std::all_of(analyzed.begin(), analyzed.end(), [](auto x) { return x; }))
    {
        auto t0 = std::chrono::steady_clock::now();

        if(!std::all_of(done.begin(), done.end(), [](auto x) { return x; }))
        {
            for(size_t i = 0; i < size; ++i)
            {
                if(!done[i] && decodingFutures.find(i) == decodingFutures.end())
                {
                    auto scan_coords = index2coords(i, scan_shape);

                    size_t minDim = 0;
                    for(; minDim < N + 1; ++minDim)
                        if(last_coords[minDim] != scan_coords[minDim]) break;

                    for(size_t dim = minDim; dim < N + 1; ++dim) ranges[dim].apply(scan_coords[dim]);

                    last_coords = scan_coords;

                    std::vector<uint32_t> data;

                    ResetChipCounters();
                    
                    // Reset TriggerID of each chip
                    // Base::for_each_chip([&, this] (auto* chip) {
                    //     // Base::chipInterface().template SendCommand<RD53BCmd::Clear>(chip);
                    //     Base::chipInterface().SendGlobalPulse(chip, {"ResetCounters"}, 16);
                    // });
                    
                    auto* board = Base::system().fDetectorContainer->at(0);

                    size_t nEventsReceived = 0;
                    try
                    {
                        nEventsReceived = Base::getFWInterface(board).GetEventData(board, data, param("readoutTimeoutSeconds"_s));
                    }
                    catch(std::exception& e)
                    {
                        LOG(ERROR) << "Exception occurred during injection round # " << i << ":\n" << e.what();
                    }

                    if(param("debugMode"_s))
                        print_debug_info(data, options);

                    if(nEventsReceived == nEvents()) { 
                        decodingFutures.insert({i, decoder.decode_events(std::move(data))}); 
                    }
                    else if (nEventsReceived > nEvents()) {
                        auto event_start = RD53BEventDecoding::event_start(data);
                        decodingFutures.insert({i, decoder.decode_events({data.begin(), data.begin() + event_start[nEvents()]})}); 
                    }
                    else
                    {
                        ++nFailedAttempts;
                        LOG(INFO) << "Error occured during injection round: " << std::to_string(i);
                        LOG(INFO) << "Failed readout attempts: " << nFailedAttempts << "/" << param("maxFailedAttempts"_s);
                        if(nFailedAttempts >= param("maxFailedAttempts"_s)) throw std::runtime_error("Too many failed attempts");
                    }

                    if(decodingFutures.size() > param("maxEventQueueSize"_s)) break;
                }
            }

            // process decoded events
            for(auto it = decodingFutures.begin(); it != decodingFutures.end();)
            {
                auto& future = it->second;
                if(future.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
                {
                    auto result = future.get();

                    auto scan_coords = index2coords(it->first, scan_shape);

                    if(result)
                    {
                        // group events by chip
                        ChipEventsMap events_local;
                        for(auto& event: result.value())
                        {
                            // const int c = event.l1a_counter;
                            // const int d = param("triggerDuration"_s);
                            // int expected_tag = (c / d * (1 + d / 4) + (c % d + 1) / 4) % 31;
                            // if (expected_tag != event.triggerTag)
                            //     LOG(WARNING) << "Trigger tag and trigger counter mismatch (tag: " << event.triggerTag << ", l1a_cnt: " << event.l1a_counter << ", expected tag: " << expected_tag << ")";
                            for(auto& chipEvent: event.events)
                            { events_local[{chipEvent.hybridId, chipEvent.chipLane}].emplace_back(event.l1a_counter, chipEvent.BCID, chipEvent.triggerTag, std::move(chipEvent.hits)); }
                        }
                        // check if we have the right number of events for each chip
                        done[it->first] = true; // assume all ok
                        Base::for_each_chip([&](auto chip) {
                            size_t nEventsLocal = events_local[chip].size();
                            if(nEventsLocal != nEvents())
                            {
                                LOG(WARNING) << "Got " << nEventsLocal << "/" << nEvents() << " events for injection round #" << it->first << ". Repeating...";
                                done[it->first] = false; // not ok
                            }
                        });

                        if(done[it->first])
                        {
                            // move current batch of events into the overall event storage

                            std::array<size_t, std::max(N, 1ul)> strides;
                            std::transform(scan_coords.begin(), scan_coords.end() - bool(N), scan_strides.begin(), strides.begin(), std::multiplies<>());
                            size_t index = std::accumulate(strides.begin(), strides.end(), 0) / pointsPerAnalysis;

                            auto jt = events.find(index);
                            if(jt == events.end())
                            {
                                jt = events.insert({index, {}}).first;
                                Base::for_each_chip([&](auto chip) { jt->second.insert({chip, xt::empty<RD53BEventDecoding::RD53BEvent>(result_shape)}); });
                            }

                            Base::for_each_chip([&](auto chip) {
                                if(N > 0)
                                    std::move(events_local[chip].begin(), events_local[chip].end(), xt::view(jt->second[chip], scan_coords[N], xt::all()).begin());
                                else
                                    std::move(events_local[chip].begin(), events_local[chip].end(), jt->second[chip].begin());
                            });
                            progress.update(++nDone / (double)size);
                        }
                    }
                    else
                    {
                        std::ostringstream oss;
                        oss << "Decoding error occured during injection ( ";
                        for(auto c: scan_coords) oss << c << " ";
                        oss << "):\n" << result.error();
                        LOG(ERROR) << BOLDRED << oss.str() << RESET;

                        ++nFailedAttempts;
                        LOG(INFO) << "Failed readout attempts: " << nFailedAttempts << "/" << param("maxFailedAttempts"_s);
                        if(nFailedAttempts > param("maxFailedAttempts"_s)) throw std::runtime_error("Too many failed attempts for injection round # " + std::to_string(it->first));
                    }

                    it = decodingFutures.erase(it);
                }
                else
                    ++it;
            }
        }

        // analyze events for pixels belonging to frames of the injection pattern that have been completed
        for(size_t i = 0; i < nAnalyses; ++i)
        {
            if(!analyzed[i] && std::all_of(done.begin() + i * pointsPerAnalysis, done.begin() + (i + 1) * pointsPerAnalysis, [](auto x) { return x; }))
            {
                std::forward<A>(analyzer)(i, std::move(events.at(i)));
                events.erase(i);
                analyzed[i] = true;
            }
        }

        std::this_thread::sleep_until(t0 + std::chrono::microseconds(100));
    }

    // reset masks
    Base::for_each_chip([&](Chip* chip) {
        Base::chipInterface().UpdatePixelConfig(chip, true, false);
        Base::chipInterface().ChipErrorReport(static_cast<ReadoutChip*>(chip));
        // events[chip].resize(result_shape);
    });

    // return events;
}

} // namespace RD53BTools

#endif