#include "RD53BUplinkCalibration.h"

#include <iostream>

namespace RD53BTools
{
template <class Flavor>
ChipDataMap<std::vector<size_t>> RD53BUplinkCalibration<Flavor>::run() const
{
    auto& chipInterface = Base::chipInterface();

    ChipDataMap<std::vector<size_t>> serviceBlockCounts;

    auto tap0Bins = xt::arange<size_t>(param("tap0Range"_s)[0], param("tap0Range"_s)[1], param("tap0Step"_s));

    //Base::for_each_chip([&](auto chip) { chipInterface.WriteReg(chip, "EnServiceData", 0); });
    Base::for_each_chip([&](auto chip) { chipInterface.WriteReg(chip, "ServiceDataConf",50); });

    Base::for_each_board([&, this](auto board) {
        auto& fwInterface = Base::getFWInterface(board);

        RD53BUtils::for_each_device<Chip>(board, [&](auto chip) {
            auto rd53b = static_cast<RD53BProd<Flavor>*>(chip);
            if(!rd53b->isPrimary())
            {
		        fwInterface.WriteReg("user.ctrl_regs.Aurora_block.error_cntr_chip_addr", rd53b->getPrimary());

                chipInterface.WriteReg(chip, "EnServiceData", 1);

                for(auto i = 0u; i < tap0Bins.size(); ++i)
                {
                    chipInterface.WriteReg(chip, "DAC_CML_BIAS_0", tap0Bins[i]);

                    fwInterface.WriteReg("user.ctrl_regs.Aurora_block.rst_frame_cntr", 1);

                    fwInterface.WriteReg("user.ctrl_regs.Aurora_block.rst_frame_cntr", 0);

                    fwInterface.WriteReg("user.ctrl_regs.Aurora_block.start_frame_cntr", 1);

                    fwInterface.WriteReg("user.ctrl_regs.Aurora_block.start_frame_cntr", 0);

                    usleep(300000);

                    while(fwInterface.ReadReg("user.stat_regs.aurora_frame_cntr") < param("nFrames"_s)) usleep(10000);

                    serviceBlockCounts[chip].push_back(fwInterface.ReadReg("user.stat_regs.aurora_service_blk_cntr"));
                }

		for(auto i = 0u; i < tap0Bins.size(); ++i)
			std::cout << "i: " << i << ", count: " << serviceBlockCounts[chip][i] << std::endl;

                auto it = std::max_element(serviceBlockCounts[chip].begin(), serviceBlockCounts[chip].end());

                auto max_range = std::equal_range(it, serviceBlockCounts[chip].end(), *it);

                it += (max_range.second - max_range.first) / 2;

                auto bestTap0 = tap0Bins(it - serviceBlockCounts[chip].begin());

                chipInterface.ConfigureReg(static_cast<RD53BProd<Flavor>*>(chip), "DAC_CML_BIAS_0", bestTap0);

                LOG(INFO) << "Best TAP0 setting for chip " << ChipLocation{chip} << " is " << +bestTap0;

                chipInterface.WriteReg(chip, "EnServiceData", 0);
            }
        });
    });

    Base::for_each_chip([&](auto chip) { chipInterface.WriteReg(chip, "EnServiceData", 1); });

    return serviceBlockCounts;
}

template <class Flavor>
void RD53BUplinkCalibration<Flavor>::draw(const ChipDataMap<std::vector<size_t>>& serviceBlockCounts)
{
    Base::createRootFile();

    for(const auto& item: serviceBlockCounts)
    {
        auto chip = item.first;
        auto data = item.second;

        Base::createRootFileDirectory(chip);

        // std::cout << xt::adapt(data) << std::endl;

        Base::drawHistRaw(data, "Service Block Counts", param("tap0Range"_s)[0], param("tap0Range"_s)[1], "TAP0", "Service Block Count");
    }
}

template class RD53BUplinkCalibration<RD53BFlavor::ATLAS>;
template class RD53BUplinkCalibration<RD53BFlavor::CMS>;
template class RD53BUplinkCalibration<RD53BFlavor::ITkPixV2>;
template class RD53BUplinkCalibration<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
