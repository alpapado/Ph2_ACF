#include "RD53BFWRegReader.h"

namespace RD53BTools
{
template <class Flavor>
bool RD53BFWRegReader<Flavor>::run(Task progress) const
{
    for(auto* board: *Base::system().fDetectorContainer)
    {
        auto& fwInterface = Base::getFWInterface(board);

        auto hwInterface = fwInterface.getHardwareInterface();

        // hwInterface->getNode().stream(std::cout);

        for(const auto& path: hwInterface->getNodes("user.+"))
        {
            auto& node = hwInterface->getNode(path);
            if(node.getMode() == uhal::defs::BlockReadWriteMode::SINGLE && (int)node.getPermission() & 1 && ++node.begin() == node.end())
                LOG(INFO) << node.getPath() << " = " << fwInterface.ReadReg(path);
        }

        // for (const auto& node : hwInterface->getNode("user")) {
        //     auto path = node.getPath();
        //     LOG(INFO) << path << " = " << fwInterface.ReadReg(path);
        // }
    }
    return true;
}

template class RD53BFWRegReader<RD53BFlavor::ATLAS>;
template class RD53BFWRegReader<RD53BFlavor::CMS>;
template class RD53BFWRegReader<RD53BFlavor::ITkPixV2>;
template class RD53BFWRegReader<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
