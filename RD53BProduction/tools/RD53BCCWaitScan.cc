#include "RD53BCCWaitScan.h"

#include <string>

namespace RD53BTools {

template <class Flavor>
bool RD53BCCWaitScan<Flavor>::run(Task progress) const {
    //setup
    Ph2_System::SystemController& system = Base::system();
    auto& chipInterface = Base::chipInterface();
    ChipDataMap<uint16_t> ccWaitRegValues;

    //saving the previous chip configuration for AuroraConfig
    for_each_device<Chip>(system, [&] (Chip* chip) {
        ccWaitRegValues.insert({chip, chipInterface.ReadReg(chip, "CCWait")});
        //chipInterface.WriteReg(chip, Flavor::Reg::DataMerging, 317);
    });

    uint32_t nPoints = Base::param("nPoints"_s);
    uint32_t configDelayUs = Base::param("configDelayUs"_s);
    uint16_t sleepTimeUs = Base::param("sleepTimeUs"_s);

    uint16_t iErrors = 0;
    uint16_t nErrors[64];

    //looping on all devices to perform the test
    for_each_device<Chip>(system, [&](DeviceChain devices) {
        auto& fwInterface = Base::getFWInterface(devices.board);
        auto chip = static_cast<RD53BProd<Flavor>*>(devices.chip);

        //variable for holding the Aurora status
        uint8_t channelStatus = 0;

        //looping
        for (uint8_t i = 0; i < 64; i++){
            //setting CCWait
            LOG(INFO) << "Setting CCWait to " << std::to_string(i) << RESET;
            chipInterface.WriteReg(chip, "CCWait", i);
            usleep(configDelayUs);

            //performing several tries to measure the Aurora channel status
            iErrors = 0;
            for (uint32_t j = 0; j < nPoints; j++) {
                //reading the channel status
                channelStatus = fwInterface.ReadReg("user.stat_regs.aurora_rx_channel_up");
                if (channelStatus != 1) {
                    iErrors++;
                    //LOG(INFO) << BOLDRED << "Channel status: "
                    //    << std::to_string(channelStatus) << " (CCWAIT = "
                    //    << std::to_string(i) << "; j = " << std::to_string(j)
                    //    <<  ")"  << RESET;
                }
                //sleep for 5 milliseconds and retry
                usleep(sleepTimeUs);
            }
            if (iErrors == 0) {
                LOG(INFO) << BOLDGREEN << "No errors" << RESET;
            } else {
                LOG(INFO) << BOLDRED << "Number of errors: "
                    << std::to_string(iErrors) << "/" << std::to_string(nPoints)
                    << RESET;
                nErrors[i] = iErrors;
            }

        }

    });

    //restoring the previous configuration
    for_each_device<Chip>(system, [&] (Chip* chip) {
        chipInterface.WriteReg(
            chip,
            "CCWait",
            ccWaitRegValues[chip]
        );
    });

    //printing the results
    //LOG(INFO) << "Problematic values of CCWait found:" << RESET;
    //for (uint8_t i = 0; i < 64; i++) {
    //    if (nErrors[i] != 0) {
    //        LOG(INFO) << std::to_string(i) << ": " << std::to_string(nErrors[i])
    //            <<  " errors" << RESET;
    //    }

    //};

    return true;
}

//template <class Flavor>
//void RD53BCCWaitScan<Flavor>::draw(const std::stringstream& results) const {
//    std::ofstream csvFile(Base::getOutputFilePath("results.csv"));
//    csvFile << results.rdbuf();
//}

template class RD53BCCWaitScan<RD53BFlavor::ATLAS>;
template class RD53BCCWaitScan<RD53BFlavor::CMS>;
template class RD53BCCWaitScan<RD53BFlavor::ITkPixV2>;
template class RD53BCCWaitScan<RD53BFlavor::CROCv2>;

}
