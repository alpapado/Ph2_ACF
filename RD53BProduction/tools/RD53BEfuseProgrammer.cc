#include "RD53BEfuseProgrammer.h"

#include <stdexcept>
#include <string>
#include <regex>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include "RD53BTool.h"

namespace RD53BTools {

template <class F>
void RD53BEfuseProgrammer<F>::writeRegSure(RD53BProdInterface<F>& iface, Chip* chip, const RD53BProdConstants::Register& reg, uint16_t val) {
    bool done = false;
    int n = param("readbackAttempts"_s);
    while(!done && n > 0) {
        iface.WriteReg(chip, reg, val);
        done = (iface.ReadReg(chip, reg, true) == val);
        --n;
    }
    if(!done) throw std::runtime_error("Failed to set register " + reg.name + " to " + std::to_string(val) + ".");
}

template <class F>
uint32_t RD53BEfuseProgrammer<F>::readEfuses(RD53BProdInterface<F>& iface, Chip* chip) {
    writeRegSure(iface, chip, F::Reg::GlobalPulseConf, 1u << RD53BProd<F>::GlobalPulseRoutes.at("ResetEfuses"));
    writeRegSure(iface, chip, F::Reg::GlobalPulseWidth, 4 / F::globalPulseUnit);
    iface.template SendCommand<RD53BCmd::GlobalPulse>(chip);
    writeRegSure(iface, chip, F::Reg::EfusesConfig, EFUSES_READ);
    uint32_t rb = bits::pack<16, 16>(
        iface.ReadReg(chip, F::Reg::EfusesReadData1, true),
        iface.ReadReg(chip, F::Reg::EfusesReadData0, true)
    );
    return rb;
}

template <class F>
void RD53BEfuseProgrammer<F>::writeEfuses(RD53BProdInterface<F>& iface, Chip* chip, uint32_t word) {
    writeRegSure(iface, chip, F::Reg::GlobalPulseConf, 1u << RD53BProd<F>::GlobalPulseRoutes.at("ResetEfuses")); 
    writeRegSure(iface, chip, F::Reg::GlobalPulseWidth, 4 / F::globalPulseUnit);
    iface.template SendCommand<RD53BCmd::GlobalPulse>(chip);
    uint16_t lower, upper;
    std::tie(upper, lower) = bits::unpack<16, 16>(word);
    writeRegSure(iface, chip, F::Reg::EfusesWriteData0, lower);
    writeRegSure(iface, chip, F::Reg::EfusesWriteData1, upper);
    writeRegSure(iface, chip, F::Reg::EfusesConfig, EFUSES_WRITE);
    writeRegSure(iface, chip, F::Reg::GlobalPulseConf, 1u << RD53BProd<F>::GlobalPulseRoutes.at("StartEfusesProgrammer"));
    writeRegSure(iface, chip, F::Reg::GlobalPulseWidth, param("width"_s) / F::globalPulseUnit);
    unsigned long i2cData = readExpander();
    LOG(INFO) << "Before starting programming, the I2C expander was set to " << i2cData << RESET;
    writeExpander(i2cData | 0x80);
    iface.template SendCommand<RD53BCmd::GlobalPulse>(chip);
    writeExpander(i2cData);
    i2cData = readExpander();
    LOG(INFO) << "After programming, the I2C expander is set to " << i2cData << RESET;
}

template <class F>
unsigned long RD53BEfuseProgrammer<F>::readExpander(unsigned i2cAddr) {
    char c;
    std::string reply;
    std::ostringstream i2cR;

    i2cR << std::hex << "IR," << i2cAddr << "\n";
    ::write(fWPAC, i2cR.str().c_str(), i2cR.str().size());
    while(::read(fWPAC, &c, 1) > 0) {
        reply.push_back(c);
    }
    //logging the raw WPAC reply but removing carriage return and linefeed characters
    LOG(INFO) << "Read " << std::regex_replace(reply, std::regex("\\n|\\r|\\r\\n|"), "")
        << " from the WPAC" << RESET;
    if(
        reply.size() != 7 ||
        reply.find("I,0x") != 0 ||
        reply.find_first_not_of("0123456789ABCDEFabcdef", 4) != 6 ||
        reply.find('\n') != 6
    ) {
        throw std::runtime_error("Failed to read the I2C expander configuration");
    }
    return std::stoul(reply.substr(4, 2), nullptr, 16);
}

template <class F>
bool RD53BEfuseProgrammer<F>::writeExpander(unsigned long data, unsigned i2cAddr) {
    char c;
    std::string reply;
    std::ostringstream i2cW;

    i2cW << std::hex << "IW," << i2cAddr << "," << data << "\n";
    ::write(fWPAC, i2cW.str().c_str(), i2cW.str().size());
    while(::read(fWPAC, &c, 1) > 0) {
        reply.push_back(c);
    }
    return reply == "OK\n";
}

template <class F>
ChipDataMap<typename RD53BEfuseProgrammer<F>::EfusesState> RD53BEfuseProgrammer<F>::run() {
    Ph2_System::SystemController& sysCtrller = Base::system();
    RD53BProdInterface<F>& chipIface = *static_cast<RD53BProdInterface<F>*>(sysCtrller.fReadoutChipInterface);
    ChipDataMap<EfusesState> eFuses;

    fWPAC = ::open(param("wpacPort"_s).c_str(), O_RDWR);
    if(fWPAC < 0) {
        throw std::runtime_error("Could not open WPAC port");
    }
    termios conf[2]; ///> { original serial port config, new config }
    ::tcgetattr(fWPAC, conf + 0); // backup current config to restore at the end
    ::tcgetattr(fWPAC, conf + 1);
    ::cfmakeraw(conf + 1);
    conf[1].c_cc[VMIN] = 0; // timeout timer starts immediately
    conf[1].c_cc[VTIME] = 1; // == 0.1 s
    ::tcsetattr(fWPAC, TCSADRAIN, conf + 1);

    unsigned long muxConf = readExpander(0x20);
    writeExpander((muxConf & (~0x7cu)) | (0x16u << 2), 0x20);

    for_each_device<Chip>(sysCtrller, [this, &chipIface, &eFuses](Chip* chip) {
        eFuses[chip].before = readEfuses(chipIface, chip);
        if(this->param("abortIfNonZero"_s) && eFuses[chip].before != 0) {
            eFuses[chip].after = eFuses[chip].before;
            return;
        }
        writeEfuses(chipIface, chip, this->param("word"_s));
        eFuses[chip].after = readEfuses(chipIface, chip);
    });

    writeExpander(muxConf, 0x20);

    ::tcsetattr(fWPAC, TCSADRAIN, conf + 0); // restoring original configuration
    ::close(fWPAC);
    return eFuses;
}

template <class F>
void RD53BEfuseProgrammer<F>::draw(const ChipDataMap<typename RD53BEfuseProgrammer<F>::EfusesState>& eFuses) const {
    for(const auto& item : eFuses) {
        // value to be written to the efuses of this chip
        uint32_t wordToWrite = this->param("word"_s);
        // logging the value to be written
        LOG(INFO) << "Efuses word to be written"
#ifdef CHIP_PATH
                  << " at "
                  << "board=" << item.first.board_id << "/"
                  << "hybrid=" << item.first.hybrid_id << "/"
                  << "chip=" << item.first.chip_id
#endif
                  << ": "
                  << wordToWrite
                  << " ("
                  << std::showbase << std::hex
                  << wordToWrite
                  << std::dec
                  << ")"
                  << RESET;
        // logging the status of the efuses before programming
        LOG(INFO) << "Efuses word "
#ifdef CHIP_PATH
                  << "at "
                  << "board=" << item.first.board_id << "/"
                  << "hybrid=" << item.first.hybrid_id << "/"
                  << "chip=" << item.first.chip_id << " "
#endif
                  << "before programming: "
                  << item.second.before
                  << " ("
                  << std::showbase << std::hex
                  << item.second.before
                  << std::dec
                  << ")"
                  << RESET;
        // logging the status of the efuses after programming
        LOG(INFO) << "Efuses word "
#ifdef CHIP_PATH
                  << "at "
                  << "board=" << item.first.board_id << "/"
                  << "hybrid=" << item.first.hybrid_id << "/"
                  << "chip=" << item.first.chip_id << " "
#endif
                  << "after programming: "
                  << item.second.after
                  << " ("
                  << std::showbase << std::hex
                  << item.second.after
                  << std::dec
                  << ")"
                  << RESET;

        // logging the result of the procedure
        if (item.second.after != wordToWrite) {
            LOG(ERROR) << BOLDRED << "Efuses programming failed!" << RESET;
        } else {
            LOG(INFO) << "Efuses programming completed" << RESET;
        }

    }
}

template class RD53BEfuseProgrammer<RD53BFlavor::ATLAS>;
template class RD53BEfuseProgrammer<RD53BFlavor::CMS>;
template class RD53BEfuseProgrammer<RD53BFlavor::ITkPixV2>;
template class RD53BEfuseProgrammer<RD53BFlavor::CROCv2>;

}
