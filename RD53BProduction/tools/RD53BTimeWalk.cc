#include "RD53BTimeWalk.h"

#include "Utils/xtensor/xadapt.hpp"
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xmasked_view.hpp"
#include "Utils/xtensor/xrandom.hpp"
#include "xtensor/xtensor.hpp"

#include <TGraph.h>
#include <TMultiGraph.h>
#include <TTree.h>

#include <algorithm>

namespace RD53BTools
{
template <class Flavor>
void RD53BTimeWalk<Flavor>::init()
{
    // nVcalSteps = std::ceil((param("vcalRange"_s)[1] - param("vcalRange"_s)[0]) / double(param("vcalStep"_s)));
    nDelaySteps = std::ceil(double(param("maxDelay"_s)) / param("fineDelayStep"_s));
    if(param("logScaleVcal"_s))
        vcalBins = xt::logspace(std::log10(std::max(param("vcalRange"_s)[0], 1ul)), std::log10(param("vcalRange"_s)[1]), param("nVcalSteps"_s));
    else
        vcalBins = xt::linspace(param("vcalRange"_s)[0], param("vcalRange"_s)[1], param("nVcalSteps"_s));
}

template <class Flavor>
auto RD53BTimeWalk<Flavor>::run(Task progress) -> Result
{
    auto& chipInterface = *static_cast<RD53BProdInterface<Flavor>*>(Base::system().fReadoutChipInterface);

    ChipDataMap<ChipResults> results;

    auto& injTool = param("injectionTool"_s);

    const double delayStep = 25. / 32. * param("fineDelayStep"_s);

    if(param("storeHits"_s))
    {
        Base::createOutputDirectory();
        Base::createRootFile();
        Base::for_each_chip([&](Chip* chip) {
            Base::createRootFileDirectory(chip);
            hitTrees.insert({chip, new TTree("hits", "Hits")});
        });
    }

    Base::for_each_chip([&](Chip* chip) {
        results.insert({chip, {xt::zeros<size_t>({injTool.param("triggerDuration"_s) * nDelaySteps, vcalBins.size()}), xt::zeros<double>({vcalBins.size(), injTool.size(0), injTool.size(1)})}});
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
    });

    size_t lastCoarseDelay = 0;
    bool   digitalScan     = injTool.param("injectionType"_s) == "digital";
    injTool.injectionScan(progress,
                          makeScan(ScanRange(vcalBins.size(),
                                             [&, this](auto i) {
                                                if(digitalScan)
                                                {
                                                    injTool.param("pulseDuration"_s) = std::round(vcalBins(i));
                                                    injTool.configureInjections();
                                                }
                                                else
                                                    Base::for_each_hybrid([&](Hybrid* hybrid) {
                                                        chipInterface.WriteReg(hybrid, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + std::round(vcalBins(i)));
                                                    });
                                             }),
                                   ScanRange(nDelaySteps,
                                             [&, this](auto i) {
                                                 size_t totalDelay  = i * param("fineDelayStep"_s);
                                                 size_t fineDelay   = totalDelay % param("maxFineDelay"_s);
                                                 size_t coarseDelay = (totalDelay - fineDelay) / 8;
                                                 Base::for_each_hybrid([&](Hybrid* hybrid) { chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", fineDelay); });
                                                 if(coarseDelay != lastCoarseDelay)
                                                 {
                                                     injTool.param("injectionDelay160MHz"_s) = coarseDelay;
                                                     injTool.configureInjections();
                                                     lastCoarseDelay = coarseDelay;
                                                 }
                                             })),
                          [&, this](auto i, auto&& events) {
                              size_t frameId = i / vcalBins.size();
                              size_t vcalId  = i % vcalBins.size();

                              auto usedPixels = param("injectionTool"_s).generateInjectionMask(frameId);
                              Base::for_each_chip([&](auto chip) {
                                  if(param("storeHits"_s))
                                  {
                                      Base::createRootFileDirectory(chip);

                                      uint16_t charge;
                                      uint16_t delay;
                                      uint16_t row;
                                      uint16_t col;
                                      uint16_t tot;
                                      uint16_t trigger_id;

                                      auto& hitsTree = hitTrees[chip];
                                      hitsTree->Branch("charge", &charge);
                                      hitsTree->Branch("delay", &delay);
                                      hitsTree->Branch("row", &row);
                                      hitsTree->Branch("col", &col);
                                      hitsTree->Branch("tot", &tot);
                                      hitsTree->Branch("trigger_id", &trigger_id);

                                      for(size_t j = 0; j < nDelaySteps; ++j)
                                      {
                                          for(const auto& event: xt::view(events[chip], j, xt::all()))
                                          {
                                              for(const auto& hit: event.hits)
                                              {
                                                  charge     = std::round(vcalBins(vcalId));
                                                  delay      = j * param("fineDelayStep"_s);
                                                  row        = hit.row;
                                                  col        = hit.col;
                                                  tot        = hit.tot;
                                                  trigger_id = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                                                  hitsTree->Fill();
                                              }
                                          }
                                      }
                                  }

                                  xt::xtensor<bool, 2> enabled = xt::view(usedPixels && chip->injectablePixels(), injTool.rowRange(), injTool.colRange());

                                  xt::xtensor<double, 3> hitCount = xt::zeros<double>({injTool.size(0), injTool.size(1), nDelaySteps * injTool.param("triggerDuration"_s)});

                                  for(size_t j = 0; j < nDelaySteps; ++j)
                                  {
                                      for(const auto& event: xt::view(events[chip], j, xt::all()))
                                      {
                                          auto bcid       = event.triggerId % injTool.param("triggerDuration"_s);
                                          auto delayIndex = 32 / param("fineDelayStep"_s) * (bcid + 1) - 1 - j;
                                          results.at(chip).tornado(delayIndex, vcalId) += event.hits.size();
                                          for(const auto& hit: event.hits)
                                          {
                                              int row = (int)hit.row - (int)injTool.offset(0);
                                              int col = (int)hit.col - (int)injTool.offset(1);
                                              if(row < 0 || row >= (int)injTool.size(0) || col < 0 || col >= (int)injTool.size(1) || !enabled(row, col))
                                              {
                                                  // std::cout << "Warning: hit in disabled pixel" << std::endl;
                                                  continue;
                                              }
                                              ++hitCount(row, col, delayIndex);
                                          }
                                      }
                                  }

                                  xt::xtensor<double, 3> occupancy = hitCount / (double)injTool.param("nInjections"_s);

                                  // std::cout << xt::adapt(occupancy.shape()) << std::endl;
                                  // std::cout << xt::mean(occupancy, {0, 1}) << std::endl;
                                  // std::cout << xt::view(results.at(chip).tornado, xt::all(), vcalId) << std::endl;
                                //   std::cout << "frameId: " << frameId << ", vcalId: " << vcalId << std::endl;
                                //   std::cout << xt::from_indices(xt::argwhere(enabled)) << std::endl;
                                //   std::cout << xt::from_indices(xt::argwhere(enabled && xt::equal(xt::sum(hitCount, -1), 0))) << std::endl;

                                  xt::xtensor<int, 2> right = xt::argmax(occupancy > 0.5, 2);
                                  // xt::xtensor<double, 2> rightW(right.shape());
                                  // for (auto i = 0u; i < right.size(); ++i) {
                                  //     auto idx = xt::unravel_index(i, right.shape());
                                  //     rightW[i] = xt::view(occupancy, idx[0], idx[1], xt::all())(right[i]);
                                  // }
                                  xt::xtensor<int, 2> left = xt::maximum(right - 1, 0);
                                  // xt::xtensor<double, 2> leftW(left.shape());
                                  xt::xtensor<double, 2> interpolationWeight = xt::zeros<double>(right.shape());

                                  for (const auto& idx : xt::argwhere(enabled)) {
                                    double occLeft = occupancy(idx[0], idx[1], left(idx[0], idx[1]));
                                    double occRight = occupancy(idx[0], idx[1], right(idx[0], idx[1]));
                                    // if (occLeft == occRight)
                                    //     std::cout << "idx: " << xt::adapt(idx) << std::endl;
                                    // else
                                    if (occLeft != occRight)
                                        interpolationWeight(idx[0], idx[1]) = (0.5 - occLeft) / (occRight - occLeft);
                                  }

                                //   for(auto i = 0u; i < left.size(); ++i)
                                //   {
                                //       if(enabled[i] && left[i] != right[i])
                                //       {
                                //           auto idx = xt::unravel_index(i, left.shape());
                                //           // leftW[i] = xt::view(occupancy, idx[0], idx[1], xt::all())(left[i]);
                                //           double occLeft         = xt::view(occupancy, idx[0], idx[1], xt::all())(left[i]);
                                //         //   double occLeft         = occupancy(idx[0], idx[1], left[i]);
                                //           double occRight        = xt::view(occupancy, idx[0], idx[1], xt::all())(right[i]);
                                //         //   double occRight         = occupancy(idx[0], idx[1], right[i]);
                                //           if (occLeft == occRight)
                                //               std::cout << "i: " << i << ", idx: " << xt::adapt(idx) << std::endl;
                                //           interpolationWeight[i] = (0.5 - occLeft) / (occRight - occLeft);
                                //       }
                                //   }
                                xt::xtensor<double, 2> timewalk = delayStep * (left + interpolationWeight);
                                  xt::masked_view(xt::view(results[chip].timewalk, vcalId, xt::all(), xt::all()), enabled) = timewalk;
                                  // (xt::view(hitCount, xt::all(), xt::all(), xt::keep(right)
                                  // xt::argmax(hitCount > injTool.param("nInjections"_s) / 2.0, 2);
                              });
                          });

    if(param("storeHits"_s)) Base::for_each_chip([&](Chip* chip) { hitTrees[chip]->Write(); });

    return results;
}

template <class Flavor>
void RD53BTimeWalk<Flavor>::draw(const Result& results)
{
    auto usedPixels = param("injectionTool"_s).usedPixels();
    // xt::xtensor<double, 1> delayBins = 25. / 32. * xt::arange(0u, param("maxDelay"_s), param("fineDelayStep"_s));

    Base::createOutputDirectory();
    Base::createRootFile();

    Base::for_each_chip([&](auto* chip) {
        auto dir = Base::createRootFileDirectory(chip);

        auto enabled = xt::view(usedPixels && chip->injectablePixels(), param("injectionTool"_s).rowRange(), param("injectionTool"_s).colRange());

        // {
        //     xt::xtensor<double, 2> thresholdPeakToPeak = xt::amax(results.at(chip).threshold, 0) - xt::amin(results.at(chip).threshold, 0);

        //     Base::drawMap(thresholdPeakToPeak, "Threshold Peak-to-Peak Map", "Threshold Peak-to-Peak", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
        //     auto filteredThresholdPeakToPeak = xt::filter(thresholdPeakToPeak, enabled);
        //     Base::drawHist(filteredThresholdPeakToPeak, "Threshold Peak-to-Peak Distribution", 50, 0, 1.1 * xt::amax(filteredThresholdPeakToPeak)(), "Threshold Peak-to-Peak", false);
        // }

        // std::vector<double> meanTimeWalk(vcalBins.size(), 0);

        xt::xtensor<double, 3> timewalk = xt::transpose(results.at(chip).timewalk, {1, 2, 0});

        // xt::xtensor<size_t, 2> offset = xt::argmax(timewalk > 0, 2);

        // std::cout << "offset = " << xt::filter(offset, enabled) << std::endl;

        for(size_t i = 0; i < timewalk.shape()[0]; ++i)
        {
            for(size_t j = 0; j < timewalk.shape()[1]; ++j)
            {
                auto view = xt::view(timewalk, i, j, xt::all());
                if(enabled(i, j))
                {
                    // std::cout << view << std::endl;
                    // view = xt::roll(view, -offset(i, j));
                    // std::cout << "rolled by " << +offset(i, j) << std::endl;
                    // std::cout << view << std::endl;
                    // // xt::view(view, xt::range(0, offset(i, j))) = xt::view(view, xt::range(vcalBins.size() - offset(i, j), vcalBins.size()));
                    // // xt::view(timewalk, i, j, xt::range(0, vcalBins.size() - offset(i, j))) = xt::view(timewalk, i, j, xt::range(offset(i, j), vcalBins.size()));
                    // xt::view(view, xt::range(vcalBins.size() - offset(i, j), vcalBins.size())) = 0;
                    // std::cout << i << ", " << j << " " << xt::amin(xt::filter(view, view > view.periodic(-1) / 2)) << std::endl;
                    double offset = xt::amin(xt::filter(view, view > view.periodic(-1) / 2))();
                    xt::xarray<double> a = view - offset;
                    view = xt::maximum(a, 0);
                    // view = 0;
                    // xt::view(view, xt::range(0, offset(i, j))) -= xt::amin(xt::filter(view, view > 0));
                }
                else
                {
                    view = 0;
                }
            }
        }

        // xt::filter(timewalk, xt::isclose(timewalk, 0)) = std::numeric_limits<double>::quiet_NaN();

        // timewalk -= xt::expand_dims(xt::nanmin(timewalk, 2), 2);

        // xt::xtensor<double, 3> filtered_timewalk = xt::where(enabled, timewalk, 0);

        xt::xtensor<double, 1> meanTimeWalk = xt::sum(timewalk, {0, 1}) / xt::count_nonzero(enabled);

        // auto mean_timewalk_view = xt::mean(xt::filter(timewalk, enabled), {0, 1});

        // xt::xtensor<double, 1> meanTimeWalk = xt::where(enabled, mean_timewalk_view, 0);

        std::cout << meanTimeWalk << std::endl;

        // xt::xtensor<double, 3> timewalkNormalized = results.at(chip).timewalk;
        // xt::filter(timewalkNormalized, timewalkNormalized > 0) = xt::amax(timewalkNormalized);

        // xt::xtensor<double, 3> timewalkNormalized =
        //     timewalk - xt::amin(xt::where(timewalk > 0, timewalk, )

        // xt::xtensor<double, 3> timewalkNormalized = xt::where(
        //     results.at(chip).timewalk > 0,
        //     results.at(chip).timewalk - xt::amin(results.at(chip).timewalk, 0),
        //     0
        // );

        // xt::xtensor<double, 1> meanTimeWalk = xt::zeros<double>({vcalBins.size()});
        // for (size_t i = 0; i < vcalBins.size(); ++i) {
        //     auto timewalk = xt::view(timewalkNormalized, i, xt::all(), xt::all());
        //     if (xt::any(enabled && timewalk > 0))
        //         meanTimeWalk(i) = xt::mean(xt::filter(timewalk, enabled && timewalk > 0))();
        // }

        {
            auto c = new TCanvas("mean_timewalk");
            auto g = new TGraph(vcalBins.size(), vcalBins.data(), meanTimeWalk.data());
            g->SetTitle("Average TimeWalk");
            g->Draw();
            // g->Write();
            c->Write();
        }

        {
            xt::xtensor<double, 2> tornado                     = results.at(chip).tornado / (double)param("injectionTool"_s).param("nInjections"_s) / xt::count_nonzero(enabled);
            TCanvas*               c                           = new TCanvas("Tornado plot", "Tornado plot", 600, 600);
            xt::xtensor<double, 1> bin_edges                   = xt::zeros<double>({vcalBins.size() + 1});
            xt::view(bin_edges, xt::range(1, vcalBins.size())) = xt::view(vcalBins, xt::range(0, vcalBins.size() - 1)) + xt::diff(vcalBins) / 2.0;
            bin_edges(0)                                       = vcalBins(0);
            bin_edges.periodic(-1)                             = vcalBins.periodic(-1);
            // std::cout << bin_edges << std::endl;
            TH2F* h = new TH2F("Tornado plot", "Tornado plot", tornado.shape()[0], 0, param("injectionTool"_s).param("triggerDuration"_s) * 25, vcalBins.size(), bin_edges.data());
            h->SetXTitle("Detection Delay (triggerId * 32 - fineDelay) * 25 / 32[ns]");
            h->SetYTitle("Delta VCAL");
            h->SetZTitle("Occupancy");
            for(size_t i = 0; i < tornado.shape()[0]; ++i)
                for(size_t j = 0; j < tornado.shape()[1]; ++j) h->SetBinContent(i + 1, j + 1, tornado(i, j));
            h->Draw("COLZ");
            c->Write();
        }

        // Base::drawHist2D(
        //     results.at(chip).tornado / (double)param("injectionTool"_s).param("nInjections"_s) / xt::count_nonzero(enabled),
        //     "Tornado plot",
        //     0,
        //     param("injectionTool"_s).param("triggerDuration"_s) * 25,
        //     param("vcalRange"_s)[0],
        //     param("vcalRange"_s)[1],
        //     "Detection Delay (triggerId * 32 - fineDelay) [ns]",
        //     "Delta VCAL",
        //     "Occupancy"
        // );

        pixel_matrix_t<Flavor, double> maxTimewalk;
        maxTimewalk.fill(0);
        xt::view(maxTimewalk, param("injectionTool"_s).rowRange(), param("injectionTool"_s).colRange()) = xt::amax(timewalk, 2);

        Base::drawMap(maxTimewalk, "Max Timewalk Map", "Max Timewalk");

        // xt::xtensor<double, 3> timewalk2 = xt::transpose(timewalk, {1, 2, 0});

        auto rowRange = param("injectionTool"_s).rowRange();
        auto colRange = param("injectionTool"_s).colRange();

        auto pixelPositions = xt::argwhere(xt::view(usedPixels && chip->injectablePixels(), rowRange, colRange));

        auto mg = new TMultiGraph();

        dir->mkdir("SinglePixels")->cd();

        xt::xtensor<size_t, 1> indices;
        if(param("pickRandomPixels"_s))
            indices = xt::sort(xt::view(xt::random::permutation(pixelPositions.size()), xt::range(0, param("maxNPlots"_s))));
        else
            indices = xt::arange(std::min(pixelPositions.size(), param("maxNPlots"_s)));

        for(auto idx: indices)
        {
            auto pixelPos = pixelPositions[idx];

            std::string name = "timewalk (" + std::to_string(param("injectionTool"_s).offset(0) + pixelPos[0]) + ", " + std::to_string(param("injectionTool"_s).offset(1) + pixelPos[1]) + ')';

            std::cout << name << std::endl;

            // std::cout << xt::view(timewalk, pixelPos[0], pixelPos[1], xt::all()) << std::endl;
            xt::xtensor<double, 1> timewalkLocal = xt::view(timewalk, pixelPos[0], pixelPos[1], xt::all());
            // std::cout << timewalkLocal << std::endl;
            auto g = new TGraph(vcalBins.size(), vcalBins.data(), timewalkLocal.data());
            g->SetName(name.c_str());
            g->SetTitle("TimeWalk");
            g->Draw();
            g->Write();
            mg->Add(g);
        }

        dir->cd();
        auto c = new TCanvas("TimeWalk single pixels");
        mg->SetName("Timewalk single pixels");
        mg->SetTitle("TimeWalk");
        mg->Draw();

        c->Write();
    });
}

template class RD53BTimeWalk<RD53BFlavor::ATLAS>;
template class RD53BTimeWalk<RD53BFlavor::CMS>;
template class RD53BTimeWalk<RD53BFlavor::ITkPixV2>;
template class RD53BTimeWalk<RD53BFlavor::CROCv2>;

} // namespace RD53BTools