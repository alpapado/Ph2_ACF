#ifndef RD53BMonMuxTest_H
#define RD53BMonMuxTest_H

#include <map>
#include <string>
#include <vector>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BMonMuxTest;

template <class Flavor>
const auto ToolParameters<RD53BMonMuxTest<Flavor>> = make_named_tuple(
   std::make_pair("voltmeterId"_s, std::string("dacCalib")),
   std::make_pair("voltmeterChannelId"_s, std::string("dacCalib")),
   std::make_pair("currentMuxResistance"_s, -1.0f),
   std::make_pair("GNDA_REF1"_s, 0.0f),
   std::make_pair("GNDA_REF2"_s, 0.0f),
   std::make_pair("GNDD_REF"_s, 0.0),
   std::make_pair("iList"_s, std::vector<uint8_t>{}),
   std::make_pair("vList"_s, std::vector<uint8_t>{})
);

template <class Flavor>
struct RD53BMonMuxTest : public RD53BTool<RD53BMonMuxTest, Flavor> {

    using Base = RD53BTool<RD53BMonMuxTest, Flavor>;
    using Base::Base;
    using Base::param;

    static std::set<uint8_t> vRefAna1, vRefAna2, vRefDig;

    struct Results {
        std::map<std::string, float> v, i;
    };

    ChipDataMap<Results> run() const;

    void draw(const ChipDataMap<Results>& results) const;

};

}

#endif
