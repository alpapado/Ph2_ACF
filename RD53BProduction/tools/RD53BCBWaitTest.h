#ifndef RD53BCBWAITTEST_H
#define RD53BCBWAITTEST_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BCBWaitTest; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BCBWaitTest<Flavor>> = make_named_tuple(
    std::make_pair("testValues"_s, std::vector<uint16_t> {}),
    std::make_pair("nPoints"_s, 1),
    std::make_pair("nRepetitions"_s, 1000),
    std::make_pair("configDelayUs"_s, 100),
    std::make_pair("sleepTimeUs"_s, 0)
);

template <class Flavor>
struct RD53BCBWaitTest : public RD53BTool<RD53BCBWaitTest, Flavor> {
    using Base = RD53BTool<RD53BCBWaitTest, Flavor>;
    using Base::Base;

   bool run(Task progress) const;
    //void draw(const std::stringstream& results) const;
};

}

#endif
