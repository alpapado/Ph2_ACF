#include "RD53BCBWaitTest.h"

#include <string>

namespace RD53BTools {

template <class Flavor>
bool RD53BCBWaitTest<Flavor>::run(Task progress) const {
    //setup
    Ph2_System::SystemController& system = Base::system();
    auto& chipInterface = Base::chipInterface();
    ChipDataMap<uint16_t> cbWaitRegValues;

    std::vector<uint16_t> testValues = Base::param("testValues"_s);
    uint32_t nPoints = Base::param("nPoints"_s);
    uint16_t nRepetitions = Base::param("nRepetitions"_s);
    uint32_t configDelayUs = Base::param("configDelayUs"_s);
    uint16_t sleepTimeUs = Base::param("sleepTimeUs"_s);

    //counter for the number of errors obtained by sampling `channel_up`
    uint16_t iErrors = 0;

    //counter for the number of failed iterations for a CBWait value
    uint16_t nIterErrors = 0;

    //variable for holding the Aurora status
    uint8_t channelStatus = 0;

    LOG(INFO) << BOLDGREEN << "Starting CBWait stability test" << RESET;

    //looping on all devices to perform the test
    for_each_device<Chip>(system, [&](DeviceChain devices) {
        auto& fwInterface = Base::getFWInterface(devices.board);
        auto chip = static_cast<RD53BProd<Flavor>*>(devices.chip);

        //saving the previous chip configuration for AuroraConfig for each chip
        cbWaitRegValues.insert({chip, chipInterface.ReadReg(chip, "CBWait")});

        //looping on the CBWait values to be tested
        for (auto cbwait : testValues){
            LOG(INFO) << BOLDGREEN << "Testing CBWait = "
                << std::to_string(cbwait) << RESET;

            //resetting the variable for holding the Aurora channel status
            channelStatus = 0;

            //resetting the counter for the number of iteration errors
            nIterErrors = 0;

            //setting the CBWait configuration
            chipInterface.WriteReg(chip, "CBWait", cbwait);

            //looping on the number of repeated tests for a CBWait code
            for (uint16_t i = 0; i < nRepetitions; i++) {

                //resetting the Aurora
                chipInterface.SendGlobalPulse(chip, {"ResetAurora"}, 10);
                std::this_thread::sleep_for(std::chrono::microseconds(configDelayUs));

                //performing several tries to measure the Aurora channel status
                iErrors = 0;
                for (uint32_t j = 0; j < nPoints; j++) {
                    //reading the channel status
                    channelStatus = fwInterface.ReadReg(
                        "user.stat_regs.aurora_rx_channel_up");
                    if (channelStatus != 1) {
                        iErrors++;
                    }
                    //sleep and retry
                    std::this_thread::sleep_for(std::chrono::microseconds(sleepTimeUs));
                } //end of loop on channel_up polls

                //checking the number of errors with repeated polling
                if (iErrors != 0) {
                    nIterErrors += 1;
                }

            } //end of loop on repetitions for each CBWait setting

            //logging the number of failed attempts
            if (nIterErrors == 0) {
                LOG(INFO) << BOLDGREEN << "No failed iterations for CBWait = "
                    << std::to_string(cbwait) << RESET;
            } else {
                LOG(INFO) << BOLDRED << std::to_string(nIterErrors)
                    << " failed iteration(s) for CBWait = "
                    << std::to_string(cbwait) << RESET;
            }

        } //end of loop on CBWait settings

        //restoring the CBWait configuration before starting the test
        chipInterface.WriteReg(chip, "CBWait", cbWaitRegValues[chip]);

    }); //end of loop on chips

    LOG(INFO) << BOLDGREEN << "Completed CBWait stability test" << RESET;

    return true;
}

template class RD53BCBWaitTest<RD53BFlavor::ATLAS>;
template class RD53BCBWaitTest<RD53BFlavor::CMS>;
template class RD53BCBWaitTest<RD53BFlavor::ITkPixV2>;
template class RD53BCBWaitTest<RD53BFlavor::CROCv2>;

}
