#ifndef RD53BTemp_H
#define RD53BTemp_H

#include <array>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BTemp;

template <class Flavor>
const auto ToolParameters<RD53BTemp<Flavor>> = make_named_tuple(
    std::make_pair("enSens"_s, std::vector<std::string>{}),
    std::make_pair("voltmeterId"_s, std::string("analogCalib")),
    std::make_pair("voltmeterChannelId"_s, std::string("analogCalib"))
);

template <class Flavor>
struct RD53BTemp : public RD53BTool<RD53BTemp, Flavor> {
    using Base = RD53BTool<RD53BTemp, Flavor>;
    using Base::Base;
    using Base::param;

    struct TransistorConfig {
        std::string mux;
        std::reference_wrapper<const RD53BProdConstants::Register> reg;
        int offset;
        std::string muxGND;
    };

    struct Result {
        float temperature;
        float vLowBias, vHighBias;
    };

    static const std::vector<TransistorConfig> transistorSeq;
    static const std::vector<std::array<std::string, 2>> resistorSeq;

    void init();

    ChipDataMap<std::unordered_map<std::string, Result>> run();

    void draw(const ChipDataMap<std::unordered_map<std::string, Result>>& meas);

  private:
    std::unordered_map<std::string, float> coeff;
    std::unordered_set<std::string> enSens;
};

}

#endif
