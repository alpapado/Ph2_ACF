#ifndef RD53BREGREADER_H
#define RD53BREGREADER_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53BRegReader : public RD53BTool<RD53BRegReader, Flavor>
{
    using Base = RD53BTool<RD53BRegReader, Flavor>;
    using Base::Base;

    bool run(Task progress) const;
};

} // namespace RD53BTools

#endif