#ifndef RD53RingOscillator_H
#define RD53RingOscillator_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53RingOscillator : public RD53BTool<RD53RingOscillator, Flavor>
{
    using Base = RD53BTool<RD53RingOscillator, Flavor>;
    using Base::Base;

    struct Results
    {
        double trimOscCounts[42][16];
        double trimOscFrequency[42][16];
        double trimVoltage[16];
    };

    Results run() const;

    void draw(const Results& results) const;
};

} // namespace RD53BTools

#endif
