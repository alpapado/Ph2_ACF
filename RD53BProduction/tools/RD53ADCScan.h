#ifndef RD53ADCScan_H
#define RD53ADCScan_H

#include "RD53BTool.h"
#include <utility>

namespace RD53BTools
{
template <class>
struct RD53ADCScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53ADCScan<Flavor>> = make_named_tuple(
    std::make_pair("nVariables"_s, 1),
    std::make_pair("serialPort"_s, std::string("/dev/ttyUSB0")),
    std::make_pair("serialPortInputTerminator"_s, std::string("\r")),
    std::make_pair("serialPortOutputTerminator"_s, std::string("\r"))
);

template <class Flavor>
struct RD53ADCScan : public RD53BTool<RD53ADCScan, Flavor>
{
    using Base = RD53BTool<RD53ADCScan, Flavor>;
    using Base::Base;
    using Base::param;

    static constexpr const char* writeVar[] = {"VCAL_HIGH", "VCAL_MED", "REF_KRUM_LIN", "Vthreshold_LIN", "VTH_SYNC", "VBL_SYNC", "VREF_KRUM_SYNC", "VTH_HI_DIFF", "VTH_LO_DIFF"};

    struct Results
    {
        double                           fitStart[9];
        double                           fitEnd[9];
        std::vector<std::vector<double>> VMUXvolt;
        std::vector<std::vector<double>> ADCcode;
    };

    Results run() const;

    void draw(const Results& results);
};

} // namespace RD53BTools

#endif
