#ifndef RD53MuxScan_H
#define RD53MuxScan_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53MuxScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53MuxScan<Flavor>> = make_named_tuple();

template <class Flavor>
struct RD53MuxScan : public RD53BTool<RD53MuxScan, Flavor>
{
    using Base = RD53BTool<RD53MuxScan, Flavor>;
    using Base::Base;

    struct Results
    {
        std::map<std::string, std::pair<double, double>> VMuxVolt;
        std::map<std::string, std::pair<double, double>> IMuxVolt;
    };

    ChipDataMap<Results> run() const;

    void draw(const ChipDataMap<Results>& results) const;
};

} // namespace RD53BTools

#endif
