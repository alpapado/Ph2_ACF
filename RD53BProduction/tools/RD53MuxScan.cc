#include "RD53MuxScan.h"

#include "ProductionToolsIT/ITchipTestingInterface.h"

#include "DQMUtils/RD53MuxScanHistograms.h"

#include <iostream>

namespace RD53BTools
{
template <class Flavor>
ChipDataMap<typename RD53MuxScan<Flavor>::Results> RD53MuxScan<Flavor>::run() const
{
    ChipDataMap<Results> results;
    auto&                chipInterface = Base::chipInterface();

    // std::unique_ptr<Ph2_ITchipTesting::ITpowerSupplyChannelInterface> dKeithley2410;

    ChipDataMap<Ph2_ITchipTesting::ITpowerSupplyChannelInterface> keithleys;

    // if (Base::param("useKeithley"_s)) {

    Base::for_each_chip([&](auto chip) {
        if(chip->comment().empty()) return;

        auto& dKeithley2410 =
            keithleys.emplace(std::piecewise_construct, std::forward_as_tuple(chip), std::forward_as_tuple(Base::system().fPowerSupplyClient, chip->comment(), "Front")).first->second;

        std::cout << "setupKeithley2410ChannelSense for chip " << ChipLocation{chip} << "..." << std::endl;

        dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

        std::cout << "setupKeithley2410ChannelSense done!" << std::endl;
    });

    // dKeithley2410 = std::make_unique<Ph2_ITchipTesting::ITpowerSupplyChannelInterface>(Base::system().fPowerSupplyClient, Base::param("keithleyID"_s), "Front");

    // std::cout << "setupKeithley2410ChannelSense..." << std::endl;

    // dKeithley2410->setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    // std::cout << "setupKeithley2410ChannelSense done!" << std::endl;

    // }

    Base::for_each_chip([&](auto chip) {
        chipInterface.WriteReg(chip, "IMonitor", Flavor::IMuxMap.at("HIGH_Z"));
        auto  it          = keithleys.find(chip);
        bool  hasKeithley = it != keithleys.end();
        auto* keithley    = hasKeithley ? &it->second : nullptr;
        for(const auto& VMuxVar: Flavor::VMuxMap)
        {
            if(VMuxVar.first == "HIGH_Z" || VMuxVar.first == "IMUX_OUT") continue;

            chipInterface.WriteReg(chip, "MonitorEnable", 1); // Choose MUX entry
            chipInterface.WriteReg(chip, "VMonitor", VMuxVar.second);
            // usleep(1000000);
            double external = 0;
            if(hasKeithley) external = keithley->getVoltage();
            chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); // ADC start conversion
            usleep(100000);
            double adc = chipInterface.ReadReg(chip, "MonitoringDataADC");
            results[chip].VMuxVolt.insert({VMuxVar.first, {external, adc}});
            LOG(INFO) << BOLDBLUE << "VMUX: " << BOLDYELLOW << VMuxVar.first << " (" << +VMuxVar.second << ")" << RESET << " external: " << external << " adc: " << adc;
        }

        chipInterface.WriteReg(chip, "VMonitor", Flavor::VMuxMap.at("IMUX_OUT"));
        for(const auto& IMuxVar: Flavor::IMuxMap)
        {
            if(IMuxVar.first == "HIGH_Z") continue;
            chipInterface.WriteReg(chip, "MonitorEnable", 1); // Choose MUX entry
            chipInterface.WriteReg(chip, "IMonitor", IMuxVar.second);
            // usleep(1000000);
            double external = 0;
            if(hasKeithley) external = keithley->getVoltage();
            chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); // ADC start conversion
            usleep(100000);
            double adc = chipInterface.ReadReg(chip, "MonitoringDataADC");
            results[chip].IMuxVolt.insert({IMuxVar.first, {external, adc}});
            LOG(INFO) << BOLDBLUE << "IMUX: " << BOLDYELLOW << IMuxVar.first << " (" << +IMuxVar.second << ")" << RESET << " external: " << external << " adc: " << adc;
        }
    });

    return results;
}

template <class Flavor>
void RD53MuxScan<Flavor>::draw(const ChipDataMap<Results>& results) const
{
    std::ofstream outFile(Base::getOutputFilePath("muxScan.csv"));

    outFile << "chip, type, var, external, adc\n";

    Base::for_each_chip([&](auto* chip) {
        const auto& chipResults = results.at(chip);

        for(const auto& item: chipResults.VMuxVolt) outFile << ChipLocation{chip} << ", VMux, " << item.first << ", " << item.second.first << ", " << item.second.second << '\n';

        for(const auto& item: chipResults.IMuxVolt) outFile << ChipLocation{chip} << ", IMux, " << item.first << ", " << item.second.first << ", " << item.second.second << '\n';
    });
}

template class RD53MuxScan<RD53BFlavor::ATLAS>;
template class RD53MuxScan<RD53BFlavor::CMS>;
template class RD53MuxScan<RD53BFlavor::ITkPixV2>;
template class RD53MuxScan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
