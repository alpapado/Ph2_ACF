#include "RD53BTool.h"

namespace RD53BTools
{
size_t RD53BToolBase::nPlots = 0;

void Task::update(double progress)
{
    struct winsize window_size;
    int            width;
    int            status = ioctl(STDOUT_FILENO, TIOCGWINSZ, &window_size);
    if(status < 0)
        width = 100;
    else
        width = window_size.ws_col;
    _bar.set_progress(100 * (_progressRange[0] + progress * size()), false);
    _bar.set_option(indicators::option::BarWidth{std::max(int(width) - int(3 + _bar.get_postfix_text().second + _bar.get_prefix_text().second), 1)});
    _bar.print_progress();
}

} // namespace RD53BTools