#include "RD53BNoiseFloor.h"

#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xmasked_view.hpp"
#include "Utils/xtensor/xrandom.hpp"
#include "Utils/xtensor/xview.hpp"

#include <TCanvas.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>

namespace RD53BTools
{
template <class Flavor>
void RD53BNoiseFloor<Flavor>::init()
{
    GDACBins                                        = xt::arange(param("GDACRange"_s)[0], param("GDACRange"_s)[1], param("GDACStep"_s));
    param("noiseScan"_s).param("maskNoisyPixels"_s) = false;
}

template <class Flavor>
ChipDataMap<typename RD53BNoiseFloor<Flavor>::ChipResults> RD53BNoiseFloor<Flavor>::run(Task progress)
{
    ChipDataMap<ChipResults> results;

    const size_t shape[] = {GDACBins.size(), param("noiseScan"_s).param("injectionTool"_s).size(0), param("noiseScan"_s).param("injectionTool"_s).size(1)};

    Base::for_each_chip([&](auto chip) {
        results.insert({chip, {xt::zeros<size_t>(shape), xt::zeros<double>(shape)}});
        // occupancy[chip] = xt::zeros<size_t>({GDACBins.size(), param("noiseScan"_s).param("injectionTool"_s).size(0), param("noiseScan"_s).param("injectionTool"_s).size(1)});
    });

    auto& chipInterface = Base::chipInterface();

    for(auto i = 0u; i < GDACBins.size(); ++i)
    {
        auto GDAC = GDACBins(i);

        Base::for_each_hybrid([&](auto hybrid) {
            chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_L_LIN, GDAC);
            chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_R_LIN, GDAC);
            chipInterface.WriteReg(hybrid, Flavor::Reg::DAC_GDAC_M_LIN, GDAC);
        });

        auto noiseScanResult = param("noiseScan"_s).run(progress.subTask(i, i + 1, GDACBins.size()));

        // auto hitCount = param("noiseScan"_s).hitCount(noiseScanResult);

        Base::for_each_chip([&](auto chip) {
            xt::view(results[chip].hitCount, i, xt::all(), xt::all()) = xt::view(noiseScanResult[chip].hitCount,
                                                                                 param("noiseScan"_s).param("injectionTool"_s).rowRange(),
                                                                                 param("noiseScan"_s).param("injectionTool"_s).colRange()); // / param("noiseScan"_s).param("nTriggers"_s);
            const auto& tot                                           = noiseScanResult.at(chip).ToTHistogram;
            const auto  totSum                                        = xt::sum(tot, 2);
            // xt::view(results[chip].meanToT, i, xt::all(), xt::all()) =
            auto meanToT = xt::sum<double>(xt::broadcast(xt::arange<double>(16), tot.shape()) * tot, 2) / xt::where(xt::equal(totSum, 0ul), 1, totSum);
            std::cout << xt::adapt(meanToT.shape());
            xt::view(results[chip].meanToT, i, xt::all(), xt::all()) =
                xt::view(meanToT, param("noiseScan"_s).param("injectionTool"_s).rowRange(), param("noiseScan"_s).param("injectionTool"_s).colRange());
            // if (!xt::all(xt::equal(tot, 0u)))
            //     xt::view(results[chip].meanToT, i, xt::all(), xt::all()) = xt::average(xt::broadcast(xt::arange<double>(16), tot.shape()), tot, 2);
        });
    }

    return results;
}

template <class Flavor>
void RD53BNoiseFloor<Flavor>::draw(const ChipDataMap<ChipResults>& results)
{
    Base::createRootFile();

    auto usedPixels = param("noiseScan"_s).param("injectionTool"_s).usedPixels();

    double nTriggers = param("noiseScan"_s).param("nTriggers"_s); // * param("noiseScan"_s).param("injectionTool"_s).param("triggerDuration"_s);

    Base::for_each_chip([&](auto chip) {
        auto dir = Base::createRootFileDirectory(chip);

        auto& hitCount = results.at(chip).hitCount;

        xt::xtensor<bool, 2> enabled =
            xt::view(usedPixels && chip->pixelConfig().enable, param("noiseScan"_s).param("injectionTool"_s).rowRange(), param("noiseScan"_s).param("injectionTool"_s).colRange());

        xt::xtensor<double, 1> meanOcc = xt::average<double>(hitCount, xt::broadcast(enabled, hitCount.shape()), {1, 2});

        Base::drawHistRaw(meanOcc, "Mean Hit Count", param("GDACRange"_s)[0], param("GDACRange"_s)[1], "GDAC", "Hit Count");

        // xt::xtensor<double, 1> meanToT = xt::average(xt::np.arange(16), results.at(chip).meanToT, {1, 2});

        Base::drawHistRaw(xt::mean(results.at(chip).meanToT, {1, 2}), "Mean ToT vs. GDAC", param("GDACRange"_s)[0], param("GDACRange"_s)[1], "GDAC", "Mean ToT");

        auto pixelPositions = xt::argwhere(enabled);

        dir->mkdir("Pixels")->cd();
        for(auto idx: xt::view(xt::random::permutation(pixelPositions.size()), xt::range(0, 100)))
        {
            auto        pixelPos  = pixelPositions[idx];
            std::string pixelName = "(" + std::to_string(param("noiseScan"_s).param("injectionTool"_s).offset(0) + pixelPos[0]) + ", " +
                                    std::to_string(param("noiseScan"_s).param("injectionTool"_s).offset(1) + pixelPos[1]) + ")";
            xt::xtensor<size_t, 1> hitCount_local = xt::view(hitCount, xt::all(), pixelPos[0], pixelPos[1]);
            xt::xtensor<double, 1> tot_local      = xt::view(results.at(chip).meanToT, xt::all(), pixelPos[0], pixelPos[1]);
            Base::drawHistRaw(hitCount_local, "Hit Count " + pixelName, param("GDACRange"_s)[0], param("GDACRange"_s)[1], "GDAC", "Hit Count");

            Base::drawHistRaw(tot_local, "Mean ToT" + pixelName, param("GDACRange"_s)[0], param("GDACRange"_s)[1], "GDAC", "Mean ToT");

            xt::xtensor<double, 1> occ_local = hitCount_local / nTriggers;

            Base::drawHistRaw(occ_local, "Occupancy " + pixelName, param("GDACRange"_s)[0], param("GDACRange"_s)[1], "GDAC", "Occupancy");

            auto g = new TGraph(occ_local.size(), occ_local.data(), tot_local.data());
            g->SetName(("Mean ToT vs. Occ " + pixelName).c_str());
            g->SetTitle(("Mean ToT vs. Occ " + pixelName).c_str());
            g->Draw("AC*");
            g->Write();
            // g->Write();
            // mg->Add(g);
        }
    });
}

template class RD53BNoiseFloor<RD53BFlavor::ATLAS>;
template class RD53BNoiseFloor<RD53BFlavor::CMS>;
template class RD53BNoiseFloor<RD53BFlavor::ITkPixV2>;
template class RD53BNoiseFloor<RD53BFlavor::CROCv2>;

} // namespace RD53BTools