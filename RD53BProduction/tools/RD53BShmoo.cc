#include "RD53BShmoo.h"

#include "Utils/xtensor/xindex_view.hpp"

namespace RD53BTools
{
template <class Flavor>
std::vector<tool_result_t<RD53BInjectionTool<Flavor>>> RD53BShmoo<Flavor>::run(Task progress) const
{
    TCPClient& psIface = *Base::system().fPowerSupplyClient;

    // first we switch everything off
    std::string psId    = param("powerSupplyId"_s);
    auto        psChIds = param("powerSupplyChannelIds"_s);

    for(auto* board: *Base::system().fDetectorContainer)
    {
        auto& fwInterface = Base::getFWInterface(board);
        fwInterface.WriteArbitraryRegister("user.ctrl_regs.gtx_drp.aurora_speed", 1, board, Base::system().fReadoutChipInterface);
        fwInterface.WriteArbitraryRegister("user.ctrl_regs.gtx_drp.set_aurora_speed", 1, board, Base::system().fReadoutChipInterface);
        fwInterface.WriteArbitraryRegister("user.ctrl_regs.gtx_drp.set_aurora_speed", 0, board, Base::system().fReadoutChipInterface);
        fwInterface.WriteArbitraryRegister("user.ctrl_regs.ctrl_cdr.cdr_bypass_en", 1, board, Base::system().fReadoutChipInterface);
    }

    const int   vddRange = param("vddValues"_s);
    const float vddMin   = param("vddMin"_s);
    const float vddMax   = param("vddMax"_s);

    float vddStep = (vddMax - vddMin) / float(vddRange - 1);

    // First switch on all channels to a default value
    for(auto& chan: psChIds)
    {
        psIface.sendAndReceivePacket("TurnOff,PowerSupplyId:" + psId + ",ChannelId:" + chan);
        psIface.sendAndReceivePacket("SetVoltage,PowerSupplyId:" + psId + ",ChannelId:" + chan + ",Value:" + std::to_string(param("defaultVDD"_s)));
        psIface.sendAndReceivePacket("TurnOn,PowerSupplyId:" + psId + ",ChannelId:" + chan);
    }

    std::vector<tool_result_t<RD53BInjectionTool<Flavor>>> injResults(clockRange * vddRange);

    const size_t size = vddRange * clockRange;

    for(auto* board: *Base::system().fDetectorContainer)
    {
        auto& fwInterface = Base::getFWInterface(board);
        for(int vdd_idx = 0; vdd_idx < vddRange; ++vdd_idx)
        {
            // set all voltages that we want to scan
            for(auto& chan: param("channels_to_scan"_s))
            {
                psIface.sendAndReceivePacket("TurnOff,PowerSupplyId:" + psId + ",ChannelId:" + chan);
                psIface.sendAndReceivePacket("SetVoltage,PowerSupplyId:" + psId + ",ChannelId:" + chan + ",Value:" + std::to_string(vddMin + vdd_idx * vddStep));
                psIface.sendAndReceivePacket("TurnOn,PowerSupplyId:" + psId + ",ChannelId:" + chan);
            }

            for(int clock_idx = 0; clock_idx < clockRange; ++clock_idx)
            {
                fwInterface.WriteArbitraryRegister("user.ctrl_regs.ctrl_cdr.cdr_freq_sel", clock_idx, board, Base::system().fReadoutChipInterface, true);

                // try to set it twice
                const int nTrials = 2;
                for(int i = 0; i < nTrials; ++i) { fwInterface.WriteArbitraryRegister("user.ctrl_regs.ctrl_cdr.cdr_freq_sel", clock_idx, board, Base::system().fReadoutChipInterface, true); }

                try
                {
                    Base::system().ConfigureHw();
                }
                catch(std::runtime_error& e)
                {
                    std::cout << "Could not configure the chip. Exception thrown: " << e.what() << std::endl;
                    continue;
                }

                // now lets run a pixel alive
                const size_t flat_index = clock_idx + vdd_idx * clockRange;
                try
                {
                    injResults[flat_index] = param("injectionTool"_s).run(progress.subTask(flat_index, flat_index + 1, size));
                }
                catch(std::exception& e)
                {
                    std::cout << "We have an exception" << std::endl;
                    std::cout << "Exception when running injectionTool " << e.what();
                    // break;
                }

                // now lets run a pixel alive
                // param("injectionTool"_s).configureInjections();
                // size_t nFrames = param("injectionTool"_s).nFrames();
                // for(size_t frame = 0; frame < nFrames; ++frame)
                // {
                //     param("injectionTool"_s).setupMaskFrame(frame);
                //     try
                //     {
                //         param("injectionTool"_s).inject(injResults[clock_idx + vdd_idx * clockRange]);
                //     }
                //     catch(std::exception& e)
                //     {
                //         std::cout << "We have an exception" << std::endl;
                //         std::cout << "Exception when running the injection " << e.what();
                //         break;
                //     }
                //     progress.update(double(frame + nFrames * (clock_idx + vdd_idx * clockRange)) / double(nFrames + nFrames * (clockRange + vddRange * clockRange)));
                // }

                Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
                    auto usedPixels = param("injectionTool"_s).usedPixels();
                    auto enabled    = usedPixels && chip->injectablePixels();
                    auto occ        = param("injectionTool"_s).occupancy(injResults[clock_idx + vdd_idx * clockRange])[chip];
                });
            }
        }
    }
    return injResults;
}

template <class Flavor>
void RD53BShmoo<Flavor>::draw(std::vector<tool_result_t<RD53BInjectionTool<Flavor>>> result) const
{
    // Base::createRootFile();

    std::cout << "We are going to draw" << std::endl;
    auto usedPixels = param("injectionTool"_s).usedPixels();

    Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
        xt::xtensor<float, 2> plot    = xt::zeros<float>({clockRange, param("vddValues"_s)});
        auto                  enabled = usedPixels && chip->injectablePixels();
        for(auto i = 0u; i < result.size(); ++i)
        {
            int  clock_idx           = i % clockRange;
            int  vdd_idx             = (i - clock_idx) / clockRange;
            auto occ                 = param("injectionTool"_s).occupancy(result[i])[chip];
            plot(clock_idx, vdd_idx) = xt::mean(xt::filter(occ, enabled))();
        }
        Base::drawHist2D(plot, "Shmoo plot", 0, clockRange, param("vddMin"_s), param("vddMax"_s), "Clock frequency", "VDD", "Occupancy");
    });
    return;
}

template class RD53BShmoo<RD53BFlavor::ATLAS>;
template class RD53BShmoo<RD53BFlavor::CMS>;
template class RD53BShmoo<RD53BFlavor::ITkPixV2>;
template class RD53BShmoo<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
