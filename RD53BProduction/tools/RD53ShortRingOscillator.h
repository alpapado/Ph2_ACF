#ifndef RD53ShortRingOscillator_H
#define RD53ShortRingOscillator_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53ShortRingOscillator : public RD53BTool<RD53ShortRingOscillator, Flavor>
{
    using Base = RD53BTool<RD53ShortRingOscillator, Flavor>;
    using Base::Base;

    using ChipResult = std::array<double, 42>;

    ChipDataMap<ChipResult> run() const;

    void draw(const ChipDataMap<ChipResult>& results);
};

} // namespace RD53BTools

#endif
