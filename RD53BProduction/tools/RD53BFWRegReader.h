#ifndef RD53BFWRegReader_H
#define RD53BFWRegReader_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53BFWRegReader : public RD53BTool<RD53BFWRegReader, Flavor>
{
    using Base = RD53BTool<RD53BFWRegReader, Flavor>;
    using Base::Base;

    bool run(Task progress) const;
};

} // namespace RD53BTools

#endif