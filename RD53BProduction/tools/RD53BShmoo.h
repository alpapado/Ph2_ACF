#ifndef RD53BSHMOO_H
#define RD53BSHMOO_H

#include "RD53BInjectionTool.h"
#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53BShmoo;

template <class Flavor>
const auto ToolParameters<RD53BShmoo<Flavor>> = make_named_tuple(std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
                                                                 std::make_pair("powerSupplyId"_s, std::string("analogCalib")),
                                                                 std::make_pair("powerSupplyChannelIds"_s, std::vector<std::string>()),
                                                                 std::make_pair("channels_to_scan"_s, std::vector<std::string>()),
                                                                 std::make_pair("vddValues"_s, int(0)),
                                                                 std::make_pair("vddMin"_s, float(0.)),
                                                                 std::make_pair("defaultVDD"_s, float(1.3)),
                                                                 std::make_pair("vddMax"_s, float(0.)));

template <class Flavor>
struct RD53BShmoo : public RD53BTool<RD53BShmoo, Flavor>
{
    using Base = RD53BTool<RD53BShmoo, Flavor>;
    using Base::Base;
    using Base::param;

    std::vector<tool_result_t<RD53BInjectionTool<Flavor>>> run(Task progress) const;
    void                                                   draw(std::vector<tool_result_t<RD53BInjectionTool<Flavor>>> result) const;
    const int                                              clockRange = 8;
};

} // namespace RD53BTools

#endif
