#include "RD53BRegTest.h"

namespace RD53BTools
{
template <class Flavor>
std::stringstream RD53BRegTest<Flavor>::run(Task progress) const {

    //parameters
    bool saveSuccesses = Base::param("saveSuccesses"_s);

    //output stream initialisation
    std::stringstream results;
    results << "Register; Expected; Readback; Result" << std::endl;

    auto& chipInterface = Base::chipInterface();

    size_t nChips = 0;
    Base::for_each_chip([&](Chip* chip) { ++nChips; });

    //loop on devices
    size_t i = 0;
    for_each_device<Chip>(Base::system(), [&](DeviceChain devices) {
        auto& fwInterface = Base::getFWInterface(devices.board);
        auto  chip        = static_cast<RD53BProd<Flavor>*>(devices.chip);

        chipInterface.WriteReg(chip, "ServiceFrameSkip", Base::param("ServiceFrameSkip"_s));

        //progress update
        auto chipProgress = progress.subTask({double(i) / nChips, double(i + 1) / nChips});
        auto regsProgress = Base::param("testPixels"_s) ? chipProgress.subTask({0, 0.01}) : chipProgress;

        //testing of the global registers
        if(Base::param("testRegs"_s))
        {
            //logging the start of the test
            LOG(INFO)
                << BOLDGREEN
                << "[" << Base::name() << "] "
                << "Testing global registers"
                << RESET;
            size_t nRegs =
                std::count_if(RD53BProd<Flavor>::Regs.begin(), RD53BProd<Flavor>::Regs.end(), [](auto reg) { return reg.type == RD53BProdConstants::RegType::ReadWrite && !reg.isVolatile; });


            //variables for global registers loop
            size_t j = 0;
            uint16_t expected = 0;
            uint16_t obtained = 0;
            bool isValueOk = false;

            //default test vectors to use
            static const std::vector<uint16_t> defaultVectors = {
                0b0101010101010101,
                0b1010101010101010
            };

            //getting the configuration for custom test vectors
            auto customVectors = Base::param("customVectors"_s);

            //global registers loop
            for(const auto& reg: RD53BProd<Flavor>::Regs)
            {
                //skip non-RW and volatile registers
                if (reg.type != RD53BProdConstants::RegType::ReadWrite || reg.isVolatile)
                    continue;

                //logging the register that is about to be tested
                LOG(INFO)
                    << BOLDYELLOW
                    << "[" << Base::name() << "] "
                    << "Testing register "
                    << reg.name
                    << RESET;

                //storing the previous value of the register
                auto oldValue = chipInterface.ReadReg(chip, reg, true);

                //test vectors to be used to verify the register
                std::vector<uint16_t> testVectors = defaultVectors;

                //if a custom configuration is found, use that
                //if no configuration is found, keep the default
                if (customVectors.find(reg.name) != customVectors.end()) {
                    LOG(INFO)
                        << BOLDYELLOW
                        << "[" << Base::name() << "] "
                        << "Found custom vectors for register "
                        << reg.name
                        << RESET;
                    testVectors = customVectors.find(reg.name)->second;
                }

                //looping on test vectors
                for (uint16_t testValue : testVectors) {
                    //writing the new register value and reading back
                    chipInterface.WriteReg(chip, reg, testValue);
                    auto newValue = chipInterface.ReadReg(chip, reg, true);

                    //variables for checking the results
                    expected = testValue & ((1 << reg.size) - 1);
                    obtained = newValue & ((1 << reg.size) - 1);
                    isValueOk = (expected == obtained);

                    //checking the readback value
                    if (!isValueOk) {
                        LOG (ERROR)
                             << BOLDRED
                             << "[" << Base::name() << "] "
                             << reg.name << " test failed: "
                             << std::bitset<16>(newValue)
                             << "!=" << std::bitset<16>(testValue)
                             << RESET;
                    }

                    //storing the result in the output stream
                    if ((isValueOk and saveSuccesses) or (!isValueOk)) {
                        std::string resultStr = isValueOk ? "OK" : "ERR";
                        results << reg.name << "; "
                            << std::bitset<16>(testValue)
                            << "; "
                            << std::bitset<16>(newValue)
                            << "; "
                            << resultStr
                            << std::endl;
                    }
                }
                //restoring the old value
                chipInterface.WriteReg(chip, reg, oldValue);

                //updating the progress
                regsProgress.update(double(j) / nRegs);
                ++j;
            }

            //logging the end of the global registers test
            LOG(INFO)
                << BOLDGREEN
                << "[" << Base::name() << "] "
                << "Completed global registers test"
                << RESET;
        }

        //testing of the pixel registers
        if(Base::param("testPixels"_s))
        {
            auto pixelsProgress = chipProgress.subTask({0.01, 1});

            // for (uint8_t value : {0b10101010, 0b01010101}) {
            //test vectors
            uint8_t maskValues[] = {0b010, 0b101};
            uint8_t tdacValues[] = {0b10101, 0b01010};

            //looping on test vectors
            for(size_t valueId: {0, 1})
            {
                uint8_t mask = maskValues[valueId];
                uint8_t tdac = tdacValues[valueId];

                auto pixelsValueProgress = pixelsProgress.subTask({valueId * 0.5, (valueId + 1) * 0.5});

                //chip configuration update
                // The pattern written to the TDAC field is the mapping of what
                // is passed to `UpdatePixelConfigUniform` via `encodeTDAC`
                // To counter this, `decodeTDAC` is applied to `tdac`
                chipInterface.UpdatePixelConfigUniform(chip, mask & 1, mask & 2, mask & 4, Flavor::decodeTDAC(tdac));

                chipInterface.WriteReg(chip, Flavor::Reg::PIX_MODE, uint16_t{1});

                //configuration to be used for the test
                size_t pixelConfig = (tdac << 3) | mask;
                size_t pairConfig  = (pixelConfig << 8) | pixelConfig;

                //logging the test vector being used
                LOG(INFO)
                    << BOLDGREEN
                    << "[" << Base::name() << "] "
                    << "Testing pixel registers using test vector "
                    << std::bitset<8>(pixelConfig)
                    << RESET;

                //looping on all pixel pairs
                for(uint16_t col_pair = 0; col_pair < Flavor::nCols / 2; ++col_pair)
                {
                    auto colProgress = pixelsValueProgress.subTask({double(col_pair) / Flavor::nCols * 2, double(col_pair + 1) / Flavor::nCols * 2});

                    std::vector<uint16_t> cmdStream;

                    chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);
                    chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);

                    chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, cmdStream, Flavor::Reg::REGION_COL.address, col_pair);

                    chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, cmdStream, Flavor::Reg::REGION_ROW.address, uint16_t{0});
                    for(uint16_t row = 0; row < Flavor::nRows; ++row)
                    {
                        chipInterface.template SerializeCommand<RD53BCmd::RdReg>(chip, cmdStream, Flavor::Reg::PIX_PORTAL.address);
                        chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);
                        chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);
                        for (int idleWord = 0; idleWord < 32; ++idleWord)
                            chipInterface.template SerializeCommand<RD53BCmd::PLLlock>(chip, cmdStream);
                        chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);
                        chipInterface.template SerializeCommand<RD53BCmd::Sync>(chip, cmdStream);
                    }
                    if (cmdStream.size()) {
                        chipInterface.SendCommandStream(chip, cmdStream);
                    }

                    //readback value for pixel registers
                    std::vector<std::pair<uint16_t, uint16_t>> regReadback;
                    std::vector<std::string> fwRegs;
                    for(uint16_t i = 0; i < Flavor::nRows; ++i) {
                        fwRegs.push_back("user.stat_regs.readout1.register_fifo_empty");
                        fwRegs.push_back("user.stat_regs.Register_Rdback_fifo");
                    }
                    fwRegs.push_back("user.stat_regs.readout1.register_fifo_empty");
                    std::vector<uint32_t> fwVals;
                    do {
                        fwVals = fwInterface.ReadStackReg(fwRegs);
                        for(std::vector<uint32_t>::size_type i = 1; i < fwVals.size(); i += 2) {
                            if(!fwVals[i - 1]) { // fifo read while not empty
                                uint16_t address, value;
                                std::tie(std::ignore, address, value)
                                    = bits::unpack<6, 10, 16>(fwVals[i]);
                                regReadback.emplace_back(address, value);
                            }
                        }
                    } while (!fwVals.empty() && !fwVals.back());

                    //stores if data has been received or not from a row
                    std::array<bool, Flavor::nRows> received = {0};

                    //looping on all readback values
                    bool isValueOk = false;
                    for(const auto& item: regReadback)
                    {
                        if(item.first & 0b1000000000)
                        {
                            auto row      = item.first & 0b111111111;
                            received[row] = true;

                            //checking the results
                            uint8_t tdacField[2], otherField[2];
                            std::tie(tdacField[0], otherField[0], tdacField[1], otherField[1]) = bits::unpack<5, 3, 5, 3>(item.second);
                            for (uint8_t &field : tdacField) field = Flavor::decodeTDAC(field);
                            isValueOk = (item.second == pairConfig);
                            if (!isValueOk)
                                LOG(ERROR) << BOLDRED << "[" << Base::name() << "] pixel pair (" << row << ", " << (col_pair * 2) << ") config test failed: " << std::bitset<16>(item.second)
                                           << "!=" << std::bitset<16>(pairConfig) << RESET;

                            //saving data to output stream
                            if ((isValueOk and saveSuccesses) or (!isValueOk)) {
                                std::string resultStr = isValueOk ? "OK" : "ERR";
                                results << "("
                                    << std::setfill('0') << std::setw(3)
                                    << row
                                    << ", "
                                    << std::setfill('0') << std::setw(3)
                                    << (col_pair * 2) << "); "
                                    << std::bitset<16>(pairConfig)
                                    << "; "
                                    << std::bitset<16>(item.second)
                                    << "; "
                                    << resultStr
                                    << std::endl;
                            }
                        }
                    }

                    //looping on rows to print if data has not been received
                    for(uint16_t row = 0; row < Flavor::nRows; ++row)
                    {
                        //checking for empty readback value
                        bool isReadbackOk = received[row];
                        if (!isReadbackOk) {
                            LOG(ERROR)
                                << BOLDRED
                                << "[" << Base::name() << "]"
                                << " pixel pair ("
                                << row << ", " << (col_pair * 2)
                                << ") config test failed: no readback"
                                << RESET;

                            //saving data to output stream
                            results << "("
                                << std::setfill('0') << std::setw(3)
                                << row
                                << ", "
                                << std::setfill('0') << std::setw(3)
                                << (col_pair * 2) << "); "
                                << std::bitset<16>(pairConfig)
                                << "; "
                                << "----------------"
                                << "; "
                                << "ERR"
                                << std::endl;
                        }

                    }

                    //progress update
                    colProgress.update(double(col_pair + 1) / (Flavor::nCols / 2));
                }
            }

            chipInterface.WriteReg(chip, "ServiceFrameSkip", 50);

        }
        ++i;
    });
    return results;
}

template <class Flavor>
void RD53BRegTest<Flavor>::draw(const std::stringstream& results) const {
    std::ofstream csvFile(Base::getOutputFilePath("results.csv"));
    csvFile << results.rdbuf();
}

template class RD53BRegTest<RD53BFlavor::ATLAS>;
template class RD53BRegTest<RD53BFlavor::CMS>;
template class RD53BRegTest<RD53BFlavor::ITkPixV2>;
template class RD53BRegTest<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
