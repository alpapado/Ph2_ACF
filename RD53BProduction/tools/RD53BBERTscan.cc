#include "RD53BBERTscan.h"

#include "Utils/xtensor/xview.hpp"

namespace RD53BTools
{
template <class Flavor>
ChipDataMap<std::vector<double>> RD53BBERTscan<Flavor>::run(Task progress) const
{
    auto& chipInterface = Base::chipInterface();

    ChipDataMap<std::vector<double>> results;
    for(int iP1 = 0; iP1 < param1_npoints; ++iP1)
    {
        int param1 = param1_min + iP1 * (param1_max - param1_min) / (param1_npoints - 1);
        std::cout << "Setting reg " << param1_name << " to " << param1 << std::endl;
        Base::for_each_chip([&](Chip* chip) { chipInterface.WriteReg(chip, param1_name, param1); });
        for(int iP2 = 0; iP2 < param2_npoints; ++iP2)
        {
            if(param2_name.length() > 0)
            {
                int param2 = param2_min + iP2 * (param2_max - param2_min) / (param2_npoints - 1);
                std::cout << "Setting reg " << param2_name << " to " << param2 << std::endl;
                Base::for_each_chip([&](Chip* chip) { chipInterface.WriteReg(chip, param2_name, param2); });
            }
            auto chipResults = runBER(progress);
            Base::for_each_chip([&](Chip* chip) { results[chip].push_back(chipResults[chip]); });

            progress.update(double(iP2 + iP1 * param2_npoints) / param1_npoints * param2_npoints);
        }
    }

    return results;
}

template <class Flavor>
ChipDataMap<double> RD53BBERTscan<Flavor>::runBER(Task progress) const
{
    ChipDataMap<double> results;
    auto&               chipInterface = Base::chipInterface();

    if(chain2test == 1) { throw std::runtime_error(std::string("BE-lpGBT BER tests not yet supported for RD53B tools")); }

    for(auto* board: *Base::system().fDetectorContainer)
    {
        auto&    fwInterface   = Base::getFWInterface(board);
        uint32_t frontendSpeed = fwInterface.ReadoutSpeed();
        for(auto* optgroup: *board)
        {
            for(auto* hybrid: *optgroup)
            {
                for(auto* chip: *hybrid)
                {
                    chipInterface.StartPRBSpattern(chip);
                    bool success = false;
                    int  attempt = 0;
                    while(!success && attempt < 5)
                    {
                        // do the BER test
                        if(chain2test == 0) { results[chip] = fwInterface.RunBERtest(given_time, frames_or_time, hybrid->getId(), static_cast<RD53Base*>(chip)->getChipLane(), frontendSpeed); }
                        else
                        {
                            uint8_t cGroup   = static_cast<RD53*>(chip)->getRxGroups()[0];
                            uint8_t cChannel = static_cast<RD53*>(chip)->getRxChannel();
                            results[chip]    = Base::system().flpGBTInterface->RunBERtest(optgroup->flpGBT, cGroup, cChannel, given_time, frames_or_time, frontendSpeed);
                        }
                        chipInterface.StopPRBSpattern(chip);
                        chipInterface.InitRD53Downlink(board);
                        chipInterface.StopPRBSpattern(chip);

                        attempt++;
                        if(results[chip] >= 0) success = true;

                        chipInterface.WriteReg(chip, "DAC_CML_BIAS_0", 500);
                    }
                }
            }
        }
    }

    return results;
}

template <class Flavor>
void RD53BBERTscan<Flavor>::draw(ChipDataMap<std::vector<double>> result)
{
    Base::createRootFile();
    if(param2_name.length() == 0)
    {
        Base::for_each_chip([&](Chip* chip) {
            xt::xtensor<float, 1> plot = xt::zeros<float>({param1_npoints});
            for(auto iu = 0u; iu < result[chip].size(); ++iu)
            {
                std::cout << iu << " " << result[chip].at(iu) << std::endl;
                plot(iu) = result[chip].at(iu);
            }
            Base::drawHistRaw(plot, "BER scan", param1_min, param1_max, param1_name, "Bit error rate (frames with errors)");
        });
    }
    else
    {
        Base::for_each_chip([&](Chip* chip) {
            xt::xtensor<float, 2> plot = xt::zeros<float>({param1_npoints, param2_npoints});
            for(auto iu = 0u; iu < result[chip].size(); ++iu)
            {
                int iparam1            = iu % param2_npoints;
                int iparam2            = (iu - iparam1) / param2_npoints;
                plot(iparam1, iparam2) = result[chip].at(iu);
            }
            Base::drawHist2D(plot, "BER scan", param1_min, param1_max, param2_min, param2_max, param1_name, param2_name, "Bit error rate (frames with errors)");
        });
    }
    return;
}

template class RD53BBERTscan<RD53BFlavor::ATLAS>;
template class RD53BBERTscan<RD53BFlavor::CMS>;
template class RD53BBERTscan<RD53BFlavor::ITkPixV2>;
template class RD53BBERTscan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
