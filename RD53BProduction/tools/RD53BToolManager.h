#ifndef RD53BTOOLMANAGER_H
#define RD53BTOOLMANAGER_H

#include "RD53BTool.h"

#include "Utils/indicators/cursor_control.hpp"
#include "Utils/indicators/progress_bar.hpp"

#include <boost/iostreams/filter/line.hpp>
#include <exception>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <boost/iostreams/filtering_streambuf.hpp>
#pragma GCC diagnostic pop

namespace RD53BTools
{
template <class Tools>
struct ToolManager : public ToolManagerBase
{
    ToolManager(SystemController& system, const std::string& configPath, bool showPlots = true, std::string resultsPath = "Results/")
        : ToolManagerBase(system, configPath, std::move(resultsPath)), _showPlots(showPlots)
    {
        initialize<Tools>();
    }

    void run_tools(const std::vector<std::string>& toolNames) const
    {
        for(const auto& toolName: toolNames) run_tool(toolName);
    }

    template <class T, typename std::enable_if_t<!std::is_base_of<RD53BToolBase, T>::value && !std::is_base_of<NamedTupleBase, T>::value, int> = 0>
    static void update_arg_value(T& arg, const toml::value& value)
    {
        arg = toml::get<T>(value);
    }

    template <class T, typename std::enable_if_t<std::is_base_of<RD53BToolBase, T>::value, int> = 0>
    static void update_arg_value(T& arg, const toml::value& value)
    {
        update_arg_value(arg.params(), value);
    }

    template <class T, typename std::enable_if_t<std::is_base_of<NamedTupleBase, T>::value, int> = 0>
    static void update_arg_value(T& named_tuple, const toml::value& table)
    {
        for(const auto& item: table.as_table())
            if(!visit(named_tuple, item.first, [&](auto& value) { update_arg_value(value, item.second); }))
                // throw std::runtime_error("Invalid argument: " + item.first);
                throw std::runtime_error(toml::format_error("Invalid argument", item.second, item.first + " does not match any parameter name"));
    }

    void run_tool(const std::string& toolStr) const
    {
        auto                                         it = std::find(toolStr.begin(), toolStr.end(), '{');
        std::string                                  toolName(toolStr.begin(), it);
        std::unordered_map<std::string, toml::value> cmd_line_args;
        if(it != toolStr.end())
        {
            toml::detail::location loc{toolStr, std::vector<char>(it, toolStr.end())};
            cmd_line_args = toml::detail::parse_inline_table<toml::value>(loc).unwrap().first;
        }

        std::string typeName = getToolTypeName(toolName);
        bool        found    = false;
        Tools::for_each_index([&](auto index) {
            if(!found && Tools::names[index.value] == typeName)
            {
                found = true;
                try {
                    auto configured_args = getToolArgs(toolName);

                    auto tool = typename Tools::template field_type<index.value>(this, toolName, configured_args, _showPlots);

                    update_arg_value(tool.params(), cmd_line_args);

                    LOG(INFO) << "Starting " << toolName << " with parameters: ";
                    std::cout << tool.params() << std::endl;

                    tool.init();
                    run_tool(tool);
                }
                catch (std::exception& e) {
                    std::cout << "Exception occured while running " << toolName << ": " << e.what() << std::endl;
                }
            }
        });
        if(!found) throw std::runtime_error(toolName + " has a type (" + typeName + ") which does not exist");
    }

  private:
    template <class ToolType, typename std::enable_if_t<has_draw_v<ToolType>, int> = 0>
    void run_tool(ToolType&& tool) const
    {
        tool.Draw(run_with_progress(tool));
    }

    template <class ToolType, typename std::enable_if_t<!has_draw_v<ToolType>, int> = 0>
    void run_tool(ToolType&& tool) const
    {
        run_with_progress(tool);
    }

    // Simple Filter that inserts new lines before the last one
    struct LineFilter : boost::iostreams::line_filter
    {
        std::string do_filter(const std::string& line) { return "\033[0m\n\033[A\033[1L" + line; }
    };

    template <class ToolType>
    auto run_with_progress(ToolType&& tool) const
    {
        // Change the streambuf of std::cout, to force all new lines to be inserted before the last one which is used for the progress bar.

        // get stdout streambuf
        auto* stdout_buf = std::cout.rdbuf();

        // create filtering streambuf
        boost::iostreams::filtering_ostreambuf filtering_buf{};
        filtering_buf.push(LineFilter()); // add line filter
        filtering_buf.push(*stdout_buf);  // stdout_buf

        // force cout to use our buffer
        std::cout.rdbuf(&filtering_buf);

        // create local ostream acting as std::cout normally would
        std::ostream os(stdout_buf);

        termcolor::colorize(os);

        indicators::ProgressBar bar{indicators::option::Start{" ["},
                                    indicators::option::Fill{"█"},
                                    indicators::option::Lead{"█"},
                                    indicators::option::Remainder{"-"},
                                    indicators::option::End{"]"},
                                    indicators::option::PrefixText{tool.name()},
                                    indicators::option::ForegroundColor{indicators::Color::green},
                                    indicators::option::ShowElapsedTime{true},
                                    indicators::option::ShowRemainingTime{true},
                                    indicators::option::ShowPercentage{true},
                                    indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}},
                                    indicators::option::Stream{os}};

        indicators::show_console_cursor(false);

        // std::cout << "\x1b[?7l"; // auto-wrap mode off

        Task progress{bar};

        progress.update(0);

        try {

            auto result = run_with_progress(tool, progress);

            if(bar.current() < 100) progress.update(100);

            // std::cout << "\x1b[?7h"; // auto-wrap mode on

            indicators::show_console_cursor(true);

            // reset cout
            std::cout.rdbuf(stdout_buf);

            return result;
            
        }
        catch (std::exception& e) {
            indicators::show_console_cursor(true);
            std::cout.rdbuf(stdout_buf);
            throw;
        }
    }

    template <class ToolType, typename std::enable_if_t<has_progress_v<ToolType>, int> = 0>
    auto run_with_progress(ToolType&& tool, Task task) const
    {
        return tool.run(task);
    }

    template <class ToolType, typename std::enable_if_t<!has_progress_v<ToolType>, int> = 0>
    auto run_with_progress(ToolType&& tool, Task task) const
    {
        return tool.run();
    }

  private:
    bool _showPlots;
};

} // namespace RD53BTools

#endif