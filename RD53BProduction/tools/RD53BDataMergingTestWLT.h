#ifndef RD53BDATAMERGINGTESTWLT_H
#define RD53BDATAMERGINGTESTWLT_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BDataMergingTestWLT; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BDataMergingTestWLT<Flavor>> = make_named_tuple();

template <class Flavor>
struct RD53BDataMergingTestWLT : public RD53BTool<RD53BDataMergingTestWLT, Flavor> {
    using Base = RD53BTool<RD53BDataMergingTestWLT, Flavor>;
    using Base::Base;

    bool run(Task progress) const;
};

}

#endif
