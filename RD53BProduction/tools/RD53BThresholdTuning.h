#ifndef RD53BThresholdTuning_H
#define RD53BThresholdTuning_H

#include "RD53BThresholdEqualization.h"

namespace RD53BTools
{
template <class>
struct RD53BThresholdTuning; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BThresholdTuning<Flavor>> = make_named_tuple(std::make_pair("targetThreshold"_s, 300),
                                                                           std::make_pair("gdacRange"_s, std::vector<size_t>({380, 500})),
                                                                           std::make_pair("ldacRange"_s, std::vector<size_t>({0, 200})),
                                                                           std::make_pair("vcalMed"_s, 300),
                                                                           std::make_pair("thresholdEqualization"_s, RD53BThresholdEqualization<Flavor>()),
                                                                           std::make_pair("belowThresholdPixelRatio"_s, 0.01),
                                                                           std::make_pair("aboveThresholdPixelRatio"_s, 0.01),
                                                                           std::make_pair("occupancyThresholdBelow"_s, 0.9),
                                                                           std::make_pair("occupancyThresholdAbove"_s, 0.1),
                                                                           std::make_pair("stuckPixelOccThreshold"_s, 0.9));

template <class Flavor>
struct RD53BThresholdTuning : public RD53BTool<RD53BThresholdTuning, Flavor>
{
    using Base = RD53BTool<RD53BThresholdTuning, Flavor>;
    using Base::Base;
    using Base::param;

    void init();

    tool_result_t<RD53BThresholdEqualization<Flavor>> run(Task progress);

    void draw(const tool_result_t<RD53BThresholdEqualization<Flavor>>&);

  private:
    void tune_regs(Task                                                      progress,
                   const std::vector<typename RD53BProdConstants::Register>& regs,
                   size_t                                                    minValue,
                   size_t                                                    maxValue,
                   double                                                    targetRatio,
                   bool                                                      inverseDirection   = false,
                   float                                                     occupancyThreshold = 0.5) const;

    RD53BInjectionTool<Flavor>* injectionTool;
};

} // namespace RD53BTools

#endif