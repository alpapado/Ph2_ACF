#ifndef RD53BBERTSCAN_H
#define RD53BBERTSCAN_H

#include "../HWInterface/RD53InterfaceBase.h"
#include "../HWInterface/lpGBTInterface.h"
#include "HWDescription/RD53BProd.h"
#include "RD53BTool.h"
#include "Utils/RD53BUtils.h"

namespace RD53BTools
{
template <class>
struct RD53BBERTscan;

template <class Flavor>
const auto ToolParameters<RD53BBERTscan<Flavor>> = make_named_tuple(std::make_pair("param1_name"_s, std::string("DAC_CML_BIAS_0")),
                                                                    std::make_pair("param1_min"_s, 200),
                                                                    std::make_pair("param1_max"_s, 500),
                                                                    std::make_pair("param1_npoints"_s, 50),
                                                                    std::make_pair("param2_name"_s, std::string("")),
                                                                    std::make_pair("param2_min"_s, 0),
                                                                    std::make_pair("param2_max"_s, 0),
                                                                    std::make_pair("param2_npoints"_s, 1),
                                                                    std::make_pair("given_time"_s, bool(false)),
                                                                    std::make_pair("frames_or_time"_s, long(500)),
                                                                    std::make_pair("chain2test"_s, int(0)));

template <class Flavor>
struct RD53BBERTscan : public RD53BTool<RD53BBERTscan, Flavor>
{
    using Base = RD53BTool<RD53BBERTscan, Flavor>;
    using Base::Base;
    using Base::param;

    bool      given_time;
    long long frames_or_time;
    int       chain2test;

    std::string param1_name;
    int         param1_min;
    int         param1_max;
    int         param1_npoints;

    std::string param2_name;
    int         param2_min;
    int         param2_max;
    int         param2_npoints;

    void init()
    {
        given_time     = param("given_time"_s);
        frames_or_time = param("frames_or_time"_s);
        chain2test     = param("chain2test"_s);

        param1_name    = param("param1_name"_s);
        param1_npoints = param("param1_npoints"_s);
        param1_max     = param("param1_max"_s);
        param1_min     = param("param1_min"_s);

        param2_name    = param("param2_name"_s);
        param2_npoints = param("param2_npoints"_s);
        param2_max     = param("param2_max"_s);
        param2_min     = param("param2_min"_s);
    }

    ChipDataMap<std::vector<double>> run(Task progress) const;
    void                             draw(ChipDataMap<std::vector<double>> result);
    ChipDataMap<double>              runBER(Task progress) const;
};

} // namespace RD53BTools

#endif
