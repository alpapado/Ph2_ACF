#ifndef PLLLOCKSCAN_H
#define PLLLOCKSCAN_H

#include "RD53BTool.h"
#include "xtensor/xtensor_forward.hpp"

#include <xtensor/xarray.hpp>

namespace RD53BTools
{
template <class>
struct PllLockScan; // forward declaration

template <class Flavor>
const auto ToolParameters<PllLockScan<Flavor>> = make_named_tuple(
    std::make_pair("periodRange"_s, std::vector<uint32_t>{6u, 32u}),
    std::make_pair("periodStep"_s, 2u),
    std::make_pair("durationRange"_s, std::vector<uint32_t>{6u, 32u}),
    std::make_pair("durationStep"_s, 1u),
    std::make_pair("repetitions"_s, 10u)
);

template <class Flavor>
struct PllLockScan : public RD53BTool<PllLockScan, Flavor>
{
    using Base = RD53BTool<PllLockScan, Flavor>;
    using Base::Base;
    using Base::param;

    using Results = ChipDataMap<xt::xarray<int>>;

    void init();

    Results run(Task progress) const;

    void draw(const Results& results);

private:
    xt::xarray<int> periods;
    xt::xarray<int> durations;    
};

} // namespace RD53BTools

#endif