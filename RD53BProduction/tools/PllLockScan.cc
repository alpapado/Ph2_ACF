#include "PllLockScan.h"
#include "HWDescription/RD53BProd.h"
#include "HWInterface/RD53BProdInterface.h"
#include "RD53BProduction/tools/RD53BTool.h"
#include "xtensor/xrandom.hpp"
#include "xtensor/xtensor_forward.hpp"
#include <cstddef>

namespace RD53BTools
{

template <class Flavor>
void PllLockScan<Flavor>::init() {
    periods = xt::arange(param("periodRange"_s)[0], param("periodRange"_s)[1], param("periodStep"_s));
    durations = xt::arange(param("durationRange"_s)[0], param("durationRange"_s)[1], param("durationStep"_s));
}


template <class Flavor>
typename PllLockScan<Flavor>::Results PllLockScan<Flavor>::run(Task progress) const {
    RD53BProdInterface<Flavor>& chipInterface = Base::chipInterface();
    
    pixel_matrix_t<Flavor, uint8_t> tdac = xt::random::randint({Flavor::nRows, Flavor::nCols}, 0, 32);
    pixel_matrix_t<Flavor, uint8_t> enable = xt::random::randint({Flavor::nRows, Flavor::nCols}, 0, 2);
    pixel_matrix_t<Flavor, uint8_t> enableInjections = xt::random::randint({Flavor::nRows, Flavor::nCols}, 0, 2);
    pixel_matrix_t<Flavor, uint8_t> enableHitor = xt::random::randint({Flavor::nRows, Flavor::nCols}, 0, 2);

    Results retries;
    Base::for_each_chip([&] (auto chip) {
        retries.insert({chip, xt::zeros<int>({periods.size(), durations.size()})});
    });

    for (auto ip = 0u; ip < periods.size(); ++ip) {
        auto period = periods[ip];
        auto subTask = progress.subTask({double(ip) / periods.size(), double(ip + 1) / periods.size()});
        for (auto id = 0u; id < durations.size(); ++id) {
            auto duration = durations[id];
            chipInterface.PLLlockPeriod = period;
            chipInterface.nPLLlockWords = duration;
            Base::for_each_chip([&] (auto chip) {
                int sum = 0;
                for (auto j = 0u; j < param("repetitions"_s); ++j)
                    sum += chipInterface.UpdatePixelConfig(chip, enable, enableInjections, enableHitor, tdac);
                retries[chip](ip, id) = sum;
                std::cout << period << ", " << duration << ": " << sum << std::endl;
            });
            subTask.update(double(id) / durations.size());
        }
        progress.update(double(ip) / periods.size());
    }
    return retries;
}

template <class Flavor>
void PllLockScan<Flavor>::draw(const Results& results) {
    Base::createRootFile();

    Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
        Base::createRootFileDirectory(chip);

        Base::drawHist2D(results.at(chip), "retries");
    });
}

template class PllLockScan<RD53BFlavor::ATLAS>;
template class PllLockScan<RD53BFlavor::CMS>;
template class PllLockScan<RD53BFlavor::ITkPixV2>;
template class PllLockScan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
