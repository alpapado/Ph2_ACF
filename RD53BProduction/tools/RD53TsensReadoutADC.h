#ifndef RD53TsensReadoutADC_H
#define RD53TsensReadoutADC_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53TsensReadoutADC : public RD53BTool<RD53TsensReadoutADC, Flavor>
{
    using Base = RD53BTool<RD53TsensReadoutADC, Flavor>;
    using Base::Base;

    struct ChipResults
    {
        double valueLow;
        double valueHigh;
        double temperature[10];
        double measureDV;
    };

    static constexpr int sensor_VMUX[8]    = {0b1000000000101, 0b1000000000110, 0b1000000001101, 0b1000000001110, 0b1000000001111, 0b1000000010000, 0b1000000010001, 0b1000000010010};
    double               idealityFactor[8] = {2.38, 5.04, 1.02, 1.26, 1.01, 1.26, 1.02, 1.24};

    double ADCintercept = 0.0182227;
    double ADCslope     = 0.00018808;

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;
};

} // namespace RD53BTools

#endif
