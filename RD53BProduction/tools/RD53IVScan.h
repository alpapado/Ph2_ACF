#ifndef RD53IVScan_H
#define RD53IVScan_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53IVScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53IVScan<Flavor>> = make_named_tuple(std::make_pair("configFile"_s, std::string("config/iv_it_sldo.xml")),
                                                                 std::make_pair("type"_s, std::string("complete")),
                                                                 std::make_pair("powerSupplyName"_s, std::string("powerSupply")),
                                                                 std::make_pair("channelID_Digital"_s, std::string("first")),
                                                                 std::make_pair("channelID_Analog"_s, std::string("second")),
                                                                 std::make_pair("multimeterName"_s, std::string("multimeter")),
                                                                 std::make_pair("powerSupplyVoltageProtection"_s, 3.0),
                                                                 std::make_pair("powerSupplyVoltageCompliance"_s, 2.5),
                                                                 std::make_pair("scanPointCurrentRangeFirst"_s, std::vector<float>({2.0, 0.4})),
                                                                 std::make_pair("scanPointCurrentRangeSecond"_s, std::vector<float>({0.0, 2.0})),
                                                                 std::make_pair("scanPointCurrentStep"_s, 0.1),
                                                                 std::make_pair("finalCurrentPoint"_s, 1.0),
                                                                 std::make_pair("VDDDTrim"_s, 8),
                                                                 std::make_pair("VDDATrim"_s, 8),
                                                                 std::make_pair("trimVDDOnly"_s, false));

template <class Flavor>
struct RD53IVScan : public RD53BTool<RD53IVScan, Flavor>
{
    using Base = RD53BTool<RD53IVScan, Flavor>;
    using Base::Base;

    struct ChipResults
    {
        double VMUXvolt[41];
        double IMUXvolt[33];
    };

    ChipDataMap<ChipResults> run(Task progress) const;
};

} // namespace RD53BTools

#endif
