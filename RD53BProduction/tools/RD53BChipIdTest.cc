
#include "RD53BChipIdTest.h"

#include <array>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include "RD53BTool.h"

#include "../HWDescription/RD53B.h"
#include "../HWInterface/RD53BInterface.h"

namespace RD53BTools {

template <class Flav>
typename RD53BChipIdTest<Flav>::Results RD53BChipIdTest<Flav>::run() {
    Ph2_System::SystemController& sysCtrller = Base::system();
    unsigned nChip = 0;
    for_each_device<Chip>(sysCtrller, [&nChip] (Chip* chip) {
        ++nChip;
    });
    if(nChip > 1) {
        throw std::runtime_error("Multi-chip configurations are not supported by this tool");
    }

    Results results;
    results.readsPerReg = 1;

    initCommWPAC();

    unsigned long original = 0xff00;
    for(unsigned attempts = 0; attempts <= param("wpacRetries"_s); ++attempts) {
        try {
            original = readExpander();
            break;
        }
        catch(const std::runtime_error& e) {
            LOG(ERROR) << e.what() << RESET;
            if(attempts == param("wpacRetries"_s)) {
                throw std::runtime_error("Failed to read I2C expander configuration");
            }
        }
    }

    for_each_device<Chip>(sysCtrller, [this, &results] (DeviceChain chain) {
        RD53FWInterface& fwIface = RD53BTool<RD53BChipIdTest, Flav>::getFWInterface(chain.board);
        for(Id setId = 0; setId <= maxId; ++setId) {
            for(bool& a : results.success[setId]) {
                a = false;
            }
            if(!setChipId(setId)) {
                LOG(ERROR) << "Failed to set chip id " << setId << ", skipping" << RESET;
                continue;
            }
            LOG(INFO) << "Testing chip id " << setId << RESET;
            results.badPadReadout[setId] = isPadReadoutBad(setId, fwIface, chain.hybrid);
            const char* const mode[2] = {"addressed", "broadcast"};
            for(int i : {0, 1}) { // i is index for mode[]
                LOG(INFO) << "Sending " << mode[i] << " commands to the chip" << RESET;
                testActivity(fwIface, chain.hybrid, results.activity[setId][i], i);
                results.success[setId][i] = true; ///> useless now, could be removed
            }
            LOG(INFO) << "Testing of chip id " << setId << " complete" << RESET;
        }
    });

    for(unsigned attempts = 0; attempts <= param("wpacRetries"_s); ++attempts) {
        if(writeExpander(original)) {
            break;
        }
        else if(attempts == param("wpacRetries"_s)) {
            throw std::runtime_error("Failed to restore original I2C expander configuration");
        }
    }

    releaseWPAC();

    return results;
}

template <class Flav>
void RD53BChipIdTest<Flav>::draw(const RD53BChipIdTest::Results& results) const {
    const char* mode[2] = {"addressed", "broadcast"};
    unsigned efficiency[2][maxId + 1] = {{}, {}};
    unsigned contamination[maxId + 1] = {};
    std::ofstream json(Base::getOutputFilePath("results.json"));
    json << "{";
    for(Id setId = 0; setId <= maxId; ++setId) {
        json << "\"" << setId << "\":{"; // data for one chip id begins here
        json << "\"bad_pad_readout\":" << (results.badPadReadout[setId] ? "true" : "false");
        const char* comma = ",";
        for(const int i : {0, 1}) {
            json << comma << "\"" << mode[i] << "\":{"; // mode data begins here
            json << "\"valid\":" << (results.success[setId][i] ? "true" : "false");
            if(results.success[setId][i]) {
                json << ",";
                json << "\"activity\":[";
                for(Id testId = 0; testId <= maxId; ++testId) {
                    json << results.activity[setId][i][testId];
                    if(testId < maxId) {
                        json << ",";
                    }
                    if(i == 1 || testId == setId) {
                        efficiency[i][setId] += results.activity[setId][i][testId];
                    }
                    else if(i == 0) {
                        contamination[setId] += results.activity[setId][i][testId];
                    }
                }
                json << "]";
            }
            json << "}"; // mode data ends here
            comma = ",";
        }
        json << "},"; // data for one chip id ends here
    }
    json << "\"tot_reads_per_reg\":" << results.readsPerReg << ",";
    const char* comma = "";
    json << "\"broadcast_efficiency\":[";
    for(const unsigned& a : efficiency[1]) {
        json << comma << a;
        comma = ",";
    }
    json << "],\"addressed_efficiency\":[";
    comma = "";
    for(const unsigned& a : efficiency[0]) {
        json << comma << a;
        comma = ",";
    }
    json << "],\"contamination\":[";
    comma = "";
    for(const unsigned& a : contamination) {
        json << comma << a;
        comma = ",";
    }
    json << "]}";
}

template <class Flav>
void RD53BChipIdTest<Flav>::initCommWPAC() {
    if(param("noWPAC"_s)) {
        return;
    }

    fWPAC = ::open(param("wpacPort"_s).c_str(), O_RDWR);
    if(fWPAC < 0) {
        throw std::runtime_error("Could not open WPAC port");
    }
    ::tcgetattr(fWPAC, &conf[0]);
    ::tcgetattr(fWPAC, &conf[1]);
    ::cfmakeraw(&conf[1]);
    conf[1].c_cc[VMIN] = 0; // timeout timer starts immediately
    conf[1].c_cc[VTIME] = 1; // == 0.1 s
    ::tcsetattr(fWPAC, TCSADRAIN, &conf[1]);
}

template <class Flav>
void RD53BChipIdTest<Flav>::releaseWPAC() {
    if(param("noWPAC"_s)) {
        return;
    }
    ::tcsetattr(fWPAC, TCSADRAIN, &conf[0]); // restoring original configuration
    ::close(fWPAC);
}

template <class Flav>
unsigned long RD53BChipIdTest<Flav>::readExpander() {
    if(param("noWPAC"_s)) {
        LOG(WARNING) << "Faking reading state = 0 from I2C expander" << RESET;
        return 0;
    }

    std::string reply;
    std::ostringstream i2cR;

    i2cR << std::hex << "IR," << i2cAddr << "\n";
    ::write(fWPAC, i2cR.str().c_str(), i2cR.str().size());
    char c = '\0';
    while(c != '\n' && ::read(fWPAC, &c, 1) > 0) {
        reply.push_back(c);
    }
    LOG(INFO) << "Read " << reply << " from the I2C expander" << RESET;
    if(
        reply.size() != 7 ||
        reply.find("I,0x") != 0 ||
        reply.find_first_not_of("0123456789ABCDEFabcdef", 4) != 6 ||
        reply.find('\n') != 6
    ) {
        throw std::runtime_error("Failed to read the I2C expander configuration");
    }
    return std::stoul(reply.substr(4, 2), nullptr, 16);
}

template <class Flav>
bool RD53BChipIdTest<Flav>::writeExpander(unsigned long data) {
    if(param("noWPAC"_s)) {
        LOG(WARNING) << "Faking writing state of the I2C expander" << RESET;
        return true;
    }

    std::string reply;
    std::ostringstream i2cW;

    i2cW << std::hex << "IW," << i2cAddr << "," << data << "\n";
    ::write(fWPAC, i2cW.str().c_str(), i2cW.str().size());
    char c = '\0';
    while(c != '\n' && ::read(fWPAC, &c, 1) > 0) {
        reply.push_back(c);
    }
    return reply == "OK\n";
}

template <class Flav>
bool RD53BChipIdTest<Flav>::setChipId(Id id) {
    if(id > maxId) {
        throw std::logic_error("Invalid chip id setting requested");
    }

    if(param("noWPAC"_s)) {
        LOG(WARNING) << "Running the test without setting the chip id" << RESET;
        return true;
    }

    for(unsigned attempts = 0; attempts <= param("wpacRetries"_s); ++attempts) { // equal sign intended
        unsigned long data;

        try {
           data = readExpander();
        }
        catch (const std::runtime_error& e) {
            LOG(ERROR) << e.what() << RESET;
            continue;
        }

        data = (data & 0xf0) | (id & 0x0f);
        if(!writeExpander(data)) {
            continue;
        }

        try {
            if(readExpander() == data) {
                return true;
            }
        }
        catch (const std::runtime_error& e) {
            LOG(ERROR) << e.what() << RESET;
        }
    }
    return false;
}

template <class Flav>
void RD53BChipIdTest<Flav>::testActivity(RD53FWInterface& fwIface, const Hybrid* hybrid, unsigned (&activity)[maxId + 1], bool broadcast) {
    // clear the output array
    for(unsigned& a : activity) {
        a = 0;
    }
    // send read commands to the chip
    std::vector<uint16_t> q;
    Id baseId = broadcast ? (maxId + 1) : 0;
    for(Id testId = 0; testId <= maxId; ++testId) {
        q.clear();
        RD53BCmd::serialize(RD53BCmd::RdReg{baseId + testId, 1}, q);
        fwIface.WriteChipCommand(q, hybrid->getHybridId());
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        while(!fwIface.ReadReg("user.stat_regs.readout1.register_fifo_empty")) {
            // TODO check chip id from Aurora? (if firmware supports it)
            fwIface.ReadReg("user.stat_regs.Register_Rdback_fifo"); // don't care about the received data
            activity[testId] = 1;
        }
    }
}


template class RD53BChipIdTest<RD53BFlavor::ATLAS>;
template class RD53BChipIdTest<RD53BFlavor::CMS>;
template class RD53BChipIdTest<RD53BFlavor::ITkPixV2>;
template class RD53BChipIdTest<RD53BFlavor::CROCv2>;

}
