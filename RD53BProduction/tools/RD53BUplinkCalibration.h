#ifndef RD53BUplinkCalibration_H
#define RD53BUplinkCalibration_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class>
struct RD53BUplinkCalibration; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BUplinkCalibration<Flavor>> = make_named_tuple(std::make_pair("tap0Range"_s, std::vector<size_t>({0, 500})),
                                                                             std::make_pair("tap0Step"_s, 10u),
                                                                             std::make_pair("nFrames"_s, 10000000u));

template <class Flavor>
struct RD53BUplinkCalibration : public RD53BTool<RD53BUplinkCalibration, Flavor>
{
    using Base = RD53BTool<RD53BUplinkCalibration, Flavor>;
    using Base::Base;
    using Base::param;

    ChipDataMap<std::vector<size_t>> run() const;

    void draw(const ChipDataMap<std::vector<size_t>>& results);
};

} // namespace RD53BTools

#endif
