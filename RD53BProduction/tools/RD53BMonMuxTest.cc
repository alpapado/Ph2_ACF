#include "RD53BMonMuxTest.h"

#include "../HWDescription/RD53B.h"
#include "../HWInterface/RD53BInterface.h"
//#include "RD53BRegister.h"

namespace RD53BTools {

template <class Flavor>
std::set<uint8_t> RD53BMonMuxTest<Flavor>::vRefAna1 {
    0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
};

template <class Flavor>
std::set<uint8_t> RD53BMonMuxTest<Flavor>::vRefAna2 {
    4, 31, 32, 33, 34, 35, 36
};

template <class Flavor>
std::set<uint8_t> RD53BMonMuxTest<Flavor>::vRefDig {
    37, 38, 39
};

template <class Flavor>
ChipDataMap<typename RD53BMonMuxTest<Flavor>::Results> RD53BMonMuxTest<Flavor>::run() const {
    //results for each chip
    ChipDataMap<Results> results;

    //interface to instruments
    TCPClient& psInterface = *Base::system().fPowerSupplyClient;

    //measurement command for the instrument
    std::string psCmd(
        "ReadVoltmeter,"
        "VoltmeterId:" + param("voltmeterId"_s) + ","
        "ChannelId:" + param("voltmeterChannelId"_s)
    );

    //looping on each chip
    Base::for_each_chip([&](Chip* chip) {
        //chip interface used to write the chip registers
        auto& chipInterface = Base::chipInterface();

        //looping on VMUX measurements
        for(uint8_t sel : param("vList"_s)) {
            //setting the multiplexer
            chipInterface.WriteReg(chip, Flavor::Reg::MonitorConfig, static_cast<uint8_t>(Flavor::IMux::HIGH_Z) << 6 | sel);
            //getting the name of the VMux entry from the map
            std::string name;
            for(const std::pair<const std::string, uint8_t>& p : Flavor::VMuxMap) {
                if(p.second == sel) {
                    name = p.first;
                    break;
                }
            }
            //performing the measurement and adding it to the results
            float meas = std::strtof(psInterface.sendAndReceivePacket(psCmd).c_str(), nullptr);
            float gndCorr = 0.0f;
            if(vRefAna1.count(sel)) gndCorr = param("GNDA_REF1"_s);
            else if(vRefAna2.count(sel)) gndCorr = param("GNDA_REF2"_s);
            else if(vRefDig.count(sel)) gndCorr = param("GNDD_REF"_s);
            results[chip].v[name] = meas - gndCorr;
        } //end of VMUX loop

        //looping on IMUX measurements
        for(uint8_t sel : param("iList"_s)) {
            //setting the multiplexer
            chipInterface.WriteReg(chip, Flavor::Reg::MonitorConfig, sel << 6 | static_cast<uint8_t>(Flavor::VMux::IMUX_OUT));
            //getting the name of the VMux entry from the map
            std::string name;
            for(const std::pair<const std::string, uint8_t>& p : Flavor::IMuxMap) {
                if(p.second == sel) {
                    name = p.first;
                    break;
                }
            }
            //performing the measurement and adding it to the results
            float meas = std::strtof(psInterface.sendAndReceivePacket(psCmd).c_str(), nullptr);
            meas -= param("GNDA_REF1"_s);
            meas /= param("currentMuxResistance"_s);
            results[chip].i[name] = meas;
        } //end of IMUX loop
    });

    return results;
}

template <class Flavor>
void RD53BMonMuxTest<Flavor>::draw(const ChipDataMap<typename RD53BMonMuxTest<Flavor>::Results>& results) const {
    //initialising the output file
    std::ofstream csvFile(Base::getOutputFilePath("results.csv"));
    csvFile << "Name;Type;Measurement;Unit" << std::endl;

    //looping on the results
    for(auto p : results) {
        //looping on VMUX measurements
        LOG(INFO) << "=== VMUX ===";
        for(const std::pair<std::string, float> q : p.second.v) {
            LOG(INFO) << "\"" << q.first << "\" -> " << std::scientific << q.second << " V";
            csvFile << q.first << ";VMUX;" << std::scientific << q.second << ";V" << std::endl;
        }
        //looping on IMUX measurements
        LOG(INFO) << "=== IMUX ===";
        for(const std::pair<std::string, float> q : p.second.i) {
            LOG(INFO) << "\"" << q.first << "\" -> " << std::scientific << q.second << " A";
            csvFile << q.first << ";IMUX;" << std::scientific << q.second << ";A" << std::endl;
        }
    }

}

template class RD53BMonMuxTest<RD53BFlavor::ATLAS>;
template class RD53BMonMuxTest<RD53BFlavor::CMS>;
template class RD53BMonMuxTest<RD53BFlavor::ITkPixV2>;
template class RD53BMonMuxTest<RD53BFlavor::CROCv2>;

}
