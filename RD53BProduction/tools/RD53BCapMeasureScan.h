#ifndef RD53BCapMeasureScan_H
#define RD53BCapMeasureScan_H

#include "RD53BTool.h"

namespace RD53BTools
{
template <class Flavor>
struct RD53BCapMeasureScan : public RD53BTool<RD53BCapMeasureScan, Flavor>
{
    using Base = RD53BTool<RD53BCapMeasureScan, Flavor>;
    using Base::Base;

    struct CapVoltages
    {
        double VMain[16];
        double VDDAMain[16];
        double VTrim[16];
        double VPara[16];
        double VDDAPara[16];
    };

    using capVoltages = ChipDataMap<RD53BCapMeasureScan::CapVoltages>;
    capVoltages run() const;

    void draw(const capVoltages& results) const;
};

} // namespace RD53BTools

#endif
