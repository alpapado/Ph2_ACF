#include "RD53BThresholdOscillation.h"

#include "Utils/xtensor/xadapt.hpp"
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xrandom.hpp"

#include <TGraph.h>
#include <TMultiGraph.h>
#include <TTree.h>

#include <algorithm>

namespace RD53BTools
{
template <class Flavor>
void RD53BThresholdOscillation<Flavor>::init()
{
    nVcalSteps  = std::ceil((param("vcalRange"_s)[1] - param("vcalRange"_s)[0]) / double(param("vcalStep"_s)));
    nDelaySteps = std::ceil(double(param("maxDelay"_s)) / param("fineDelayStep"_s));
}

template <class Flavor>
auto RD53BThresholdOscillation<Flavor>::run(Task progress) -> Result
{
    auto& chipInterface = *static_cast<RD53BProdInterface<Flavor>*>(Base::system().fReadoutChipInterface);

    ChipDataMap<ChipResults> results;

    auto& injTool = param("injectionTool"_s);

    auto                  vcalBins = xt::arange(param("vcalRange"_s)[0], param("vcalRange"_s)[1], param("vcalStep"_s));
    AsyncWorkerPool<void> analyzer_pool(param("analyzerThreads"_s));
    ThresholdEstimator    thresholdEstimator(
        vcalBins, injTool.param("nInjections"_s), injTool.param("triggerDuration"_s), param("epsilon"_s), {injTool.size(0), injTool.size(1)}, {injTool.offset(0), injTool.offset(1)});

    if(param("storeHits"_s))
    {
        Base::createOutputDirectory();
        Base::createRootFile();
        Base::for_each_chip([&](Chip* chip) {
            Base::createRootFileDirectory(chip);
            hitTrees.insert({chip, new TTree("hits", "Hits")});
        });
    }

    std::vector<std::future<void>> analysisFutures;

    Base::for_each_chip([&](Chip* chip) {
        results.insert({chip,
                        {xt::zeros<size_t>({injTool.param("triggerDuration"_s) * 32, nVcalSteps}),
                         xt::zeros<double>({nDelaySteps, injTool.size(0), injTool.size(1)}),
                         xt::zeros<double>({nDelaySteps, injTool.size(0), injTool.size(1)})}});
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
    });

    size_t lastCoarseDelay = 0;
    bool   digitalScan     = injTool.param("injectionType"_s) == "digital";
    injTool.injectionScan(progress,
                          makeScan(ScanRange(nDelaySteps,
                                             [&, this](auto i) {
                                                 size_t totalDelay  = i * param("fineDelayStep"_s);
                                                 size_t fineDelay   = totalDelay % param("maxFineDelay"_s);
                                                 size_t coarseDelay = (totalDelay - fineDelay) / 8;
                                                 Base::for_each_hybrid([&](Hybrid* hybrid) { chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", fineDelay); });
                                                 if(coarseDelay != lastCoarseDelay)
                                                 {
                                                     injTool.param("injectionDelay160MHz"_s) = coarseDelay;
                                                     injTool.configureInjections();
                                                     lastCoarseDelay = coarseDelay;
                                                 }
                                             }),
                                   ScanRange(nVcalSteps,
                                             [&, this](auto i) {
                                                 Base::for_each_hybrid([&](Hybrid* hybrid) {
                                                     if(digitalScan)
                                                     {
                                                         injTool.param("pulseDuration"_s) = param("vcalRange"_s)[0] + param("vcalStep"_s) * i;
                                                         injTool.configureInjections();
                                                     }
                                                     else
                                                         chipInterface.WriteReg(hybrid, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + param("vcalRange"_s)[0] + param("vcalStep"_s) * i);
                                                 });
                                             })),
                          [&, this](auto i, auto&& events) {
                              size_t frameId = i / nDelaySteps;
                              size_t delayId = i % nDelaySteps;

                              auto usedPixels = param("injectionTool"_s).generateInjectionMask(frameId);
                              Base::for_each_chip([&](auto chip) {
                                  if(param("storeHits"_s))
                                  {
                                      Base::createRootFileDirectory(chip);

                                      uint16_t charge;
                                      uint16_t delay;
                                      uint16_t row;
                                      uint16_t col;
                                      uint16_t tot;
                                      uint16_t trigger_id;

                                      auto& hitsTree = hitTrees[chip];
                                      hitsTree->Branch("charge", &charge);
                                      hitsTree->Branch("delay", &delay);
                                      hitsTree->Branch("row", &row);
                                      hitsTree->Branch("col", &col);
                                      hitsTree->Branch("tot", &tot);
                                      hitsTree->Branch("trigger_id", &trigger_id);

                                      for(size_t j = 0; j < nVcalSteps; ++j)
                                      {
                                          for(const auto& event: xt::view(events[chip], j, xt::all()))
                                          {
                                              for(const auto& hit: event.hits)
                                              {
                                                  charge     = param("vcalRange"_s)[0] + j * param("vcalStep"_s);
                                                  delay      = delayId * param("fineDelayStep"_s);
                                                  row        = hit.row;
                                                  col        = hit.col;
                                                  tot        = hit.tot;
                                                  trigger_id = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                                                  hitsTree->Fill();
                                              }
                                          }
                                      }
                                  }

                                  for(size_t j = 0; j < nVcalSteps; ++j)
                                  {
                                      for(const auto& event: xt::view(events[chip], j, xt::all()))
                                      {
                                          auto bcid = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                                          results.at(chip).tornado(32 * (bcid + 1) - 1 - ((delayId * param("fineDelayStep"_s)) % 32), j) += event.hits.size();
                                      }
                                  }

                                  analysisFutures.erase(
                                      std::remove_if(analysisFutures.begin(), analysisFutures.end(), [](const auto& f) { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }),
                                      analysisFutures.end());

                                  xt::xtensor<bool, 2> enabled = xt::view(usedPixels && chip->injectablePixels(), injTool.rowRange(), injTool.colRange());

                                  analysisFutures.push_back(analyzer_pool.enqueue_task([&, delayId, chip, enabled = std::move(enabled), events = std::move(events[chip])]() {
                                      ThresholdEstimatorResults thresholdResults{injTool.size(0), injTool.size(1), vcalBins.size()};
                                      thresholdEstimator(events, enabled, thresholdResults);
                                      xt::filter(xt::view(results.at(chip).threshold, delayId, xt::all(), xt::all()), enabled) = xt::filter(thresholdResults.threshold, enabled);
                                      xt::filter(xt::view(results.at(chip).noise, delayId, xt::all(), xt::all()), enabled)     = xt::filter(thresholdResults.noise, enabled);
                                  }));
                              });
                          });

    for(const auto& f: analysisFutures) f.wait();

    analyzer_pool.join();

    if(param("storeHits"_s)) Base::for_each_chip([&](Chip* chip) { hitTrees[chip]->Write(); });

    return results;
}

template <class Flavor>
void RD53BThresholdOscillation<Flavor>::draw(const Result& results)
{
    auto                   usedPixels = param("injectionTool"_s).usedPixels();
    xt::xtensor<double, 1> delayBins  = 25. / 32. * xt::arange(0u, param("maxDelay"_s), param("fineDelayStep"_s));

    Base::createOutputDirectory();
    Base::createRootFile();

    Base::for_each_chip([&](auto* chip) {
        auto dir = Base::createRootFileDirectory(chip);

        auto enabled = xt::view(usedPixels && chip->injectablePixels(), param("injectionTool"_s).rowRange(), param("injectionTool"_s).colRange());

        {
            xt::xtensor<double, 2> thresholdVariance = xt::stddev(results.at(chip).threshold, {0});

            Base::drawMap(thresholdVariance, "Threshold Variance Map", "Threshold Variance", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
            auto filteredThresholdVariance = xt::filter(thresholdVariance, enabled);
            Base::drawHist(filteredThresholdVariance, "Threshold Variance Distribution", 50, 0, 1.1 * xt::amax(filteredThresholdVariance)(), "Threshold Variance", false);
        }

        {
            xt::xtensor<double, 2> thresholdPeakToPeak = xt::amax(results.at(chip).threshold, 0) - xt::amin(results.at(chip).threshold, 0);

            Base::drawMap(thresholdPeakToPeak, "Threshold Peak-to-Peak Map", "Threshold Peak-to-Peak", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
            auto filteredThresholdPeakToPeak = xt::filter(thresholdPeakToPeak, enabled);
            Base::drawHist(filteredThresholdPeakToPeak, "Threshold Peak-to-Peak Distribution", 50, 0, 1.1 * xt::amax(filteredThresholdPeakToPeak)(), "Threshold Peak-to-Peak", false);
        }

        std::vector<double> meanThresholds;
        std::vector<double> sigmaThresholds;
        std::vector<double> meanNoise;
        for(size_t i = 0; i < nDelaySteps; ++i)
        {
            double thr      = xt::mean(xt::filter(xt::view(results.at(chip).threshold, i, xt::all(), xt::all()), enabled))();
            double thrSigma = xt::stddev(xt::filter(xt::view(results.at(chip).threshold, i, xt::all(), xt::all()), enabled))();
            double noise    = xt::mean(xt::filter(xt::view(results.at(chip).noise, i, xt::all(), xt::all()), enabled))();
            meanThresholds.push_back(thr);
            sigmaThresholds.push_back(thrSigma);
            meanNoise.push_back(noise);
        }

        {
            auto c = new TCanvas("threshold");
            auto g = new TGraph(delayBins.size(), delayBins.data(), meanThresholds.data());
            g->SetTitle("Threshold vs. Injection Delay");
            g->Draw();
            // g->Write();
            c->Write();
        }

        {
            auto c = new TCanvas("threshold dispersion");
            auto g = new TGraph(delayBins.size(), delayBins.data(), sigmaThresholds.data());
            g->SetTitle("Threshold dispersion vs. Injection Delay");
            g->Draw();
            // g->Write();
            c->Write();
        }

        {
            auto c = new TCanvas("noise");
            auto g = new TGraph(delayBins.size(), delayBins.data(), meanNoise.data());
            g->SetTitle("Noise vs. Injection Delay");
            g->Draw();
            // g->Write();
            c->Write();
        }

        Base::drawHist2D(results.at(chip).tornado / (double)param("injectionTool"_s).param("nInjections"_s) / xt::count_nonzero(enabled),
                         "Tornado plot",
                         0,
                         param("injectionTool"_s).param("triggerDuration"_s) * 25,
                         param("vcalRange"_s)[0],
                         param("vcalRange"_s)[1],
                         "Detection Delay (triggerId * 32 - fineDelay) [ns]",
                         "Delta VCAL",
                         "Occupancy");

        xt::xtensor<double, 3> threshold = xt::transpose(results.at(chip).threshold, {1, 2, 0});

        auto rowRange = param("injectionTool"_s).rowRange();
        auto colRange = param("injectionTool"_s).colRange();

        auto pixelPositions = xt::argwhere(xt::view(usedPixels && chip->injectablePixels(), rowRange, colRange));

        auto mg = new TMultiGraph();

        dir->mkdir("SinglePixels")->cd();
        // for (const auto& idx : xt::random::choice(xt::adapt(xt::argwhere(enabled)), 100)) {
        xt::xtensor<size_t, 1> indices;
        if(param("pickRandomPixels"_s))
            indices = xt::view(xt::random::permutation(pixelPositions.size()), xt::range(0, param("maxNPlots"_s)));
        else
            indices = xt::arange(std::min(pixelPositions.size(), param("maxNPlots"_s)));
        for(auto idx: indices)
        {
            auto        pixelPos = pixelPositions[idx];
            std::string name     = "threshold (" + std::to_string(param("injectionTool"_s).offset(0) + pixelPos[0]) + ", " + std::to_string(param("injectionTool"_s).offset(1) + pixelPos[1]) + ')';
            auto        g        = new TGraph(delayBins.size(), delayBins.data(), &threshold(pixelPos[0], pixelPos[1], 0));
            g->SetName(name.c_str());
            g->SetTitle("Threshold vs. Injection Delay");
            g->Draw();
            g->Write();
            mg->Add(g);
        }

        dir->cd();
        auto c = new TCanvas("threshold single pixels");
        mg->SetName("threshold single pixels");
        mg->SetTitle("Threshold vs. Injection Delay");
        mg->Draw();

        c->Write();
    });
}

template class RD53BThresholdOscillation<RD53BFlavor::ATLAS>;
template class RD53BThresholdOscillation<RD53BFlavor::CMS>;
template class RD53BThresholdOscillation<RD53BFlavor::ITkPixV2>;
template class RD53BThresholdOscillation<RD53BFlavor::CROCv2>;

} // namespace RD53BTools