#include "RD53BNoiseScan.h"

#include "Utils/xtensor/xindex_view.hpp"
#include "Utils/xtensor/xio.hpp"
#include "Utils/xtensor/xrandom.hpp"
#include "Utils/xtensor/xview.hpp"

#include <TCanvas.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TTree.h>

#include <experimental/array>

namespace RD53BTools
{
template <class Flavor>
void RD53BNoiseScan<Flavor>::init()
{
    if(param("readoutPeriod"_s) == 0) param("readoutPeriod"_s) = param("nTriggers"_s);

    _nRounds = std::ceil(param("nTriggers"_s) / double(param("readoutPeriod"_s) * param("injectionTool"_s).param("triggerDuration"_s)));

    _nReduced = param("nTriggers"_s) % _nRounds;
    if(_nReduced) _nReduced = _nRounds - _nReduced;

    param("injectionTool"_s).param("nInjections"_s)       = param("readoutPeriod"_s);
    param("injectionTool"_s).param("injectionType"_s)     = "None";
    param("injectionTool"_s).param("delayAfterTrigger"_s) = param("triggerPeriod"_s);
}

template <class Flavor>
ChipDataMap<typename RD53BNoiseScan<Flavor>::ChipResult> RD53BNoiseScan<Flavor>::run(Task progress)
{
    auto&                   chipInterface = Base::chipInterface();
    ChipDataMap<ChipResult> results;

    auto usedPixels = param("injectionTool"_s).usedPixels();

    Base::for_each_chip([&](auto* chip) {
        results.insert({chip,
                        {usedPixels && chip->pixelConfig().enable,
                         // xt::zeros_like(usedPixels),
                         xt::zeros<size_t>({Flavor::nRows, Flavor::nCols, param("injectionTool"_s).param("triggerDuration"_s)}),
                         xt::zeros<size_t>({Flavor::nRows, Flavor::nCols, param("injectionTool"_s).param("triggerDuration"_s), 16ul})}});
    });

    if(param("storeHits"_s))
    {
        Base::createOutputDirectory();
        Base::createRootFile();
        Base::for_each_chip([&](Chip* chip) {
            Base::createRootFileDirectory(chip);
            hitTrees.insert({chip, new TTree("hits", "Hits")});
        });
    }

    param("injectionTool"_s).injectionScan(progress, std::experimental::make_array(ScanRange(_nRounds, [&](auto i) {})), [&, this](size_t i, auto&& events) {
        Base::for_each_chip([&](auto chip) {
            auto& chipEvents  = events[chip];
            auto& chipResults = results[chip];
            if(param("storeHits"_s))
            {
                Base::createRootFileDirectory(chip);

                uint16_t row;
                uint16_t col;
                uint16_t tot;
                uint16_t trigger_id;
                uint32_t event_id = 0;

                auto& hitsTree = hitTrees[chip];
                hitsTree->Branch("row", &row);
                hitsTree->Branch("col", &col);
                hitsTree->Branch("tot", &tot);
                hitsTree->Branch("trigger_id", &trigger_id);
                hitsTree->Branch("event_id", &event_id);

                // for (const auto& event : events[chip]) {
                for(auto round = 0u; round < _nRounds; ++round)
                {
                    size_t nInjectionsThisRound = round < _nReduced ? param("readoutPeriod"_s) - 1 : param("readoutPeriod"_s);
                    for(auto j = 0u; j < nInjectionsThisRound; ++j)
                    {
                        for(const auto& hit: chipEvents(round, j).hits)
                        {
                            row        = hit.row;
                            col        = hit.col;
                            tot        = hit.tot;
                            trigger_id = chipEvents(round, j).triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                            hitsTree->Fill();
                        }
                        ++event_id;
                    }
                }
            }
            for(auto round = 0u; round < _nRounds; ++round)
            {
                size_t nInjectionsThisRound = round < _nReduced ? param("readoutPeriod"_s) - 1 : param("readoutPeriod"_s);
                for(auto j = 0u; j < nInjectionsThisRound; ++j)
                {
                    auto triggerId = chipEvents(round, j).triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                    for(const auto& hit: chipEvents(round, j).hits)
                    {
                        ++chipResults.hitCount(hit.row, hit.col, triggerId);
                        ++chipResults.ToTHistogram(hit.row, hit.col, triggerId, hit.tot);
                    }
                }
            }
        });
    });

    if(param("maskNoisyPixels"_s))
        Base::for_each_chip([&](auto chip) {
            const auto noisy = xt::sum(results.at(chip).hitCount, 2) / double(param("nTriggers"_s)) > param("occupancyThreshold"_s);
            LOG(INFO) << "Masking " << xt::count_nonzero(chip->pixelConfig().enable && noisy) << " noisy pixels for chip: " << ChipLocation(chip) << RESET;
            xt::filter(chip->pixelConfig().enable, noisy).fill(false);
        });

    // reset pixel config
    Base::for_each_chip([&](Chip* chip) { chipInterface.UpdatePixelConfig(chip, true, false); });

    return results;
}

template <class Flavor>
void RD53BNoiseScan<Flavor>::draw(const ChipDataMap<ChipResult>& result)
{
    Base::createRootFile();

    auto triggerDuration = param("injectionTool"_s).param("triggerDuration"_s);

    Base::for_each_chip([&](auto chip) {
        auto dir = Base::createRootFileDirectory(chip);

        xt::xtensor<size_t, 2> hitCount = xt::sum(result.at(chip).hitCount, 2);

        const auto& enabled = result.at(chip).enabled;

        auto enabledIdx = xt::nonzero(enabled);

        xt::xtensor<double, 3> totHistogram = xt::sum(result.at(chip).ToTHistogram, 2);

        xt::xtensor<double, 1> totHistogramEnabled = xt::sum(xt::where(xt::expand_dims(enabled, 2), totHistogram, 0), {0, 1});

        Base::drawHistRaw(totHistogramEnabled, "ToT Histogram", 0, 16, "ToT", "Count");

        Base::drawMap(hitCount, "Hit Count Map", "Hit Count");

        size_t maxHits = xt::amax(hitCount)();

        Base::drawHist(xt::filter(hitCount, enabled), "Hit Count Distribution", 1.1 * maxHits, 0, 1.1 * maxHits, "Hit Count", false);

        if(triggerDuration > 1)
            Base::drawHistRaw(xt::sum(xt::where(xt::expand_dims(enabled, 2), result.at(chip).hitCount, 0), {0, 1}), "Hit Count vs. Trigger ID", 0, triggerDuration, "Trigger ID", "Hit Count");

        dir->mkdir("ToT Histograms")->cd();
        for(auto i: xt::view(xt::random::permutation(enabledIdx[0].size()), xt::range(0, 100)))
        {
            auto row  = enabledIdx[0][i];
            auto col  = enabledIdx[1][i];
            auto name = std::to_string(row) + ", " + std::to_string(col);
            Base::drawHistRaw(xt::view(totHistogram, row, col, xt::all()), "ToT Histogram (" + name + ")", 0, 16, "ToT", "Count");
        }

        size_t nHits = xt::sum(hitCount)();

        LOG(INFO) << "Enabled pixels: " << xt::count_nonzero(enabled)() << " / " << (Flavor::nRows * Flavor::nCols) << RESET;

        LOG(INFO) << "Total number of hits: " << nHits << RESET;

        double meanHitCountEnabled = xt::mean(xt::filter(hitCount, enabled))();
        auto   noisy               = hitCount / double(param("nTriggers"_s)) > param("occupancyThreshold"_s);
        double meanHitCountNoisy   = xt::mean(xt::filter(hitCount, enabled && noisy))();
        double meanHitCountQuiet   = xt::mean(xt::filter(hitCount, enabled && !noisy))();

        // auto noisyIdx = xt::nonzero(enabled & noisy);
        // auto totHistogramNoisy = xt::sum(xt::view(totHistogram, xt::keep(noisyIdx[0]), xt::keep(noisyIdx[1]), xt::all()), {0, 1});
        xt::xtensor<double, 3> a                 = xt::where(xt::expand_dims(enabled & noisy, 2), totHistogram, 0);
        xt::xtensor<double, 1> totHistogramNoisy = xt::sum(a, {0, 1});

        // auto quietIdx = xt::nonzero(enabled & !noisy);
        // auto totHistogramQuiet = xt::sum(xt::view(totHistogram, xt::keep(quietIdx[0]), xt::keep(quietIdx[1]), xt::all()), {0, 1});
        xt::xtensor<double, 3> b                 = xt::where(xt::expand_dims(enabled & !noisy, 2), totHistogram, 0);
        xt::xtensor<double, 1> totHistogramQuiet = xt::sum(b, {0, 1});

        LOG(INFO) << "|" << std::setw(16) << "Pixel Group"
                  << "|" << std::setw(16) << "Pixel Count"
                  << "|" << std::setw(16) << "Mean Hit Count"
                  << "|" << std::setw(16) << "Mean Occupancy"
                  << "|" << std::setw(16) << "Mean ToT";

        LOG(INFO) << "|" << std::setw(16) << "All"
                  << "|" << std::setw(16) << xt::count_nonzero(enabled)() << "|" << std::setw(16) << meanHitCountEnabled << "|" << std::setw(16) << std::scientific
                  << (meanHitCountEnabled / param("nTriggers"_s)) << std::defaultfloat << "|" << std::setw(16) << xt::average<double>(xt::arange(16), totHistogramEnabled)();

        LOG(INFO) << "|" << std::setw(16) << "Noisy"
                  << "|" << std::setw(16) << xt::count_nonzero(enabled && noisy)() << "|" << std::setw(16) << meanHitCountNoisy << "|" << std::setw(16) << std::scientific
                  << (meanHitCountNoisy / param("nTriggers"_s)) << std::defaultfloat << "|" << std::setw(16) << xt::average<double>(xt::arange(16), totHistogramNoisy)();

        LOG(INFO) << "|" << std::setw(16) << "Quiet"
                  << "|" << std::setw(16) << xt::count_nonzero(enabled && !noisy)() << "|" << std::setw(16) << meanHitCountQuiet << "|" << std::setw(16) << std::scientific
                  << (meanHitCountQuiet / param("nTriggers"_s)) << std::defaultfloat << "|" << std::setw(16) << xt::average<double>(xt::arange(16), totHistogramQuiet)();
    });
}

template class RD53BNoiseScan<RD53BFlavor::ATLAS>;
template class RD53BNoiseScan<RD53BFlavor::CMS>;
template class RD53BNoiseScan<RD53BFlavor::ITkPixV2>;
template class RD53BNoiseScan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools