#ifndef RD53BCBWAITSCAN_H
#define RD53BCBWAITSCAN_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BCBWaitScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BCBWaitScan<Flavor>> = make_named_tuple(
    std::make_pair("minValue"_s, 0),
    std::make_pair("maxValue"_s, 255),
    std::make_pair("nPoints"_s, 1000),
    std::make_pair("configDelayUs"_s, 5e5),
    std::make_pair("sleepTimeUs"_s, 5000)
);

template <class Flavor>
struct RD53BCBWaitScan : public RD53BTool<RD53BCBWaitScan, Flavor> {
    using Base = RD53BTool<RD53BCBWaitScan, Flavor>;
    using Base::Base;

   bool run(Task progress) const;
    //void draw(const std::stringstream& results) const;
};

}

#endif
