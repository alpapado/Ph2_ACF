
#include <array>
#include <termios.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include <string>
#include <stdexcept>

struct SerialPort {
    static SerialPort open(std::string path, std::string input_terminator, std::string output_terminator) {
        int serial_port = ::open(path.c_str(), O_RDWR);
        struct termios tty;
          // Read in existing settings, and handle any error

        if(tcgetattr(serial_port, &tty) != 0) {
            printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
            throw std::runtime_error("Could not open serial port");
        }

        tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
        tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
        tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
        tty.c_cflag |= CS8; // 8 bits per byte (most common)
        tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
        tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

        tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO; // Disable echo
        tty.c_lflag &= ~ECHOE; // Disable erasure
        tty.c_lflag &= ~ECHONL; // Disable new-line echo
        tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

        tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
        tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
        // tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
        // tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

        tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
        tty.c_cc[VMIN] = 0;

        // Set in/out baud rate to be 9600
        cfsetispeed(&tty, B9600);
        cfsetospeed(&tty, B9600);

        // Save tty settings, also checking for error
        if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
            printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
            throw std::runtime_error("Could not configure serial port");
        }
        return SerialPort{
            std::move(path),
            std::move(input_terminator),
            std::move(output_terminator),
            serial_port
        };  
    }

    ~SerialPort() {
        close(handle);
    }

    void write(const std::string& cmd) {
        std::string msg = cmd + output_terminator;
        ::write(handle, msg.c_str(), msg.size());
    }

    std::string readline() {
        std::string result = leftOverInput;

        auto end = std::string::npos;

        do {
            std::array<char, 256> read_buf;
            
            int num_bytes = read(handle, read_buf.data(), read_buf.size());
            
            if (num_bytes < 0)
                throw std::runtime_error("Error reading serial port.");

            result.insert(result.size(), read_buf.data());

            end = result.find(input_terminator);
        }
        while (end == std::string::npos);

        leftOverInput += result.substr(end);
        return result.substr(0, end);
    }

    std::string path;
    std::string input_terminator;
    std::string output_terminator;
    int handle;
    std::string leftOverInput = "";
};