#include "RD53BScanChainTestWLT.h"

#include <string>
#include <vector>
#include <type_traits>

using std::to_string;

namespace RD53BTools {

template <class Flavor>
bool RD53BScanChainTestWLT<Flavor>::run(Task progress) const {
    Ph2_System::SystemController& system = Base::system();

    //looping on all devices to perform the test
    for_each_device<BeBoard>(system, [&] (BeBoard* board) {
        auto& fwInterface = Base::getFWInterface(board);

        //resetting the state machine
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.reset", 1);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.reset", 0);
        usleep(1e4);

        //////////////////////////////////////////////////
        // Length of scan chains for different chips:
        // CROCv1:   312111/312110
        // ITkPixV2: 300821/300820
        // CROCv2:   317604/317603
        //////////////////////////////////////////////////

        //////////////////////////////////////////////////
        // Number of test vectors for different chips:
        // CROCv1:   2033
        // ITkPixV2: 1450
        // CROCv2:   1971
        //////////////////////////////////////////////////

        //chip-dependent configuration for the scan chain test firmware
        uint16_t nScanSteps = 0u;
        uint32_t vectorLength = 0u;
        uint32_t expVectorLength0 = 0u;
        uint32_t expVectorLength1 = 0u;

        //checking the chip type and setting the chip-dependent configuration
        if (std::is_same<Flavor, RD53BFlavor::CMS>::value == true) {
            LOG(INFO) << BOLDGREEN << "Setting up scan chain test for CROCv1 chip"
                << RESET;
            nScanSteps = 2033u;
            vectorLength = 312128u;
            expVectorLength0 = 312111u;
            expVectorLength1 = 312110u;
        } else if (std::is_same<Flavor, RD53BFlavor::ITkPixV2>::value == true) {
            LOG(INFO) << BOLDGREEN << "Setting up scan chain test for ITkPixV2 chip"
                << RESET;
            nScanSteps = 1450u;
            vectorLength = 300832u; // vector length divided by 32, rounded up and multiplied by 32
            expVectorLength0 = 300821u;
            expVectorLength1 = 300820u;
        } else if (std::is_same<Flavor, RD53BFlavor::CROCv2>::value == true) {
            LOG(INFO) << BOLDGREEN << "Setting up scan chain test for CROCv2 chip" << RESET;
            nScanSteps = 1971u;
            vectorLength = 317632u;
            expVectorLength0 = 317604u;
            expVectorLength1 = 317603u;
        } else {
            LOG(INFO) << BOLDRED << "Could not set up scan chain test!" << RESET;
            throw std::runtime_error("Invalid chip type for scan chain test!");
        }

        //logging the chip-dependent configuration
        LOG(INFO) << BOLDYELLOW << "Number of steps: " << to_string(nScanSteps) << RESET;
        LOG(INFO) << BOLDYELLOW << "Vector length: " << to_string(vectorLength) << RESET;
        LOG(INFO) << BOLDYELLOW << "Expected length, chain 0: " << to_string(expVectorLength0) << RESET;
        LOG(INFO) << BOLDYELLOW << "Expected length, chain 1: " << to_string(expVectorLength1) << RESET;

        //configuring the scan chain test firmware depending on the chip type
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.sampling_clk_phase", 0);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.invert_source_clk", 0);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain_pi_delay", 2);
        fwInterface.WriteReg("user.ctrl_regs.scan_steps", nScanSteps);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.enable_tests", 1);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.keep_pi_values", 0);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain_vector_length", vectorLength);
        fwInterface.WriteReg("user.ctrl_regs.scan_elength0", expVectorLength0);
        fwInterface.WriteReg("user.ctrl_regs.scan_elength1", expVectorLength1);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.offset", 0);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.readout_en", 1);

        //starting the test
        LOG(INFO) << BOLDGREEN << "Starting the test" << RESET;
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.load_unload", 1);
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.load_unload", 0);

        //waiting for the completion of the test
        uint32_t nErrs = 0;
        uint32_t patternCompleted = 0;
        uint32_t errorFlag = 0;
        while (true) {
            nErrs = fwInterface.ReadReg("user.stat_regs.scan_chain.error_count");
            patternCompleted = fwInterface.ReadReg("user.stat_regs.scan_chain.pattern_completed");
            errorFlag = fwInterface.ReadReg("user.stat_regs.scan_chain.error_flag");
            if ((nErrs == 10) || (patternCompleted == 1)) {
                break;
            }
            usleep(100);
        }

        //resetting the multiplexer for the selection between data merging and scan chain tests
        fwInterface.WriteReg("user.ctrl_regs.scan_chain.enable_tests", 0);

        //check if there has been a firmware error
        if (errorFlag != 0) {
            LOG(INFO) << BOLDRED << "Scan chain firmware error!" << RESET;
        }

        //logging the status
        if (nErrs == 0) {
            LOG(INFO) << BOLDGREEN << "Scan chain test completed successfully" << RESET;
            LOG(INFO) << BOLDGREEN << "Number of errors: 0" << RESET;
        } else {
            LOG(INFO) << BOLDRED << "Scan chain test failed!" << RESET;
            LOG(INFO) << BOLDRED << "Number of errors: " << to_string(nErrs) << RESET;
        }

        //getting the results of the test
        for (uint32_t i = 0; i < nErrs; i++) {
            //reading the registers
            uint32_t errType = fwInterface.ReadReg("user.stat_regs.scan_errors" + to_string(i) + ".type");
            uint32_t errBit = fwInterface.ReadReg("user.stat_regs.scan_errors" + to_string(i) + ".bit");
            uint32_t errStep = fwInterface.ReadReg("user.stat_regs.scan_errors" + to_string(i) + ".step");
            LOG(INFO) << BOLDRED
                << "Number: " << to_string(i)
                << "; Type: " << errorTypeMap.at(errType)
                << "; Step: " << to_string(errStep)
                << "; Bit: " << to_string(errBit)
                << RESET;
        }

    });

   return true;

}

template class RD53BScanChainTestWLT<RD53BFlavor::ATLAS>;
template class RD53BScanChainTestWLT<RD53BFlavor::CMS>;
template class RD53BScanChainTestWLT<RD53BFlavor::ITkPixV2>;
template class RD53BScanChainTestWLT<RD53BFlavor::CROCv2>;

}
