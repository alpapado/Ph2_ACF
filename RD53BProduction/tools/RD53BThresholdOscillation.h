#ifndef RD53BThresholdOscillation_H
#define RD53BThresholdOscillation_H

#include "RD53BInjectionTool.h"

#include "Utils/ThresholdEstimator.h"

class TTree;

namespace RD53BTools
{
template <class>
struct RD53BThresholdOscillation; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BThresholdOscillation<Flavor>> = make_named_tuple(std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
                                                                                std::make_pair("vcalMed"_s, 300u),
                                                                                std::make_pair("vcalRange"_s, std::vector<size_t>({200, 800})),
                                                                                std::make_pair("vcalStep"_s, 20),
                                                                                std::make_pair("maxDelay"_s, 32u),
                                                                                std::make_pair("maxFineDelay"_s, 32u),
                                                                                std::make_pair("fineDelayStep"_s, 1u),
                                                                                std::make_pair("storeHits"_s, false),
                                                                                std::make_pair("analyzerThreads"_s, 1u),
                                                                                std::make_pair("epsilon"_s, 1e-8),
                                                                                std::make_pair("maxNPlots"_s, 100ul),
                                                                                std::make_pair("pickRandomPixels"_s, true));

template <class Flavor>
struct RD53BThresholdOscillation : public RD53BTool<RD53BThresholdOscillation, Flavor>
{
    using Base = RD53BTool<RD53BThresholdOscillation, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResults
    {
        // xt::xarray<RD53BEventDecoding::RD53BEvent> events;
        xt::xtensor<size_t, 2> tornado;
        xt::xtensor<double, 3> threshold;
        xt::xtensor<double, 3> noise;
    };

    using Result = ChipDataMap<ChipResults>;

    void   init();
    Result run(Task progress);
    void   draw(const Result& lateHitRatio);

  private:
    ChipDataMap<TTree*> hitTrees;
    size_t              nVcalSteps;
    size_t              nDelaySteps;
};

} // namespace RD53BTools

#endif