#ifndef RD53BSCANCHAINTESTWLT_H
#define RD53BSCANCHAINTESTWLT_H

#include <map>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BScanChainTestWLT; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BScanChainTestWLT<Flavor>> = make_named_tuple();

template <class Flavor>
struct RD53BScanChainTestWLT : public RD53BTool<RD53BScanChainTestWLT, Flavor> {
    using Base = RD53BTool<RD53BScanChainTestWLT, Flavor>;
    using Base::Base;

    const std::map<uint32_t, std::string> errorTypeMap = {
        {0, "Scan out 0"},
        {1, "Scan out 1"},
        {2, "Multi-clock capture 0"},
        {3, "Multi-clock capture 1"},
    };

    bool run(Task progress) const;

};

}

#endif
