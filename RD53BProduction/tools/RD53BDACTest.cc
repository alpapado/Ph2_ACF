#include "RD53BDACTest.h"

#include <array>
#include <map>
#include <set>
#include <string>

using Ph2_HwDescription::RD53BFlavor::ATLAS;
using Ph2_HwDescription::RD53BFlavor::CMS;
using Ph2_HwDescription::RD53BFlavor::ITkPixV2;
using Ph2_HwDescription::RD53BProdConstants::Register;
using RD53BTools::RD53BDACTest;

template <class F>
typename RD53BDACTest<F>::Results RD53BDACTest<F>::run() {
    Results res;

    Base::for_each_chip([this, &res] (Chip* chip) {
        using RD53BProdConstants::Register;
        RD53BProdInterface<F>& chipIface = Base::chipInterface();

        // set TDAC = 0 across whole matrix -> minimum effect of LDAC on IANA
        // also disable digital output while fiddling with FE params
        chipIface.UpdatePixelConfigUniform(chip, 0, 0, false, true);

        std::map<std::reference_wrapper<const Register>, uint16_t> original, replacement;
        int stepsCompleted = 0;
        for (const std::vector<std::reference_wrapper<const Register>> &v : calibSeq)
        {
            original.clear();
            replacement.clear();
            /* first only gather changes, do not apply them */
            /* the same register can be visited multiple times */
            for (const Register &reg : v)
            {
                const std::string &func = regToFunc.at(&reg);
                const std::map<std::string, uint16_t> &cond = std::get<2>(testConds.at(func));
                for (const std::pair<const Register* const, std::string>& p: regToFunc)
                {
                    std::map<std::string, uint16_t>::const_iterator it = cond.find(p.second);
                    if (it != cond.end()) { replacement[*p.first] = it->second; }
                }
            }
            /* store the original register values, then replace them */
            for (const std::pair<const std::reference_wrapper<const Register>, uint16_t> &p : replacement)
            {
                original[p.first] = chipIface.ReadReg(chip, p.first);
                chipIface.WriteReg(chip, p.first, p.second);
            }
            /* all prepared, calibration can proceed */
            this->calibChip(chip, res[chip], v);
            /* restore initial state */
            for (const auto &p : original)
            {
                chipIface.WriteReg(chip, p.first, p.second);
            }
            ++stepsCompleted;
            LOG(INFO) << "Step " << stepsCompleted << " done." << RESET;
        }
        /* VCAL_* calibration */
        chipIface.WriteReg(chip, "SEL_CAL_RANGE", 1);
        this->calibChip(chip, res[chip], std::vector<std::reference_wrapper<const Register>>{F::Reg::VCAL_HIGH, F::Reg::VCAL_MED});
        ++stepsCompleted;
        LOG(INFO) << "Step " << stepsCompleted << " done." << RESET;

        chipIface.WriteReg(chip, "SEL_CAL_RANGE", 0);
        // for the high-res VCAL_*, store the results in a temporary location
        // otherwise, you overwrite data for the low-res VCAL_*
        // change the name of the registers before merging to avoid overwriting
        std::map<RD53BProdConstants::Register, DACData, MyComp> tmp;
        this->calibChip(chip, tmp, std::vector<std::reference_wrapper<const Register>>{F::Reg::VCAL_HIGH, F::Reg::VCAL_MED});
        for (const std::pair<const Register, DACData>& p : tmp)
        {
            Register reg = p.first;
            reg.name.append("_half");
            res[chip][reg] = p.second;
        }
        ++stepsCompleted;
        LOG(INFO) << "Step " << stepsCompleted << " done." << RESET;
    });
    return res;
}

template <class F>
void RD53BDACTest<F>::draw(const Results& results)
{
    std::ofstream json(Base::getOutputFilePath("results.json"));
    // TFile f(Base::getAvailablePath(".root").c_str(), "RECREATE");
    Base::createRootFile();

    TF1 line("line", "[offset]+[slope]*x");

    const char* str = "{";
    json << std::scientific << "{\"chips\":[";
    for (const std::pair<const ChipLocation, std::map<Register, DACData, MyComp>>& chipRes : results) {
        const ChipLocation& chip = chipRes.first;

        json << str;
        str = ",{";

        json << "\"board\":" << chip.board_id << ","
             << "\"hybrid\":" << chip.hybrid_id << ","
             << "\"id\":" << chip.chip_lane;

        Base::createRootFileDirectory(chip);
        // f.mkdir(("board_" + std::to_string(chip.board_id)).c_str(), "", true)
        // ->mkdir(("hybrid_" + std::to_string(chip.hybrid_id)).c_str(), "", true)
        // ->mkdir(("chip_"+ std::to_string(chip.chip_id)).c_str(), "", true)->cd();

        for(const std::pair<const Register, DACData>& p: chipRes.second)
        {
            const Register& reg  = p.first;
            const DACData&  data = p.second;

            float m = (data.volt.back() - data.volt.front()) / (data.code.back() - data.code.front());
            float q = data.volt.front() - m * data.code.front();

            TGraph* g;
            if(data.voltErr.empty())
                g = new TGraph(data.code.size(), data.code.data(), data.volt.data());
            else
                g = new TGraphErrors(data.code.size(), data.code.data(), data.volt.data(), nullptr, data.voltErr.data());
            g->SetNameTitle(reg.name.c_str());
            g->GetXaxis()->SetTitle("DAC code [DAC LSB]");
            if(isCurr(reg)) { g->GetYaxis()->SetTitle("Output current [A]"); }
            else
            {
                g->GetYaxis()->SetTitle("Output voltage [V]");
            }
            line.SetParameter("offset", q);
            line.SetParameter("slope", m);
            using namespace Ph2_HwDescription::RD53BFlavor;
            using namespace Ph2_HwDescription::RD53BProdConstants;
            if(p.first == F::Reg::VCAL_HIGH || p.first == F::Reg::VCAL_MED) { g->Fit(&line, "Q"); }
            else
            {
                bool  found = false;
                float min = 0;
                float max = 0;
                float lim = param("fitThresholdV"_s) / (isCurr(reg) ? imuxR : 1);
                for(std::vector<float>::size_type i = 0; i < data.code.size(); ++i)
                {
                    if(data.volt[i] < lim)
                    {
                        if(!found)
                        {
                            min   = data.code[i];
                            max   = data.code[i];
                            found = true;
                        }
                        else
                        {
                            if(data.code[i] < min) min = data.code[i];
                            if(data.code[i] > max) max = data.code[i];
                        }
                    }
                }
                if(found) { g->Fit(&line, "Q", "", min, max); }
            }
            g->Write();

            for(int i = 0; i < g->GetN(); ++i) { g->SetPointY(i, g->GetPointY(i) - line.GetParameter("offset") - line.GetParameter("slope") * g->GetPointX(i)); }
            g->SetNameTitle((reg.name + "_diff").c_str());
            if(isCurr(reg)) { g->GetYaxis()->SetTitle("Residual (output current) [A]"); }
            else
            {
                g->GetYaxis()->SetTitle("Residual (output voltage) [V]");
            }
            g->Write();
            delete g;

            json << ",\"" << reg.name << "\":{"
                 << "\"fitted_line\":{\"intercept\":" << line.GetParameter("offset") << ",\"slope\":" << line.GetParameter("slope") << "},";
            if(isCurr(reg))
                json << "\"currents\":{";
            else
                json << "\"voltages\":{";
            for(size_t i = 0; i < data.code.size(); ++i)
            {
                json << "\"" << static_cast<uint16_t>(data.code[i]) << "\":" << data.volt[i];
                if(i < data.code.size() - 1) json << ",";
            }
            json << "}";
            if(!data.voltErr.empty())
            {
                json << ",\"errors\":[";
                for(size_t i = 0; i < data.code.size(); ++i)
                {
                    json << data.voltErr[i];
                    if(i < data.code.size() - 1) json << ",";
                }
                json << "]";
            }
            json << "}"; // end of one DAC data

            /* determine best value for tuning */
            if(param("target"_s).count(reg.name) == 0) {
                continue;
            }
            float target = param("target"_s).at(reg.name);
            float best = -1;
            if(reg == F::Reg::VCAL_HIGH || reg == F::Reg::VCAL_MED || isCurr(reg)) { // <- linear DACs
                // just use the fit result
                best = (target - line.GetParameter("offset")) / line.GetParameter("slope");
            }
            else { // <- DACs with a non-linear characteristic
                // find measured points with voltage immediately above and below target
                unsigned iLo = data.volt.size();
                unsigned iHi = data.volt.size();
                for(unsigned i = 0; i < data.volt.size(); ++i) {
                    if(data.volt[i] < target && (iLo == data.volt.size() || data.volt[iLo] < data.volt[i])) {
                        iLo = i;
                    }
                    if(data.volt[i] > target && (iHi == data.volt.size() || data.volt[i] < data.volt[iHi])) {
                        iHi = i;
                    }
                }
                // if points were found, interpolate
                if(iLo != data.volt.size() && iHi != data.volt.size()) {
                    best = (target - data.volt[iLo]) / (data.volt[iHi] - data.volt[iLo]) * (data.code[iHi] - data.code[iLo]) + data.code[iLo];
                }
            }
            LOG(INFO) << "Best value for " << reg.name << " is " << static_cast<int>(std::round(best));
        }
        json << "}"; // end of one chip data
    }
    json << "]}";
}

template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::ATLAS), bool> = true>
const typename RD53BDACTest<F>::calibSeqType calibSeq()
{
    static std::vector<std::vector<std::reference_wrapper<const Register>>> a = {
        {
            F::Reg::DAC_PREAMP_M_DIFF,
        },
        {
            F::Reg::DAC_PREAMP_L_DIFF,
            F::Reg::DAC_PREAMP_R_DIFF,
            F::Reg::DAC_PREAMP_TL_DIFF,
            F::Reg::DAC_PREAMP_TR_DIFF,
            F::Reg::DAC_PREAMP_T_DIFF,
        },
        {
            F::Reg::DAC_PRECOMP_DIFF,
        },
        {
            F::Reg::DAC_COMP_DIFF,
        },
        {
            F::Reg::DAC_VFF_DIFF,
        },
        {
            F::Reg::DAC_TH1_L_DIFF,
            F::Reg::DAC_TH1_R_DIFF,
            F::Reg::DAC_TH1_M_DIFF,
        },
        {
            F::Reg::DAC_TH2_DIFF,
        },
        {
            F::Reg::DAC_LCC_DIFF,
        },
    };
    return a;
}

template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::CMS), bool> = true>
const typename RD53BDACTest<F>::calibSeqType calibSeq()
{
    /* TODO sort registers */
    static std::vector<std::vector<std::reference_wrapper<const Register>>> a = {
        {
            F::Reg::DAC_KRUM_CURR_LIN,
            F::Reg::DAC_COMP_TA_LIN,
            F::Reg::DAC_PREAMP_TL_LIN,
            F::Reg::DAC_PREAMP_TR_LIN,
            F::Reg::DAC_PREAMP_L_LIN,
            F::Reg::DAC_PREAMP_R_LIN,
            F::Reg::DAC_PREAMP_T_LIN,
            F::Reg::DAC_FC_LIN,
            F::Reg::DAC_REF_KRUM_LIN,
            /* LDAC has to stay last for test conditions precedence */
            F::Reg::DAC_LDAC_LIN,
        },
        {
            F::Reg::DAC_COMP_LIN,
        },
        {
            F::Reg::DAC_PREAMP_M_LIN,
        },
        {
            F::Reg::DAC_GDAC_L_LIN,
            F::Reg::DAC_GDAC_R_LIN,
            F::Reg::DAC_GDAC_M_LIN,
        },
    };
    return a;
}

template <class Flavor>
const typename RD53BDACTest<Flavor>::calibSeqType RD53BDACTest<Flavor>::calibSeq = ::calibSeq<Flavor>();


template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::ATLAS), bool> = true>
const typename RD53BDACTest<F>::testCondsType testConditions()
{
    static const std::map<std::string, std::tuple<uint16_t, uint16_t, std::map<std::string, uint16_t>>> a = {
        {
            "PA_IN_BIAS_DIFF",
            {795, 995, {}},
        },
        {
            "PRECOMP_DIFF",
            {250, 450, {}},
        },
        {
            "COMP_DIFF",
            {423, 623, {}},
        },
        {
            "VFF_DIFF",
            {60, 260, {}},
        },
        {
            "TH1_DIFF",
            {160, 360, {}},
        },
        {
            "TH2_DIFF",
            {0, 100, {}},
        },
        {
            "LCC_DIFF",
            {100, 300, {}},
        },
    };
    return a;
}

template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::CMS), bool> = true>
const typename RD53BDACTest<F>::testCondsType testConditions()
{
    static const std::map<std::string, std::tuple<uint16_t, uint16_t, std::map<std::string, uint16_t>>> a = {
        {
            "FC_BIAS_LIN",
            { 10, 70, {} },
        },
        {
            "Vthreshold_LIN",
            { 300, 600, {{"REF_KRUM_LIN",300}} },
        },
        {
            "COMP_LIN",
            { 70, 250, {{"LDAC_LIN",80},{"PA_IN_BIAS_LIN",100}} },
        },
        {
            "COMP_STAR_LIN",
            { 50, 300, {} },
        },
        {
            "LDAC_LIN",
            { 80, 500, {{"COMP_LIN",250}} },
        },
        {
            "KRUM_CURR_LIN",
            { 5, 200, {} },
        },
        {
            "PA_IN_BIAS_LIN",
            { 100, 500, {{"PA_IN_BIAS_LIN",100},{"COMP_LIN",70}} },
        },
        {
            "REF_KRUM_LIN",
            { 300, 450, {{"Vthreshold_LIN",900}} },
        },
    };
    return a;
}

template <class Flavor>
const typename RD53BDACTest<Flavor>::testCondsType RD53BDACTest<Flavor>::testConds = testConditions<Flavor>();


template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::ATLAS), bool> = true>
const typename RD53BDACTest<F>::regToFuncType& regToFunc()
{
    static std::map<const Register*, std::string> a = {
        {&F::Reg::DAC_PREAMP_L_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PREAMP_R_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PREAMP_TL_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PREAMP_TR_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PREAMP_T_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PREAMP_M_DIFF, "PA_IN_BIAS_DIFF"},
        {&F::Reg::DAC_PRECOMP_DIFF, "PRECOMP_DIFF"},
        {&F::Reg::DAC_COMP_DIFF, "COMP_DIFF"},
        {&F::Reg::DAC_VFF_DIFF, "VFF_DIFF"},
        {&F::Reg::DAC_TH1_L_DIFF, "TH1_DIFF"},
        {&F::Reg::DAC_TH1_R_DIFF, "TH1_DIFF"},
        {&F::Reg::DAC_TH1_M_DIFF, "TH1_DIFF"},
        {&F::Reg::DAC_TH2_DIFF, "TH2_DIFF"},
        {&F::Reg::DAC_LCC_DIFF, "LCC_DIFF"},
        {&F::Reg::VCAL_HIGH, "VCAL"},
        {&F::Reg::VCAL_MED, "VCAL"}
    };
    return a;
}

template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::CMS), bool> = true>
const typename RD53BDACTest<F>::regToFuncType& regToFunc()
{
    static std::map<const Register*, std::string> a = {
        {&F::Reg::DAC_PREAMP_L_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_PREAMP_R_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_PREAMP_TL_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_PREAMP_TR_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_PREAMP_T_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_PREAMP_M_LIN, "PA_IN_BIAS_LIN"},
        {&F::Reg::DAC_FC_LIN, "FC_BIAS_LIN"},
        {&F::Reg::DAC_KRUM_CURR_LIN, "KRUM_CURR_LIN"},
        {&F::Reg::DAC_REF_KRUM_LIN, "REF_KRUM_LIN"},
        {&F::Reg::DAC_COMP_LIN, "COMP_LIN"},
        {&F::Reg::DAC_COMP_TA_LIN, "COMP_STAR_LIN"},
        {&F::Reg::DAC_GDAC_L_LIN, "Vthreshold_LIN"},
        {&F::Reg::DAC_GDAC_R_LIN, "Vthreshold_LIN"},
        {&F::Reg::DAC_GDAC_M_LIN, "Vthreshold_LIN"},
        {&F::Reg::DAC_LDAC_LIN, "LDAC_LIN"}, // TODO uh? it was Vthreshold_LIN before...
        {&F::Reg::VCAL_HIGH, "VCAL"},
        {&F::Reg::VCAL_MED, "VCAL"}
    };
    return a;
}

template <class Flavor>
const typename RD53BDACTest<Flavor>::regToFuncType& RD53BDACTest<Flavor>::regToFunc = ::regToFunc<Flavor>();


template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::ATLAS), bool> = true>
const typename RD53BDACTest<F>::muxSettType& regToMux()
{
    static std::map<const Register*, std::pair<typename F::IMux, typename F::VMux>> a = {
        {&F::Reg::DAC_PREAMP_L_DIFF, {F::IMux::I_PREAMP_L, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_R_DIFF, {F::IMux::I_PREAMP_R, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_TL_DIFF, {F::IMux::I_PREAMP_TL, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_TR_DIFF, {F::IMux::I_PREAMP_TR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_T_DIFF, {F::IMux::I_PREAMP_T, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_M_DIFF, {F::IMux::I_PREAMP_M, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PRECOMP_DIFF, {F::IMux::I_PRECOMPARATOR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_COMP_DIFF, {F::IMux::I_COMPARATOR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_VFF_DIFF, {F::IMux::I_FEEDBACK, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_TH1_L_DIFF, {F::IMux::I_VTH1_L, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_TH1_R_DIFF, {F::IMux::I_VTH1_R, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_TH1_M_DIFF, {F::IMux::I_VTH1_M, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_TH2_DIFF, {F::IMux::I_VTH2, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_LCC_DIFF, {F::IMux::LEAKAGE_CURRENT_COMP, F::VMux::IMUX_OUT}},
        {&F::Reg::VCAL_HIGH, {F::IMux::HIGH_Z, F::VMux::VCAL_HIGH}},
        {&F::Reg::VCAL_MED, {F::IMux::HIGH_Z, F::VMux::VCAL_MED}},
    };
    return a;
}

template <class F, std::enable_if_t<(F::flavor == Ph2_HwDescription::RD53BFlavor::Flavor::CMS), bool> = true>
const typename RD53BDACTest<F>::muxSettType& regToMux()
{
    static std::map<const Register*, std::pair<typename F::IMux, typename F::VMux>> a = {
        {&F::Reg::DAC_PREAMP_L_LIN, {F::IMux::I_PREAMP_L, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_R_LIN, {F::IMux::I_PREAMP_R, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_TL_LIN, {F::IMux::I_PREAMP_TL, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_TR_LIN, {F::IMux::I_PREAMP_TR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_T_LIN, {F::IMux::I_PREAMP_T, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_PREAMP_M_LIN, {F::IMux::I_PREAMP_M, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_FC_LIN, {F::IMux::I_FC, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_KRUM_CURR_LIN, {F::IMux::I_KRUMMENAKER, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_REF_KRUM_LIN, {F::IMux::HIGH_Z, F::VMux::VREF_KRUM}},
        {&F::Reg::DAC_COMP_LIN, {F::IMux::I_COMPARATOR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_COMP_TA_LIN, {F::IMux::I_COMPARATOR_STAR, F::VMux::IMUX_OUT}},
        {&F::Reg::DAC_GDAC_L_LIN, {F::IMux::HIGH_Z, F::VMux::GDAC_L}},
        {&F::Reg::DAC_GDAC_R_LIN, {F::IMux::HIGH_Z, F::VMux::GDAC_R}},
        {&F::Reg::DAC_GDAC_M_LIN, {F::IMux::HIGH_Z, F::VMux::GDAC_M}},
        {&F::Reg::DAC_LDAC_LIN, {F::IMux::I_LDAC, F::VMux::IMUX_OUT}},
        {&F::Reg::VCAL_HIGH, {F::IMux::HIGH_Z, F::VMux::VCAL_HIGH}},
        {&F::Reg::VCAL_MED, {F::IMux::HIGH_Z, F::VMux::VCAL_MED}},
    };
    return a;
}

template <class Flavor>
const typename RD53BDACTest<Flavor>::muxSettType& RD53BDACTest<Flavor>::muxSett = regToMux<Flavor>();


template class RD53BTools::RD53BDACTest<ATLAS>;
template class RD53BTools::RD53BDACTest<CMS>;
template class RD53BTools::RD53BDACTest<ITkPixV2>;
template class RD53BTools::RD53BDACTest<Ph2_HwDescription::RD53BFlavor::CROCv2>;
