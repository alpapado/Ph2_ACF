#include "RD53RingOscillatorWLT.h"

#include <cmath>
#include <fstream>
#include <string>
#include <vector>

#include "TFile.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"


namespace RD53BTools
{
template <class Flavor>
void RD53RingOscillatorWLT<Flavor>::init()
{
    pulseRoute = 1u << RD53BProd<Flavor>::GlobalPulseRoutes.at("StartRingOscillatorsA") | 1u << RD53BProd<Flavor>::GlobalPulseRoutes.at("StartRingOscillatorsB");
    pulseWidth = param("gateWidth"_s) / Flavor::globalPulseUnit;
    if(pulseWidth == 0) pulseWidth = 1;
    nBX = pulseWidth * Flavor::globalPulseUnit;
    if(param("gateWidth"_s) % Flavor::globalPulseUnit != 0) { LOG(WARNING) << "Requested gate width not compatible with this chip" << RESET; }
}

template <class Flavor>
ChipDataMap<typename RD53RingOscillatorWLT<Flavor>::ChipResults> RD53RingOscillatorWLT<Flavor>::run() const
{
    ChipDataMap<ChipResults> results;
    auto&                    chipInterface = Base::chipInterface();

    Base::system().fPowerSupplyClient->sendAndReceivePacket("K2410:SetSpeed,PowerSupplyId:" + param("vmeter"_s) + ",ChannelId:" + param("vmeterCH"_s) + ",IntegrationTime:1");
    Base::system().fPowerSupplyClient->sendAndReceivePacket("VoltmeterSetRange,VoltmeterId:" + param("vmeter"_s) + ",ChannelId:" + param("vmeterCH"_s) + ",Value:1.3");

    Base::for_each_chip([&](RD53BProd<Flavor>* chip) {
        results[chip].countEnableTimeBX = nBX;

        auto& trimOscCounts    = results[chip].trimOscCounts;
        auto& trimOscFrequency = results[chip].trimOscFrequency;
        auto& trimVoltage      = results[chip].trimVoltage;
        int&  n                = results[chip].n;

        uint16_t vTrimOld = chipInterface.ReadReg(chip, Flavor::Reg::VOLTAGE_TRIM);

        std::vector<uint16_t> initQ, sweepQ;

        /* initialization */

        uint16_t monConf = chipInterface.ReadReg(chip, Flavor::Reg::MonitorConfig);
        monConf          = (monConf & 0xffc0) | (uint8_t)RD53BProd<Flavor>::VMux::VDDD_HALF;

        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::MonitorConfig.address, monConf);
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::GlobalPulseWidth.address, pulseWidth);
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::GlobalPulseConf.address, pulseRoute);

        chipInterface.SendCommandStream(chip, initQ);

        /* command queue for voltage sweep */
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, sweepQ, Flavor::Reg::RingOscConfig.address, uint16_t{0xffff}); // clear
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, sweepQ, Flavor::Reg::RingOscConfig.address, uint16_t{0x3eff}); // hold
        chipInterface.template SerializeCommand<RD53BCmd::GlobalPulse>(chip, sweepQ);

        for(int vTrim = param("minVDDD"_s); vTrim <= param("maxVDDD"_s); vTrim++)
        {
            // Trim voltages
            chipInterface.WriteReg(chip, "TRIM_VREFD", vTrim);
            double v = 2 * std::stod(Base::system().fPowerSupplyClient->sendAndReceivePacket("ReadVoltmeter"
                                                                                             ",VoltmeterId:" +
                                                                                             param("vmeter"_s) + ",ChannelId:" + param("vmeterCH"_s)));
            chipInterface.SendCommandStream(chip, sweepQ);

            LOG(INFO) << BOLDBLUE << "VDDD: " << BOLDYELLOW << v << " V" << RESET;
            if(v > param("abortVDDD"_s)) break;

            trimVoltage[n] = v;

            for(int ringOsc = 0; ringOsc < 8; ringOsc++)
            {
                chipInterface.WriteReg(chip, "RingOscARoute", ringOsc);
                trimOscCounts[ringOsc][n]    = chipInterface.ReadReg(chip, Flavor::Reg::RING_OSC_A_OUT) & 0xfff;
                trimOscFrequency[ringOsc][n] = trimOscCounts[ringOsc][n] * 40 / nBX;
            }
            for(int ringOsc = 0; ringOsc < 34; ringOsc++)
            {
                chipInterface.WriteReg(chip, "RingOscBRoute", ringOsc);
                trimOscCounts[ringOsc + 8][n]    = chipInterface.ReadReg(chip, Flavor::Reg::RING_OSC_B_OUT) & 0xfff;
                trimOscFrequency[ringOsc + 8][n] = trimOscCounts[ringOsc + 8][n] * 40 / nBX;
            }

            ++n;
        }

        chipInterface.WriteReg(chip, Flavor::Reg::VOLTAGE_TRIM, vTrimOld);
    });

    return results;
}

template <class Flavor>
void RD53RingOscillatorWLT<Flavor>::draw(const ChipDataMap<ChipResults>& results)
{
    //create the ROOT file to store the results
    Base::createRootFile();

    //ring oscillator names
    const std::vector<std::string> oscNames = {
        //first group
        "CKND0", "CKND4", "INV0", "INV4", "NAND0", "NAND4", "NOR0", "NOR4",
        //second group
        "CKND0 L","CKND0 R", "CKND4 L", "CKND4 R", "INV0 L","INV0 R", "INV4 L",
        "INV4 R", "NAND0 L","NAND0 R", "NAND4 L","NAND4 R", "NOR0 L","NOR0 R",
        "NOR4 L","NOR4 R",
        //third group
        "SCAN DFF 0", "SCAN DFF 0", "DFF 0", "DFF 0", "NEG EDGE DFF 1",
        "NEG EDGE DFF 1",
        //fourth group
        "LVT INV 0", "LVT INV 4","LVT 4-IN NAND0", "LVT 4-IN NAND 4",
        //fifth group
        "0","1","2","3","4","5","6","7"
    };

    //ring oscillator groups
    const std::vector<std::pair<uint8_t, uint8_t>> oscGroups = {
        std::make_pair(0, 7),
        std::make_pair(8, 23),
        std::make_pair(24, 29),
        std::make_pair(30, 33),
        std::make_pair(34, 41),
    };

    //array to store fit data (0: offset; 1: slope)
    double fitResults[42][2];

    //looping on chip results
    for(const std::pair<const ChipLocation, ChipResults>& item: results)
    {
        Base::createRootFileDirectory(item.first);

        //loop on ring oscillator groups
        for (auto group: oscGroups) {

            //fit line
            TF1 line("line", "[offset]+[slope]*x");

            //canvas for drawing the multigraph
            TCanvas canvas;

            //oscillator graph with VDDD with multiple ring oscillator data
            TMultiGraph vdddMG;

            //loop on ring oscillators in the group
            for (auto i = group.first; i <= group.second; i++) {
                //TGraph with the frequency as a function of VDDD
                //Note: freqPlot gets deleted automatically by ROOT
                TGraph * freqPlot = new TGraph(item.second.n,
                    item.second.trimVoltage,
                    item.second.trimOscFrequency[i]);

                //writing the multigraph to the ROOT file
                freqPlot->SetTitle(oscNames[i].c_str());
                vdddMG.Add(freqPlot, "APL");
                freqPlot->Write();

                //fitting
                freqPlot->Fit("line", "SNQ", "", 0, 16);

                //writing the fit results
                fitResults[i][0] = line.GetParameter(0);
                fitResults[i][1] = line.GetParameter(1);

            } //end of loop on ring oscillators in the group

            //writing the canvas to the ROOT file
            vdddMG.SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
            vdddMG.Draw("A pmc plc");
            gPad->BuildLegend();
            canvas.Write();

        } //end of loop on ring oscillator groups

    } //end of loop on chips

    //JSON output
    //property_tree from Boost could be used to produce the JSON output
    std::ofstream json(Base::getOutputFilePath("results.json"));
    const char*   str = "{";
    json << std::scientific << "{\"chips\":[";
    for(const std::pair<const ChipLocation, ChipResults>& chip: results)
    {
        json << str;
        str = ",{";
        json << "\"board\":" << chip.first.board_id << ","
             << "\"hybrid\":" << chip.first.hybrid_id << ","
             << "\"lane\":" << chip.first.chip_lane << ","
             << "\"count_en_t_BX\":" << chip.second.countEnableTimeBX << ",";
        for(int i = 0; i < 42; ++i)
        {
            /* oscillator data begins here */
            json << "\"";
            if(i < 8) json << "A" << i;
            else json << "B" << i - 8;
            json << "\":{\"fitted_line\":{\"intercept\":" << fitResults[i][0] << ",\"slope\":" << fitResults[i][1] << "},"
                 << "\"frequency\":[";
            for(int j = 0; j < chip.second.n; ++j)
            {
                json << chip.second.trimOscFrequency[i][j] * 1e6;
                if(j < chip.second.n - 1) json << ",";
            }
            json << "]},";
            /* oscillator data ends here */
        }
        json << "\"VDDD\":[";
        for(int j = 0; j < chip.second.n; ++j)
        {
            json << chip.second.trimVoltage[j];
            if(j < chip.second.n - 1) json << ",";
        }
        json << "]}"; // end of chip data
    }
    json << "]}";
}

template class RD53RingOscillatorWLT<RD53BFlavor::ATLAS>;
template class RD53RingOscillatorWLT<RD53BFlavor::CMS>;
template class RD53RingOscillatorWLT<RD53BFlavor::ITkPixV2>;
template class RD53RingOscillatorWLT<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
