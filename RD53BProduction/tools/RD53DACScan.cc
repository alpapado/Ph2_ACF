#include "RD53DACScan.h"

#include "ProductionToolsIT/ITchipTestingInterface.h"

#include <TFitResult.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TVector.h>

#define LOGNAME_FORMAT "%d%m%y_%H%M%S"
#define LOGNAME_SIZE 50

namespace RD53BTools
{
template <class Flavor>
constexpr const char* RD53DACScan<Flavor>::writeVar[];

template <class Flavor>
typename RD53DACScan<Flavor>::Results RD53DACScan<Flavor>::run() const
{
    Results results;
    auto&   chipInterface = Base::chipInterface();
    auto    chip          = Base::firstChip();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    auto& DACcode  = results.DACcode;
    auto& VMUXvolt = results.VMUXvolt;
    auto& fitStart = results.fitStart;
    auto& fitEnd   = results.fitEnd;
    int   stepSize = 100;

    for(int variable = 0; variable < param("nVariables"_s); variable++)
    {
        fitStart[variable] = 0;
        fitEnd[variable]   = 0;
        DACcode.push_back(std::vector<double>());
        VMUXvolt.push_back(std::vector<double>());
    }

    for(int input = 0; input < 4096; input += stepSize)
    {
        LOG(INFO) << BOLDBLUE << "i        = " << BOLDYELLOW << input << " " << RESET;
        for(int variable = 0; variable < param("nVariables"_s); variable++)
        {
            if(input > 4096) continue;
            chipInterface.WriteReg(chip, writeVar[variable], input);
            chipInterface.WriteReg(chip, "MonitorEnable", 1); // Choose MUX entry
            chipInterface.WriteReg(chip, "VMonitor", 0b000111);

            DACcode[variable].push_back(input); // Read DAC code
            VMUXvolt[variable].push_back(dKeithley2410.getVoltage());

            if(input > 1)
            {
                if(((DACcode[variable].end()[-1] > 0 && DACcode[variable].end()[-2] == 0) || (DACcode[variable].end()[-2] > 0 && DACcode[variable].end()[-1] == 0)) && fitStart[variable] == 0)
                    fitStart[variable] = DACcode[variable].end()[-1];
                if(((DACcode[variable].end()[-1] == 4095 && DACcode[variable].end()[-2] < 4095) || (DACcode[variable].end()[-1] < 4095 && DACcode[variable].end()[-2] == 4095)) &&
                   fitEnd[variable] == 0)
                    fitEnd[variable] = DACcode[variable].end()[-1];
                if(fitEnd[variable] == 0 && input >= 4000) fitEnd[variable] = DACcode[variable].end()[-1];
            }
        }
    }

    return results;
}

template <class Flavor>
void RD53DACScan<Flavor>::draw(const Results& results)
{
    auto& DACcode  = results.DACcode;
    auto& VMUXvolt = results.VMUXvolt;
    auto& fitStart = results.fitStart;
    auto& fitEnd   = results.fitEnd;

    static char auxvar[LOGNAME_SIZE];
    time_t      now = time(0);
    strftime(auxvar, sizeof(auxvar), LOGNAME_FORMAT, localtime(&now));
    std::string outputname;
    outputname = auxvar;

    auto canvas = new TCanvas();
    // remove("Results/DAC_linearity.root"); // Remove old file
    TFile* file = new TFile((Base::getOutputBasePath() + "/DAC_linearity_" + outputname + ".root").c_str(), "new");
    canvas->Print((Base::getOutputBasePath() + "/DAC_plots_" + outputname + ".pdf[").c_str());
    for(int variable = 0; variable < param("nVariables"_s); variable++)
    {
        std::cout << "Fitting: " << writeVar[variable] << ", fitStart = " << fitStart[variable] << ", fitEnd = " << fitEnd[variable] << std::endl;
        std::cout << "DAC Codes: ";
        for(auto code: DACcode[variable]) std::cout << code << " ";
        std::cout << std::endl;
        std::cout << "VMUXvolt: ";
        for(auto code: VMUXvolt[variable]) std::cout << code << " ";
        std::cout << std::endl;
        TGraph* linear = new TGraph(DACcode[variable].size(), &(DACcode[variable][0]), &(VMUXvolt[variable][0]));
        linear->SetTitle("Linear Graph;DAC Code;Voltage");
        linear->SetName(writeVar[variable]);
        linear->Fit("pol1", "", "", fitStart[variable], fitEnd[variable]);
        // linear->Fit("pol1","","",20,4080);
        TFitResultPtr r = linear->Fit("pol1", "S", "", fitStart[variable], fitEnd[variable]);
        gStyle->SetOptFit(0);
        gStyle->SetFitFormat(".2g");
        linear->Draw("APL");
        canvas->Print((Base::getOutputBasePath() + "/DAC_plots_" + outputname + ".pdf").c_str());
        static const std::string fileName = Base::getOutputBasePath() + "/DACCalibration.csv";
        std::ofstream            outFile;
        if(boost::filesystem::exists(fileName))
            outFile.open(fileName, std::ios_base::app);
        else
        {
            outFile.open(fileName);
            outFile << "time, variable, Intercept, Slope\n";
        }
        auto now = time(0);
        outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
        outFile << writeVar[variable] << ", " << r->Value(0) << ", " << r->Value(1) << "\n";
        linear->Write();
    }
    file->Write();
    canvas->Print((Base::getOutputBasePath() + "/DAC_plots_" + outputname + ".pdf]").c_str());
    file->Close();

    // Base::createRootFile();

    // std::ofstream outFile(Base::getOutputFilePath("DACCalibration.csv"));
    // outFile << "Intercept, Slope\n";

    // auto canvas = new TCanvas();

    // for(int variable = 0; variable < 1; variable++)
    // {
    //     TGraph* linear = new TGraph(results.DACcode.size(), &(results.DACcode[0][variable]), &(results.VMUXvolt[0][variable]));
    //     linear->SetTitle("Linear Graph;DAC Code;Voltage");
    //     linear->SetName(writeVar[variable]);
    //     linear->Fit("pol1", "", "", results.fitStart[variable], results.fitEnd[variable]);
    //     //linear->Fit("pol1","","",20,4080);
    //     TFitResultPtr r = linear->Fit("pol1", "S", "", results.fitStart[variable], results.fitEnd[variable]);
    //     gStyle->SetOptFit(0);
    //     gStyle->SetFitFormat(".2g");
    //     linear->Draw("APL");

    //     outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    //     linear->Write();
    // }

    // canvas->Print(Base::getOutputFilePath("DAC_plots.pdf").c_str());
}

template class RD53DACScan<RD53BFlavor::ATLAS>;
template class RD53DACScan<RD53BFlavor::CMS>;
template class RD53DACScan<RD53BFlavor::ITkPixV2>;
template class RD53DACScan<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
