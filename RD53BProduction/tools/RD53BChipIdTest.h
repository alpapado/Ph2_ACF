#ifndef RD53BCHIPIDTEST_H
#define RD53BCHIPIDTEST_H

#include <string>

#include <termios.h>

#include "RD53BTool.h"

#include "../HWDescription/RD53B.h"
#include "../HWInterface/RD53BInterface.h"
#include "Utils/BitMaster/BitVector.h"

namespace RD53BTools {

template <class>
class RD53BChipIdTest;

template <class Flav>
const auto ToolParameters<RD53BChipIdTest<Flav>> = make_named_tuple(
    std::make_pair("wpacPort"_s, std::string()),
    std::make_pair("wpacRetries"_s, 0u),
    std::make_pair("noWPAC"_s, false) // run test without a WPAC (expect test failing)
);

template <class Flav>
struct RD53BChipIdTest : public RD53BTool<RD53BChipIdTest, Flav> {
    using Address = decltype(static_cast<RD53BProdConstants::Register*>(nullptr)->address);
    using Id = size_t;

    static constexpr unsigned i2cAddr = 0x21; ///> I2C address of expander setting chip id
    static constexpr Id maxId = 15; ///> maximum chip id (for addressed commands)

    struct Results {
        unsigned readsPerReg;
        bool success[maxId + 1][2];
        unsigned activity[maxId + 1][2][maxId + 1];
        bool badPadReadout[maxId + 1];
    };

    using Base = RD53BTool<RD53BChipIdTest, Flav>;
    using Base::Base;
    using Base::param;

    Results run();

    void draw(const Results& results) const;

  private:
    int fWPAC;
    termios conf[2];

    /**
     * \brief Setup the serial communication with the WPAC
     */
    void initCommWPAC();

    /**
     * \brief Restore the original serial port configuration for the WPAC and release the resource
     */
    void releaseWPAC();

    /**
     * \brief Read state of the lines of the expander controlling the chip id
     * \return A byte with the state of one line encoded in each bit
     */
    unsigned long readExpander();

    /**
     * \brief Set state of the lines of the expander controlling the chip id
     * \param data : A byte with the state of one line encoded in each bit
     * \return true if no error was encountered, false otherwise
     */
    bool writeExpander(unsigned long data);

    /**
     * \brief Set the id of the DUT to the requested value
     * \param id : The chip id to set
     * \return false if an error was encountered
     */
    bool setChipId(Id id);

    /**
      * \brief Tests the addressing properties of the DUT
      * \param fwIface : Interface to the FC7 board
      * \param hybrid : Hybrid to test
      * \param activity : Array with counters of responses at each (command) address
      * \param broadcast : Selects whether single-chip-addressing or broadcast is tested
      */
    void testActivity(RD53FWInterface& fwIface, const Hybrid* hybrid, unsigned (&activity)[maxId + 1], bool broadcast);

    /**
     * \brief Checks if there is a problem with the pad readout
     * \param setId : The current id of the chip
     * \param fwIface : Interface to the FC7 board
     * \param hybrid : Module with the chip under test
     * \return false if no errors were encountered
     */
    bool isPadReadoutBad(Id setId, RD53FWInterface& fwIface, const Hybrid* hybrid) {
        LOG(INFO) << "Checking chip id readback from global register" << RESET;
        std::vector<uint16_t> q;
        RD53BCmd::serialize(RD53BCmd::RdReg{setId, RD53BFlavor::ITkPixV2::Reg::PadReadout.address}, q);
        fwIface.WriteChipCommand(q, hybrid->getHybridId());
        bool bad = false;
        while(!fwIface.ReadReg("user.stat_regs.readout1.register_fifo_empty")) {
            Id padId = (fwIface.ReadReg("user.stat_regs.Register_Rdback_fifo") >> 4) & 0xF;
            bad = (padId != setId);
        }
        return bad;
    }
    bool isPadReadoutBad(...) {
        LOG(INFO) << "Skipping chip id readback from global register" << RESET;
        return false;
    }
};


}

#endif
