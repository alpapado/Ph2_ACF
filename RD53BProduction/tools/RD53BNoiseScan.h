#ifndef RD53BNOISESCAN_H
#define RD53BNOISESCAN_H

#include "RD53BInjectionTool.h"

#include "Utils/RD53BEventDecoding.h"

#include "TTree.h"

namespace RD53BTools
{
template <class>
struct RD53BNoiseScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BNoiseScan<Flavor>> = make_named_tuple(std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
                                                                     std::make_pair("nTriggers"_s, 1000000ul),
                                                                     std::make_pair("triggerPeriod"_s, 100ul),
                                                                     std::make_pair("readoutPeriod"_s, 1000ul),
                                                                     std::make_pair("occupancyThreshold"_s, 1e-4),
                                                                     std::make_pair("maskNoisyPixels"_s, true),
                                                                     std::make_pair("storeHits"_s, false));

template <class Flavor>
struct RD53BNoiseScan : public RD53BTool<RD53BNoiseScan, Flavor>
{
    using Base = RD53BTool<RD53BNoiseScan, Flavor>;
    using Base::Base;
    using Base::param;

    using ChipEventsMap = ChipDataMap<std::vector<RD53BEventDecoding::RD53BEvent>>;

    struct ChipResult
    {
        pixel_matrix_t<Flavor, bool> enabled;
        // std::vector<RD53BEventDecoding::RD53BEvent> events;
        xt::xtensor<size_t, 3> hitCount;
        xt::xtensor<size_t, 4> ToTHistogram;
    };

    void init();

    ChipDataMap<ChipResult> run(Task progress);

    // ChipDataMap<pixel_matrix_t<Flavor, size_t>> hitCount(const ChipDataMap<ChipResult>& data) const;

    void draw(const ChipDataMap<ChipResult>& result);

    // const auto& offset(size_t i) const { return param("offset"_s)[i]; }
    // const auto& size(size_t i) const { return param("size"_s)[i]; }
    // auto rowRange() const { return xt::xrange<std::ptrdiff_t>(offset(0), offset(0) + size(0)); }
    // auto colRange() const { return xt::xrange<std::ptrdiff_t>(offset(1), offset(1) + size(1)); }

  private:
    ChipDataMap<TTree*> hitTrees;
    size_t              _nRounds;
    size_t              _nReduced;
};

} // namespace RD53BTools

#endif