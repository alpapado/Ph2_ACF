
#include "RD53VrefTrimming.h"

#include "ProductionToolsIT/ITchipTestingInterface.h"

#include "TFitResult.h"
#include "TGraph.h"

#include "Utils/xtensor/xarray.hpp"


namespace RD53BTools
{
template <class>
struct RD53VrefTrimming; // forward declaration

template <class Flavor>
typename RD53VrefTrimming<Flavor>::Results RD53VrefTrimming<Flavor>::run() const
{
    Results results;
    auto&   chipInterface = Base::chipInterface();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    auto chip = Base::firstChip();

    for(int vTrim = 0; vTrim < 16; vTrim++)
    { // VS VDDD
        // Trim voltages
        chipInterface.WriteReg(chip, "TRIM_VREFD", vTrim);
        chipInterface.WriteReg(chip, "MonitorEnable", 1);
        chipInterface.WriteReg(chip, "VMonitor", 38);
        results.vdddVoltage[vTrim] = dKeithley2410.getVoltage() * 2;
        LOG(INFO) << BOLDMAGENTA << "trim value: " << vTrim << ", Voltage: " << results.vdddVoltage[vTrim] << RESET;
    }

    {
        size_t i_best = 0;
        double best   = 999;
        for(int i = 0; i < 16; ++i)
        {
            auto value = std::abs(results.vdddVoltage[i] - 1.2);
            if(value < best)
            {
                best   = value;
                i_best = i;
            }
        }
        LOG(INFO) << "best VDDD voltage: " << results.vdddVoltage[i_best] << " at trim value: " << i_best;
        chipInterface.ConfigureReg(chip, "TRIM_VREFD", i_best);
    }

    for(int vTrim = 0; vTrim < 16; vTrim++)
    { // VS VDDA
        // Trim voltages
        chipInterface.WriteReg(chip, "TRIM_VREFA", vTrim);
        chipInterface.WriteReg(chip, "MonitorEnable", 1);
        chipInterface.WriteReg(chip, "VMonitor", 34);
        results.vddaVoltage[vTrim] = dKeithley2410.getVoltage() * 2;
        LOG(INFO) << BOLDMAGENTA << "trim value: " << vTrim << ", Voltage: " << results.vddaVoltage[vTrim] << RESET;
    }

    {
        size_t i_best = 0;
        double best   = 999;
        for(int i = 0; i < 16; ++i)
        {
            auto value = std::abs(results.vddaVoltage[i] - 1.2);
            if(value < best)
            {
                best   = value;
                i_best = i;
            }
        }

        LOG(INFO) << "best VDDA voltage: " << results.vddaVoltage[i_best] << " at trim value: " << i_best;

        chipInterface.ConfigureReg(chip, "TRIM_VREFA", i_best);
    }

    return results;
}

template <class Flavor>
void RD53VrefTrimming<Flavor>::draw(const Results& results)
{
    Base::createRootFile();

    std::ofstream outFile(Base::getOutputFilePath("vref_trimming.csv"));

    outFile << "VDDD_trim, VDDA_trim\n";

    xt::xarray<double> trimBits = xt::arange(16);

    // VDDD
    TGraph* vddd_linear = new TGraph(16, trimBits.data(), results.vdddVoltage);
    vddd_linear->SetTitle("VDDD vs trim value");
    TFitResultPtr vddd_fit       = vddd_linear->Fit("pol1", "S", "");
    double        vddd_idealTrim = (1.2 - vddd_fit->Value(0)) / vddd_fit->Value(1);
    vddd_linear->Write();

    // VDDA
    TGraph* vdda_linear = new TGraph(16, trimBits.data(), results.vddaVoltage);
    vdda_linear->SetTitle("VDDA vs trim value");
    TFitResultPtr vdda_fit       = vdda_linear->Fit("pol1", "S", "");
    double        vdda_idealTrim = (1.2 - vdda_fit->Value(0)) / vdda_fit->Value(1);
    vdda_linear->Write();

    outFile << vddd_idealTrim << ", " << vdda_idealTrim << '\n';
}

template class RD53VrefTrimming<RD53BFlavor::ATLAS>;
template class RD53VrefTrimming<RD53BFlavor::CMS>;
template class RD53VrefTrimming<RD53BFlavor::ITkPixV2>;
template class RD53VrefTrimming<RD53BFlavor::CROCv2>;

} // namespace RD53BTools
