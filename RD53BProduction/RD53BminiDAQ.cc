#include "HWDescription/RD53BProd.h"
#include "System/SystemController.h"
#include "Utils/argvparser.h"

#include "tools/RD53BToolManager.h"

#include "tools/RD53ADCScan.h"
#include "tools/RD53BADCCalib.h"
#include "tools/RD53BCapMeasure.h"
#include "tools/RD53BCapMeasureScan.h"
#include "tools/RD53BChipIdTest.h"
#include "tools/RD53BDACCalib.h"
#include "tools/RD53BInjectionTool.h"
#include "tools/RD53BNoiseScan.h"
#include "tools/RD53BRegReader.h"
#include "tools/RD53BRegTest.h"
#include "tools/RD53BThresholdEqualization.h"
#include "tools/RD53BThresholdScan.h"
#include "tools/RD53BTool.h"
#include "tools/RD53DACScan.h"
#include "tools/RD53IVScan.h"
#include "tools/RD53MuxScan.h"
#include "tools/RD53RingOscillator.h"
#include "tools/RD53RingOscillatorWLT.h"
#include "tools/RD53ShortRingOscillator.h"
#include "tools/RD53ShortTempSensor.h"
#include "tools/RD53TempSensor.h"
#include "tools/RD53TsensCalibration.h"
#include "tools/RD53TsensReadoutADC.h"
#include "tools/RD53VrefTrimming.h"
#include "tools/RD53BDACTest.h"
#include "tools/RD53BBERTscan.h"
#include "tools/RD53BConfigure.h"
#include "tools/RD53BFWRegReader.h"
#include "tools/RD53BGainScan.h"
#include "tools/RD53BGainTuning.h"
#include "tools/RD53BGlobalThresholdTuning.h"
#include "tools/RD53BInjectionDelay.h"
#include "tools/RD53BNoiseFloor.h"
#include "tools/RD53BShmoo.h"
#include "tools/RD53BSignalScan.h"
#include "tools/RD53BStuckPixelScan.h"
#include "tools/RD53BTempSensorScan.h"
#include "tools/RD53BThresholdOscillation.h"
#include "tools/RD53BThresholdTuning.h"
#include "tools/RD53BTimeWalk.h"
#include "tools/RD53BUplinkCalibration.h"
#include "tools/PllLockScan.h"

//WLT
#include "tools/RD53BDataMergingTestWLT.h"
#include "tools/RD53TempSensorWLT.h"
#include "tools/RD53BTemp.h"
#include "tools/RD53BScanChainTestWLT.h"
#include "tools/RD53BEfuseProgrammer.h"
#include "tools/RD53BMonMuxTest.h"
#include "tools/RD53BCCWaitScan.h"
#include "tools/RD53BCBWaitScan.h"
#include "tools/RD53BCBWaitTest.h"

#include <cstdlib>

using namespace Ph2_System;
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace RD53BTools;
using namespace RD53BUtils;

#define TOOL(x) std::make_pair(#x##_s, x<Flavor>{})

template <class Flavor>
using Tools = ToolManager<decltype(make_named_tuple(TOOL(RD53BInjectionTool),
                                                    TOOL(RD53BRegReader),
                                                    TOOL(RD53BThresholdScan),
                                                    TOOL(RD53BRegTest),
                                                    TOOL(RD53RingOscillator),
                                                    TOOL(RD53ShortRingOscillator),
                                                    TOOL(RD53MuxScan),
                                                    TOOL(RD53IVScan),
                                                    TOOL(RD53ADCScan),
                                                    TOOL(RD53DACScan),
                                                    TOOL(RD53TempSensor),
                                                    TOOL(RD53BThresholdEqualization),
                                                    TOOL(RD53BNoiseScan),
                                                    TOOL(RD53ShortTempSensor),
                                                    TOOL(RD53TsensReadoutADC),
                                                    TOOL(RD53TsensCalibration),
                                                    TOOL(RD53VrefTrimming),
                                                    TOOL(RD53BCapMeasureScan),
                                                    TOOL(RD53BCapMeasure),
                                                    TOOL(RD53RingOscillatorWLT),
                                                    TOOL(RD53TempSensorWLT),
                                                    TOOL(RD53BTemp),
                                                    TOOL(RD53BADCCalib),
                                                    TOOL(RD53BDACCalib),
                                                    TOOL(RD53BDACTest),
                                                    TOOL(RD53BChipIdTest),
                                                    TOOL(RD53BGlobalThresholdTuning),
                                                    TOOL(RD53BStuckPixelScan),
                                                    TOOL(RD53BTimeWalk),
                                                    TOOL(RD53BThresholdOscillation),
                                                    TOOL(RD53BGainTuning),
                                                    TOOL(RD53BThresholdTuning),
                                                    TOOL(RD53BShmoo),
                                                    TOOL(RD53BGainScan),
                                                    TOOL(RD53BBERTscan),
                                                    TOOL(RD53BInjectionDelay),
                                                    TOOL(RD53BConfigure),
                                                    TOOL(RD53BTempSensorScan),
                                                    TOOL(RD53BSignalScan),
                                                    TOOL(RD53BFWRegReader),
                                                    TOOL(RD53BNoiseFloor),
                                                    TOOL(RD53BUplinkCalibration),
                                                    TOOL(RD53BScanChainTestWLT),
                                                    TOOL(RD53BDataMergingTestWLT),
                                                    TOOL(RD53BEfuseProgrammer),
                                                    TOOL(RD53BMonMuxTest),
                                                    TOOL(RD53BCCWaitScan),
                                                    TOOL(RD53BCBWaitScan),
                                                    TOOL(RD53BCBWaitTest),
                                                    TOOL(PllLockScan)
                                                    ))>;

INITIALIZE_EASYLOGGINGPP

void resetAndExit(int sig)
{
    indicators::show_console_cursor(true); // show cursor
    // std::fputs("\x1b[?7h", stdout); // reset auto-wrap mode
    std::fputs("\033[0m", stdout); // reset colors
    std::fflush(stdout);           // flush stdout
    signal(sig, SIG_DFL);          // set default signal handler
    raise(sig);                    // re-raise signal
}

template <class Flavor>
void run(SystemController& system, CommandLineProcessing::ArgvParser& cmd)
{
    if(cmd.foundOption("assumeDefault"))
    {
        for_each_device<Chip>(system, [](Chip* chip) { static_cast<RD53BProd<Flavor>*>(chip)->setDefaultState(); });
    }

    if(!cmd.foundOption("skipConfiguration")) system.ConfigureHw();

    // auto toolConfig = toml::parse(cmd.optionValue("tools"));

    bool showPlots = !cmd.foundOption("hidePlots");

    std::string resultsPath;
    if(cmd.foundOption("outputDir"))
        resultsPath = cmd.optionValue("outputDir");
    else
        resultsPath = "Results/";

    std::string toolsConfigFile;
    if(cmd.foundOption("tools"))
        toolsConfigFile = cmd.optionValue("tools");
    else
        toolsConfigFile = system.findValueInSettings<std::string>("RD53BToolsConfigFile");

    LOG(INFO) << "Using tools config file: " << BOLDYELLOW << toolsConfigFile << RESET;

    Tools<Flavor>(system, toolsConfigFile, showPlots, resultsPath).run_tools(cmd.allArguments());

    if(cmd.foundOption("saveState")) for_each_device<Chip>(system, [&](Chip* chip) { static_cast<RD53BProd<Flavor>*>(chip)->saveConfig(); });
}

int main(int argc, char** argv)
{
    CommandLineProcessing::ArgvParser cmd;

    cmd.setIntroductoryDescription("RD53B test");

    cmd.defineOption("reset", "Reset the backend board", CommandLineProcessing::ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("reset", "r");

    cmd.defineOption("file", "Hardware description file (.xml)", CommandLineProcessing::ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("tools", "Tools configuration file (.toml)", CommandLineProcessing::ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("tools", "t");

    cmd.defineOption("hidePlots", "Do not show plots.", CommandLineProcessing::ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("hidePlots", "h");

    cmd.defineOption("assumeDefault", "Assume that chips are in their default initial state.", CommandLineProcessing::ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("assumeDefault", "d");

    cmd.defineOption("saveState", "Save register values and pixel configuration in .toml file.", CommandLineProcessing::ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("saveState", "s");

    cmd.defineOption("outputDir", "Specify output directory (default: \"Results/\").", CommandLineProcessing::ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("outputDir", "o");

    cmd.defineOption("skipConfiguration", "Skip system configuration.", CommandLineProcessing::ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("skipConfiguration", "i");

    int result = cmd.parse(argc, argv);

    bool reset = cmd.foundOption("reset");

    if(result != CommandLineProcessing::ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(EXIT_FAILURE);
    }

    SystemController system;

    auto configFile = cmd.optionValue("file");

    if(reset)
    {
        system.InitializeSettings(configFile);
        system.InitializeHw(configFile);
        if(system.fDetectorContainer->at(0)->at(0)->flpGBT == nullptr)
            static_cast<RD53FWInterface*>(system.fBeBoardFWMap[system.fDetectorContainer->at(0)->getId()])->ResetSequence("160");
        else
            static_cast<RD53FWInterface*>(system.fBeBoardFWMap[system.fDetectorContainer->at(0)->getId()])->ResetSequence("320");
        exit(EXIT_SUCCESS);
    }

    system.InitializeHw(configFile);
    system.InitializeSettings(configFile);

    struct sigaction sigHandler;

    sigHandler.sa_handler = resetAndExit;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags = 0;

    sigaction(SIGINT, &sigHandler, NULL);
    sigaction(SIGTERM, &sigHandler, NULL);
    sigaction(SIGABRT, &sigHandler, NULL);

    auto feType = system.fDetectorContainer->at(0)->getFrontEndType();
    if(feType == FrontEndType::ITkPixV1)
        run<RD53BFlavor::ATLAS>(system, cmd);
    else if (feType == FrontEndType::CROC)
        run<RD53BFlavor::CMS>(system, cmd);
    else if (feType == FrontEndType::ITkPixV2)
        run<RD53BFlavor::ITkPixV2>(system, cmd);
    else if (feType == FrontEndType::CROCv2)
        run<RD53BFlavor::CROCv2>(system, cmd);
    else
        LOG(ERROR) << "Incompatible front end type";

    system.Destroy();

    LOG(INFO) << "RD53BminiDAQ finished successfully.";
}
